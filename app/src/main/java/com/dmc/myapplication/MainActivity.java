package com.dmc.myapplication;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.dmc.myapplication.Models.logintime;
import com.dmc.myapplication.Models.rewards;
import com.dmc.myapplication.assessment.assessmentBiz;
import com.dmc.myapplication.assessment.assessmentConstant;
import com.dmc.myapplication.blood.bloodMain;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.bp.bpMain;
import com.dmc.myapplication.dmInfo.webViewFragment;
import com.dmc.myapplication.drug.drugBiz;
import com.dmc.myapplication.drug.drugConstant;
import com.dmc.myapplication.drug.drugDateTool;
import com.dmc.myapplication.export.exportFragment;
import com.dmc.myapplication.goal.goalBiz;
import com.dmc.myapplication.gymNew.gymNewBiz;
import com.dmc.myapplication.gymNew.gymNewConstant;
import com.dmc.myapplication.help.helpFragment;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.login.welcomeActivity;
import com.dmc.myapplication.navFood.ReviewLeaderboardActivity;
import com.dmc.myapplication.navFood.navFoodBiz;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navFoodFragment;
import com.dmc.myapplication.navFood.navFoodTime;
import com.dmc.myapplication.navFood.navfoodAsyncTask;
import com.dmc.myapplication.otherBodyIndex.otherBody_Main;
import com.dmc.myapplication.preference.SettingsActivity;
import com.dmc.myapplication.reminder.reminderFragment;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.xiaomi.BluetoothState;
import com.facebook.FacebookSdk;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Context getContext = null;
    UserLocalStore userLocalStore;
    Integer mode = 0;
    static boolean rewardedLogin = false;
    static int abc = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_main);
        checkFirstRun();
        FacebookSdk.sdkInitialize(MainActivity.this);
        System.out.println("open times: " + abc);
        bodyRecordConstant.getApplicationContext = getApplicationContext();
        getContext = this.getBaseContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        System.out.println(">>>>>" + getSupportFragmentManager().getBackStackEntryCount());

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        // set the return control
        //Intent intent = getIntent();
        final int displayId = getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
        System.out.println("displayId=" + displayId);

        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                System.out.println("getSupportFragmentManager().getBackStackEntryCount()" + getSupportFragmentManager().getBackStackEntryCount());
                System.out.println("getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE)" + getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE));

                if ((getSupportFragmentManager().getBackStackEntryCount() > 0)) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });

        //System.out.println("Candy get display Id="+displayId);


        if (displayId == systemConstant.DISPLAY_HOMEPAGE) {
            this.setTitle(R.string.app_name);

            ft.replace(R.id.content_frame, new homePageFragment()).commit();


        }
        if (displayId == systemConstant.DISPLAY_NAVFOOD) {
            // 飲食日記
            String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
            setTitle(R.string.nav_food);
            System.out.println("initial setup for food: " + getFoodDate);
            Bundle bundle = new Bundle();
            bundle.putString(navFoodConstant.GET_FOOD_DATE, getFoodDate);
            navFoodFragment navFoodFrag = new navFoodFragment();
            navFoodFrag.setArguments(bundle);
            ft.replace(R.id.content_frame, navFoodFrag).addToBackStack(null).commit();
        }
        if (displayId == systemConstant.DISPLAY_GYM) {
            // 運動日記
            //String getGYMDate = getIntent().getExtras().getString(gymConstant.GET_GYM_DATE);
            //gymBiz.startGym(this, getGYMDate, ft);
            String getGYMDate = getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);
            gymNewBiz.startNewGym(this, getGYMDate, ft);
        }
        if (displayId == systemConstant.DISPLAY_DRUG) {
            //用藥記錄
            String getDrugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
            drugBiz.startDrug(this, getDrugDate, ft);
        }
        if (displayId == systemConstant.DISPLAY_BLOOD) {
            this.setTitle(R.string.title_activity_blood);
            //ft.add(R.id.content_frame, new bloodMain()).hide(new homePageFragment()).commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ft.replace(R.id.content_frame, new bloodMain()).commit();
        }
        if (displayId == systemConstant.DISPLAY_BP) {
            this.setTitle(R.string.title_activity_bp);
            ft.replace(R.id.content_frame, new bpMain()).addToBackStack(null).commit();
        }
        if (displayId == systemConstant.DISPLAY_REMINDER) {
            this.setTitle(R.string.medic_reminder);
            ft.replace(R.id.content_frame, new reminderFragment()).addToBackStack(null).commit();
        }
        if (displayId == systemConstant.DISPLAY_OTHER_INDEX) {
            this.setTitle(R.string.title_activity_om);
            ft.replace(R.id.content_frame, new otherBody_Main()).addToBackStack(null).commit();
        }
        if (displayId == systemConstant.DISPLAY_ASSESSMENT) {
            String year = getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_YEAR);
            String weekNo = getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_WEEK);
            assessmentBiz.startAssessment(this, year, weekNo, ft);
        }
        if (displayId == systemConstant.DISPLAY_LEADERBOARD) {
            ft.replace(R.id.content_frame, new ReviewLeaderboardActivity()).addToBackStack((null)).commit();

        }

        if (displayId == systemConstant.DISPLAY_GOAL) {
            goalBiz.startGoal(this, navFoodTime.getVurrentDateInDBFormal(), ft);
        }


        //Set Menu Drawer
        //Janus
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

        //NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);
        userLocalStore = new UserLocalStore(this);


    }
    public void adjustFontScale(Configuration configuration) {
        Log.e("abc", "fontScale=" + configuration.fontScale); //Custom Log class, you can use Log.w
        if (configuration.fontScale > 1.20) {
            Log.e("abc", "font too big. scale down..."); //Custom Log class, you can use Log.w
            configuration.fontScale = (float) 1.20;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getBaseContext().getResources().updateConfiguration(configuration, metrics);
        }
    }
    private void checkFirstRun() {
        final String PREFS_NAME = "MyPrefsFile";
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;

        // Get current version code
        int currentVersionCode = BuildConfig.VERSION_CODE;

        // Get saved version code
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        List<HashMap<String, String>> result = new ArrayList<>();
        List<HashMap<String, String>> rewresult = new ArrayList<>();
        UserLocalStore userLocalStores = new UserLocalStore(getApplicationContext());
        String userId = Integer.toString(userLocalStores.getLoggedInUser().userid);
        //rewardshistory rewardshist = new rewardshistory(true);

        logintime logintime = new logintime(getApplicationContext());
        rewards rewards = new rewards(getApplicationContext());

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {

            // TODO This is just a normal run
            result = logintime.getAllCreatedatetimeEditdatetime();
            rewresult = rewards.getAllCreatedatetimeEditdatetime();
            //if (!rewardshist.compareTimestamps() && !rewardshist.isRewardsLogin()){
            //  System.out.println("can attain rewards for login");
            //  rewardshist.setRewardsLogin(true);
            // }
            System.out.println("open time setting: " + result);
            System.out.println("current rewards: " + rewresult);
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
            String currentTimestamp = s.format(new Date());

            if (result.size() > 0) {
                if (Integer.valueOf(result.get(0).get("USER_LOGIN_TIME")) < 1 && result.get(0).get("create_datetime").substring(0, 8).equals(currentTimestamp.substring(0, 8))) {
                    System.out.println("correct");
                    logintime.update(logintime.new loginrecord_class(0, Integer.valueOf(userId), Integer.valueOf(result.get(0).get("USER_LOGIN_TIME")) + 1));
                }

                if ((Integer.valueOf(result.get(0).get("USER_LOGIN_TIME")) == 1 && result.get(0).get("create_datetime").substring(0, 8).equals(currentTimestamp.substring(0, 8)) && !rewardedLogin) || rewresult != null) {
                    System.out.println("rewards");
                    rewardedLogin = true;
                    rewards.update(rewards.new foodRewards_class(0, Integer.valueOf(userId), Integer.valueOf(rewresult.get(0).get("FOOD_REWARDS")) + 5));
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

                    ConnectivityManager cm =
                            (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();
                    if (isConnected) {
                        navfoodAsyncTask asyncTask = new navfoodAsyncTask(MainActivity.this, navFoodConstant.SELECT_REWARDS_DATA);
                        asyncTask.execute(userId);
                        //asyncTask = new navfoodAsyncTask(MainActivity.this, navFoodConstant.SAVE_REWARDS_DATA);
                        //asyncTask.execute(userId, String.valueOf(Integer.valueOf(rewresult.get(0).get("FOOD_REWARDS")) + 5), currentTimestamp);
                    }
                    ContentResolver.requestSync(null, "com.dmc.myapplication.provider", bundle);

                }

                if (!result.get(0).get("create_datetime").substring(0, 8).equals(currentTimestamp.substring(0, 8))) {
                    System.out.println("pls remove");
                    logintime.delete();
                    logintime.insert(logintime.new loginrecord_class(0, Integer.valueOf(userId), 0));
                }
            }

        } else if (savedVersionCode == DOESNT_EXIST) {


            // TODO This is a new install (or the user cleared the shared preferences)
            //  logintime.insert(logintime.new loginrecord_class(0, Integer.valueOf(userId),0));


        }
        abc++;
        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}
        switch (item.getItemId()) {
            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                //  if (fm.getBackStackEntryCount() > 0) {
                //     getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
                //     fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                //     getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                // }else{
                getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new homePageFragment())
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right).commit();
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                //}
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();

        if (id == R.id.home_page) {
            this.setTitle(R.string.home_page);
            ft.replace(R.id.content_frame, new homePageFragment()).commit();

        } else if (id == R.id.nav_blood) {
            this.setTitle(R.string.nav_blood);
            System.out.println("getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);");
            getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
            bloodMain fragment1 = new bloodMain();
            mode = R.id.nav_blood;
            ft.replace(R.id.content_frame, fragment1).addToBackStack("tag").commit();
            getSupportFragmentManager().executePendingTransactions();

        } else if (id == R.id.nav_food) {
            // 飲食日記
            navFoodBiz.startNavFood(this, navFoodTime.getVurrentDateInDBFormal(), ft);
        } else if (id == R.id.nav_bp) {
            //血壓
            this.setTitle(R.string.nav_bp);
            bpMain fragment1 = new bpMain();
            ft.replace(R.id.content_frame, fragment1).commit();

        } else if (id == R.id.nav_medicine) {
            //用藥記錄
            drugBiz.startDrug(this, drugDateTool.getVurrentDateInDBFormal(), ft);
        } else if (id == R.id.nav_dminfo) {
            //糖尿資訊
            this.setTitle(R.string.title_activity_dminfo);
            webViewFragment fragment1 = new webViewFragment();
            ft.replace(R.id.content_frame, fragment1).commit();

        } else if (id == R.id.nav_gym) {
            // 運動日記
            //gymBiz.startGym(this, gymDateTimeTool.getVurrentDateInDBFormal(), ft);
            gymNewBiz.startNewGym(this, dateTool.getVurrentDateInDBFormal(), ft);

        } else if (id == R.id.other_Index) {
            this.setTitle(R.string.other_Index);
            otherBody_Main fragment1 = new otherBody_Main();
            ft.replace(R.id.content_frame, fragment1).commit();

        } else if (id == R.id.medic_reminder) {
            //服藥提醒
            this.setTitle(R.string.medic_reminder);
            ft.replace(R.id.content_frame, new reminderFragment()).commit();

        } else if (id == R.id.nav_forum) {
            //NEW
            Calendar now = Calendar.getInstance();
            String year = Integer.toString(now.get(Calendar.YEAR));
            String weekNo = Integer.toString(now.get(Calendar.WEEK_OF_YEAR));
            assessmentBiz.startAssessment(this, year, weekNo, ft);
        } else if (id == R.id.nav_setting) {
            // 用戶設定
            this.setTitle(R.string.nav_usersetting);
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, SettingsActivity.class);
            startActivity(intent);

        } else if (id == R.id.file_export) {
            this.setTitle(R.string.file_export);
            ft.replace(R.id.content_frame, new exportFragment()).commit();

        } else if (id == R.id.nav_help) {
            this.setTitle(R.string.nav_help);
            ft.replace(R.id.content_frame, new helpFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mode = getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
        System.out.println("dddd");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        getIntent().putExtra(systemConstant.REDIRECT_PAGE, mode);
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (authentication() == true) {

        } else {
            startActivity(new Intent(MainActivity.this, welcomeActivity.class));
        }

    }

    private boolean authentication() {

        return userLocalStore.getUserLoggedIn();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        if (BluetoothState.miband != null) {
            try {
                BluetoothState.miband.disconnect();
            } catch (Exception e) {
                System.out.print(e);
            }
        }
        super.onDestroy();
    }
}
