package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.tool.localDBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 11/10/2016.
 */
public class bodyRecord {
    public static final String TABLE_NAME = "bodyRecord";

    public static final String AUTHORITY = "com.dmc.myapplication.sync.ContentProvider";
    public static final Uri bodyRecord_CONTENT_URI = Uri.parse("content://" + bodyRecord.AUTHORITY + "/" + bodyRecord.TABLE_NAME);

    public static final String bodyRecord_CONTENT_TYPE =
            "vnd.android.cursor.dir/vnd.bodyRecord_ContentProvider.data";
    public static final String bodyRecord_CONTENT_TYPE_ID =
            "vnd.android.cursor.item/vnd.bodyRecord_ContentProvider.data";

    public static final String KEY_ID = "_id";

    // 其它表格欄位名稱
    public static final String USER_ID = "USER_ID";
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    public static final String RECORD_TYPE = "RECORD_TYPE";
    public static final String HEIGHT = "HEIGHT";
    public static final String WEIGHT = "WEIGHT";
    public static final String WAIST = "WAIST";
    public static final String BMI = "BMI";
    public static final String BP_H = "BP_H";
    public static final String BP_L = "BP_L";
    public static final String HEART_RATE = "HEART_RATE";
    public static final String HbA1c = "HbA1c";
    public static final String PERIOD = "PERIOD";
    public static final String TYPE_GLUCOSE = "TYPE_GLUCOSE";
    public static final String GLUCOSE = "GLUCOSE";
    public static final String FASTING_BLOOD_SUGAR = "FASTING_BLOOD_SUGAR";
    public static final String POST_GLUCOSE = "POST_GLUCOSE";
    public static final String TOTAL_C = "TOTAL_C";
    public static final String LDL_C = "LDL_C";
    public static final String HDL_C = "HDL_C";
    public static final String TRIGLYCERIDES = "TRIGLYCERIDES";
    public static final String REMARKS = "REMARKS";
    public static final String create_datetime = "create_datetime";
    public static final String edit_datetime = "edit_datetime";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    USER_ID + " INTEGER NOT NULL, " +
                    DATE + " TEXT NOT NULL, " +
                    TIME + " TEXT NOT NULL, " +
                    RECORD_TYPE + " INTEGER NOT NULL, " +
                    HEIGHT + " REAL, " +
                    WEIGHT + " REAL, " +
                    WAIST + " REAL, " +
                    BMI + " REAL, " +
                    BP_H + " INTEGER, " +
                    BP_L + " INTEGER, " +
                    HEART_RATE + " INTEGER, " +
                    HbA1c + " REAL, " +
                    PERIOD + " INTEGER, " +
                    TYPE_GLUCOSE + " INTEGER, " +
                    GLUCOSE + " REAL, " +
                    FASTING_BLOOD_SUGAR + " REAL, " +
                    POST_GLUCOSE + " REAL, " +
                    TOTAL_C + " REAL, " +
                    LDL_C + " REAL, " +
                    HDL_C + " REAL, " +
                    TRIGLYCERIDES + " REAL, " +
                    REMARKS + " TEXT, " +
                    create_datetime + " TEXT NOT NULL, " +
                    edit_datetime + " TEXT NOT NULL)";

    private static SQLiteDatabase db;

    // 建構子，一般的應用都不需要修改
    public bodyRecord(Context context) {
        db = localDBHelper.getDatabase(context);
    }

    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        db.close();
    }


    public static BodyRecord.bodyRecord insert(BodyRecord.bodyRecord BR) {

        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put("USER_ID", BR.getUserId());
        cv.put("DATE", BR.getDate());
        cv.put("TIME", BR.getTime());
        cv.put("RECORD_TYPE", BR.getRecordType());
        cv.put("HEIGHT", BR.getHeight());
        cv.put("WEIGHT", BR.getWeight());
        cv.put("WAIST", BR.getWaist());
        cv.put("BMI", BR.getBmi());
        cv.put("BP_H", BR.getBp_h());
        cv.put("BP_L", BR.getBp_l());
        cv.put("HEART_RATE", BR.getHeart_rate());
        cv.put("HbA1c", BR.getHba1c());
        cv.put("PERIOD", BR.getPeriod());
        cv.put("TYPE_GLUCOSE", BR.getType_glucose());
        cv.put("GLUCOSE", BR.getglucose());
        cv.put("TOTAL_C", BR.getTotal_c());
        cv.put("LDL_C", BR.getLdl_c());
        cv.put("HDL_C", BR.getHdl_c());
        cv.put("TRIGLYCERIDES", BR.getTriglyceriders());
        cv.put("REMARKS", BR.getRemarks());
        if (BR.create_datetime == null || BR.create_datetime == "") {
            cv.put("create_datetime", currentTimestamp);
            cv.put("edit_datetime", currentTimestamp);
        } else {
            cv.put("create_datetime", BR.create_datetime);
            cv.put("edit_datetime", BR.edit_datetime);
        }

        long id = db.insert(TABLE_NAME, null, cv);

        BR.recordId = (int) id;
        return BR;
    }

    public static boolean update(BodyRecord.bodyRecord BR) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put("USER_ID", BR.getUserId());
        cv.put("DATE", BR.getDate());
        cv.put("TIME", BR.getTime());
        cv.put("RECORD_TYPE", BR.getRecordType());
        cv.put("HEIGHT", BR.getHeight());
        cv.put("WEIGHT", BR.getWeight());
        cv.put("WAIST", BR.getWaist());
        cv.put("BMI", BR.getBmi());
        cv.put("BP_H", BR.getBp_h());
        cv.put("BP_L", BR.getBp_l());
        cv.put("HEART_RATE", BR.getHeart_rate());
        cv.put("HbA1c", BR.getHba1c());
        cv.put("PERIOD", BR.getPeriod());
        cv.put("TYPE_GLUCOSE", BR.getType_glucose());
        cv.put("GLUCOSE", BR.getglucose());
        cv.put("TOTAL_C", BR.getTotal_c());
        cv.put("LDL_C", BR.getLdl_c());
        cv.put("HDL_C", BR.getHdl_c());
        cv.put("TRIGLYCERIDES", BR.getTriglyceriders());
        cv.put("REMARKS", BR.getRemarks());
        cv.put("edit_datetime", currentTimestamp);
        String where = KEY_ID + "=" + BR.getRecordId();
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public boolean delete(long id) {
        String where = KEY_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public BodyRecord.bodyRecord get(long id) {
        BodyRecord.bodyRecord BR = null;
        String where = KEY_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public BodyRecord.bodyRecord getByCreateDatetime(String createDatetime) {
        BodyRecord.bodyRecord BR = null;
        String where = create_datetime + "=" + createDatetime;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime() {

        List<HashMap<String, String>> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            BodyRecord.bodyRecord br = getRecord(cursor);
            newL.put("create_datetime", br.create_datetime);
            newL.put("edit_datetime", br.edit_datetime);
            result.add(newL);
            br = null;
            newL = null;
        }

        cursor.close();
        return result;
    }

    public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList) {
        List<HashMap<String, String>> result = new ArrayList<>();
        for (int x = 0; x < requestedList.size(); x++) {
            BodyRecord.bodyRecord objectBR = getByCreateDatetime(requestedList.get(x));
            result.add(objectBR.toHashMap());
        }

        return result;
    }

    public BodyRecord.bodyRecord getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        BodyRecord a = new BodyRecord();
        BodyRecord.bodyRecord result = a.new bodyRecord(
                cursor.getInt(0),
                cursor.getInt(4),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getDouble(5),
                cursor.getDouble(6),
                cursor.getDouble(7),
                cursor.getDouble(8),
                cursor.getInt(9),
                cursor.getInt(10),
                cursor.getInt(11),
                cursor.getDouble(12),
                cursor.getInt(13),
                cursor.getInt(14),
                cursor.getDouble(15),
                cursor.getDouble(18),
                cursor.getDouble(19),
                cursor.getDouble(20),
                cursor.getDouble(21),
                cursor.getString(22)
        );
        result.create_datetime = cursor.getString(23);
        result.edit_datetime = cursor.getString(24);
        /*result.setId(cursor.getLong(0));
        result.setDrugName(cursor.getString(1));
        result.setManyTime(cursor.getInt(2));
        result.setTime1(cursor.getString(3));
        result.setTime2(cursor.getString(4));
        result.setTime3(cursor.getString(5));
        result.setTime4(cursor.getString(6));
        result.setBroadcastNumber1(cursor.getString(7));
        result.setBroadcastNumber2(cursor.getString(8));
        result.setBroadcastNumber3(cursor.getString(9));
        result.setBroadcastNumber4(cursor.getString(10));
        result.setRemark(cursor.getString(11));
        result.setPhotoPath(cursor.getString(12));*/

        // 回傳結果
        return result;
    }

    public List<BodyRecord.bodyRecord> getAllBp() {
        List<BodyRecord.bodyRecord> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            if (cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_BP_TYPE_CODE)) {
                result.add(getRecord(cursor));
            }
        }

        Collections.sort(result,
                new Comparator<BodyRecord.bodyRecord>() {
                    public int compare(BodyRecord.bodyRecord o1, BodyRecord.bodyRecord o2) {
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            Date a = format1.parse(o1.getDate() + " " + o1.getTime());
                            Date b = format1.parse(o2.getDate() + " " + o2.getTime());
                            return b.compareTo(a);
                        } catch (Exception e) {
                            return 0;
                        }
                    }
                });


        cursor.close();
        return result;
    }

    public BodyRecord.bodyRecord getLastGLUCOSE() {
        List<BodyRecord.bodyRecord> list = getAllGLUCOSE();
        return list.get(0);
    }

    public List<BodyRecord.bodyRecord> getAllGLUCOSE() {
        List<BodyRecord.bodyRecord> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            if (cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_TYPE_CODE)) {
                result.add(getRecord(cursor));
            }
        }

        Collections.sort(result,
                new Comparator<BodyRecord.bodyRecord>() {
                    public int compare(BodyRecord.bodyRecord o1, BodyRecord.bodyRecord o2) {
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            Date a = format1.parse(o1.getDate() + " " + o1.getTime());
                            Date b = format1.parse(o2.getDate() + " " + o2.getTime());
                            return b.compareTo(a);
                        } catch (Exception e) {
                            return 0;
                        }
                    }
                });

        cursor.close();
        return result;
    }

    public List<BodyRecord.bodyRecord> getAllBMI() {
        List<BodyRecord.bodyRecord> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            if (cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_BMI_TYPE_CODE)) {
                result.add(getRecord(cursor));
            }
        }

        cursor.close();
        return result;
    }

    public List<BodyRecord.bodyRecord> getAllREPORT() {
        List<BodyRecord.bodyRecord> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            if (cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE) || cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_HBA1C_TYPE_CODE) || cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE) || cursor.getInt(4) == Integer.parseInt(bodyRecordConstant.GET_REMARK_TYPE_CODE)) {
                result.add(getRecord(cursor));
            }
        }

        cursor.close();
        return result;
    }


}
