package com.dmc.myapplication.Models;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.tool.localDBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by januslin on 11/10/2016.
 */
public class bodyRecord_ContentProvider extends ContentProvider {


    private static final UriMatcher uriMatcher;

    private static final int TABLE = 0;  // All rows
    private static final int TABLE_ROW = 1;  //Single row;

    private localDBHelper databaseHelper;
    private SQLiteDatabase db;


    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(bodyRecord.AUTHORITY , bodyRecord.TABLE_NAME , TABLE);
        uriMatcher.addURI(bodyRecord.AUTHORITY , bodyRecord.TABLE_NAME + "/#" , TABLE_ROW);
    }


    @Override
    public boolean onCreate() {
        databaseHelper = new localDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        db = databaseHelper.getReadableDatabase();

        Cursor cursor;

        switch (uriMatcher.match(uri)){
            case TABLE :
                cursor = db.query(bodyRecord.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case TABLE_ROW :
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(bodyRecord.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
        }

        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        switch (uriMatcher.match(uri)){
            case TABLE:
                return bodyRecord.bodyRecord_CONTENT_TYPE;
            case TABLE_ROW  :
                return bodyRecord.bodyRecord_CONTENT_TYPE_ID;
        }

        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        db = databaseHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case TABLE:
                long row_id = db.insert(bodyRecord.TABLE_NAME , null , values);
                Uri _uri = ContentUris.withAppendedId(uri , row_id);
                //getContext().getContentResolver().notifyChange(_uri , null);
                return _uri;
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

}
