package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.tool.localDBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by januslin on 24/12/2016.
 */
public class exercise_type {
    public static final String TABLE_NAME = "EXERCISE_TYPE";

    public static final String EXERCISE_TYPE_ID = "EXERCISE_TYPE_ID";

    // 其它表格欄位名稱
    public static final String EXERCISE_TYPE_NAME = "EXERCISE_TYPE_NAME";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            EXERCISE_TYPE_ID + " INTEGER PRIMARY KEY, " +
            EXERCISE_TYPE_NAME + " TEXT NOT NULL)";

    private static SQLiteDatabase db;

    public exercise_type(Context context) {
        db = localDBHelper.getDatabase(context);
    }

    public void close() {
        db.close();
    }


    public exercise_type_class get(long id) {
        exercise_type_class BR = null;
        String where = EXERCISE_TYPE_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public boolean delete(long id){
        String where = EXERCISE_TYPE_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean deleteAll(){
        return db.delete(TABLE_NAME, null, null) > 0;
    }

    public exercise_type_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        exercise_type_class result = new exercise_type_class(
                cursor.getInt(0),
                cursor.getString(1)
        );

        // 回傳結果
        return result;
    }

    public exercise_type_class insert(exercise_type_class BR) {
        ContentValues cv = new ContentValues();
        cv.put(EXERCISE_TYPE_ID, BR.getEXERCISE_TYPE_ID());
        cv.put(EXERCISE_TYPE_NAME, BR.getEXERCISE_TYPE_NAME());
        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public List<exercise_type_class> getAll() {
        List<exercise_type_class> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

        //Class
    public class exercise_type_class{
        private Integer EXERCISE_TYPE_ID;
        private String EXERCISE_TYPE_NAME;

        public exercise_type_class(Integer inEXERCISE_TYPE_ID, String inEXERCISE_TYPE_NAME){
            this.EXERCISE_TYPE_ID = inEXERCISE_TYPE_ID;
            this.EXERCISE_TYPE_NAME = inEXERCISE_TYPE_NAME;
        }

        public Integer getEXERCISE_TYPE_ID(){
            return EXERCISE_TYPE_ID;
        }

        public String getEXERCISE_TYPE_NAME(){
            return EXERCISE_TYPE_NAME;
        }

    }
}
