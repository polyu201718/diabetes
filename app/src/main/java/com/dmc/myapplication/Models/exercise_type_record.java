package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.tool.localDBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 24/12/2016.
 */
public class exercise_type_record {
    public static final String TABLE_NAME = "EXERCISE_TYPE_RECORD";

    public static final String EXERCISE_RECORD_ID = "EXERCISE_RECORD_ID";
    public static final String USER_ID = "USER_ID";
    public static final String EXERCISE_DATE = "EXERCISE_DATE";
    public static final String EXERCISE_TIME = "EXERCISE_TIME";
    public static final String EXERCISE_TYPE_ID = "EXERCISE_TYPE_ID";
    public static final String EXERCISE_PERIOD = "EXERCISE_PERIOD";
    public static final String create_datetime = "create_datetime";
    public static final String edit_datetime = "edit_datetime";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            EXERCISE_RECORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_ID + " INTEGER NOT NULL, " +
            EXERCISE_DATE + " TEXT NOT NULL, " +
            EXERCISE_TIME + " TEXT NOT NULL, " +
            EXERCISE_TYPE_ID + " INTEGER NOT NULL, " +
            EXERCISE_PERIOD + " INTEGER NOT NULL, " +
            create_datetime + " TEXT NOT NULL, " +
            edit_datetime + " TEXT NOT NULL)";

    private static SQLiteDatabase db;

    public exercise_type_record(Context context) {
        db = localDBHelper.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public boolean delete(long id){
        String where = EXERCISE_RECORD_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public exercise_type_record_class insert(exercise_type_record_class BR){

        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(EXERCISE_DATE, BR.getEXERCISE_DATE());
        cv.put(EXERCISE_TIME, BR.getEXERCISE_TIME());
        cv.put(EXERCISE_TYPE_ID, BR.getEXERCISE_TYPE_ID());
        cv.put(EXERCISE_PERIOD, BR.getEXERCISE_PERIOD());

        if (BR.create_datetime == null || BR.create_datetime == ""){
            cv.put("create_datetime", currentTimestamp);
            cv.put("edit_datetime", currentTimestamp);
        }else{
            cv.put("create_datetime", BR.create_datetime);
            cv.put("edit_datetime", BR.edit_datetime);
        }

        long id = db.insert(TABLE_NAME, null, cv);

        BR.EXERCISE_RECORD_ID = (int) id;
        return BR;
    }

    public boolean update(exercise_type_record_class BR) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(EXERCISE_DATE, BR.getEXERCISE_DATE());
        cv.put(EXERCISE_TIME, BR.getEXERCISE_TIME());
        cv.put(EXERCISE_TYPE_ID, BR.getEXERCISE_TYPE_ID());
        cv.put(EXERCISE_PERIOD, BR.getEXERCISE_PERIOD());

        cv.put("edit_datetime", currentTimestamp);
        String where = EXERCISE_RECORD_ID + "=" + BR.getEXERCISE_RECORD_ID();
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public exercise_type_record_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        exercise_type_record_class result = new exercise_type_record_class(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getInt(4),
                cursor.getInt(5)
                );
        result.create_datetime = cursor.getString(6);
        result.edit_datetime = cursor.getString(7);

        // 回傳結果
        return result;
    }

    public exercise_type_record_class get(long id) {
        exercise_type_record_class BR = null;
        String where = EXERCISE_RECORD_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime(){

        List<HashMap<String, String>> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            exercise_type_record_class br = getRecord(cursor);
            newL.put("create_datetime", br.create_datetime);
            newL.put("edit_datetime", br.edit_datetime);
            result.add(newL);
            br = null;
            newL = null;
        }

        cursor.close();
        return result;
    }

    public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList){
        List<HashMap<String, String>> result = new ArrayList<>();
        for (int x = 0; x < requestedList.size(); x++){
            exercise_type_record_class objectBR = getByCreateDatetime(requestedList.get(x));
            result.add(objectBR.toHashMap());
        }

        return result;
    }


    public exercise_type_record_class getByCreateDatetime(String createDatetime) {
        exercise_type_record_class BR = null;
        String where = create_datetime + "=" + createDatetime;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<exercise_type_record_class> getByDate(String iDate) {
        exercise_type_record_class BR = null;
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        List<exercise_type_record_class> output = new ArrayList<>();
        while (result.moveToNext()) {
            exercise_type_record_class obj = getRecord(result);
            System.out.println("obj.getEXERCISE_DATE() = "+obj.getEXERCISE_DATE());
            System.out.println("iDate = "+iDate);

            if (obj.getEXERCISE_DATE().equals(iDate)){
                output.add(obj);
            }

        }
        result.close();
        return output;
    }

    //Class
    public class exercise_type_record_class {
        private Integer EXERCISE_RECORD_ID;
        private Integer USER_ID;
        private String EXERCISE_DATE;
        private String EXERCISE_TIME;
        private Integer EXERCISE_TYPE_ID;
        private Integer EXERCISE_PERIOD;
        public String create_datetime;
        public String edit_datetime;

        public exercise_type_record_class(Integer inEXERCISE_RECORD_ID, Integer inUser_ID, String inEXERCISE_DATE, String inEXERCISE_TIME, Integer inEXERCISE_TYPE_ID, Integer inEXERCISE_PERIOD){
            this.EXERCISE_RECORD_ID = inEXERCISE_RECORD_ID;
            this.USER_ID = inUser_ID;
            this.EXERCISE_DATE = inEXERCISE_DATE;
            this.EXERCISE_TIME = inEXERCISE_TIME;
            this.EXERCISE_TYPE_ID = inEXERCISE_TYPE_ID;
            this.EXERCISE_PERIOD = inEXERCISE_PERIOD;
        }

        public Integer getEXERCISE_RECORD_ID(){
            return EXERCISE_RECORD_ID;
        }

        public Integer getUSER_ID(){
            return USER_ID;
        }

        public String getEXERCISE_DATE(){
            return EXERCISE_DATE;
        }

        public String getEXERCISE_TIME(){
            return EXERCISE_TIME;
        }

        public Integer getEXERCISE_TYPE_ID(){
            return EXERCISE_TYPE_ID;
        }

        public Integer getEXERCISE_PERIOD(){
            return EXERCISE_PERIOD;
        }

        public String getEXERCISE_TYPE_NAME() {
            exercise_type etdb = new exercise_type(MainActivity.getContext);
            exercise_type.exercise_type_class etc = etdb.get(this.EXERCISE_TYPE_ID);
            return etc.getEXERCISE_TYPE_NAME();
        }

        public HashMap<String, String> toHashMap(){
            HashMap<String, String> a = new HashMap<>();
            a.put("USER_ID", String.valueOf(USER_ID));
            a.put("EXERCISE_DATE", String.valueOf(EXERCISE_DATE));
            a.put("EXERCISE_TIME", String.valueOf(EXERCISE_TIME));
            a.put("EXERCISE_TYPE_ID", String.valueOf(EXERCISE_TYPE_ID));
            a.put("EXERCISE_PERIOD", String.valueOf(EXERCISE_PERIOD));
            a.put("create_datetime", String.valueOf(create_datetime));
            a.put("edit_datetime", String.valueOf(edit_datetime));
            return a;
        }
    }
}
