package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmc.myapplication.tool.foodAssessmentDB;
import com.dmc.myapplication.tool.foodRecordDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lamivan on 14/11/2017.
 */

public class foodAssessment {
  public static final String TABLE_NAME = "FOOD_ASSESSMENT_RECORD";

  public static final String FOOD_ASSESS_ID = "FOOD_ASSESS_ID";
  public static final String EMPTY = "FOOD_EMPTY";
  public static final String USER_ID = "USER_ID";
  public static final String FOOD_ASSESS_DATE = "FOOD_ASSESS_DATE";
  public static final String FOOD_RATING = "FOOD_RATING";
  // TODO: Save in the format of "1,2,3,4,..." to indicate selection of option 1,2,3,4
  public static final String FOOD_QUESTION_2 = "FOOD_Q2";
  public static final String FOOD_QUESTION_3a = "FOOD_Q3";
  // TODO: Save in the format of "fooda,foodb;foodc,foodd..."
  public static final String FOOD_QUESTION_3b1 = "FOOD_Q3B1";
  public static final String FOOD_QUESTION_3b2 = "FOOD_Q3B2";
  public static final String FOOD_QUESTION_3b3 = "FOOD_Q3B3";
  public static final String create_datetime = "create_datetime";
  public static final String edit_datetime = "edit_datetime";

  public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
          FOOD_ASSESS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          USER_ID + " INTEGER NOT NULL, " +
          FOOD_ASSESS_DATE + " TEXT NOT NULL, " +
          FOOD_RATING + " REAL NOT NULL, " +
          FOOD_QUESTION_2 + " TEXT NULL, " +
          FOOD_QUESTION_3a + " TEXT NULL, " +
          FOOD_QUESTION_3b1 + " TEXT NULL, " +
          FOOD_QUESTION_3b2 + " TEXT NULL, " +
          FOOD_QUESTION_3b3 + " TEXT NULL, " +
          EMPTY + " INTEGER NOT NULL, " +
          create_datetime + " TEXT NOT NULL, " +
          edit_datetime + " TEXT NOT NULL)";

  private static SQLiteDatabase db;

  public foodAssessment(Context context) {
    db = foodAssessmentDB.getDatabase(context);
  }
  public foodAssessment.foodAssess_class insert(foodAssessment.foodAssess_class BR, String create_datetime) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());
    String where = create_datetime + " LIKE ?";

    ContentValues cv = new ContentValues();
    cv.put(USER_ID, BR.getUSER_ID());
    cv.put(FOOD_ASSESS_DATE, BR.getFOOD_ASSESS_DATE());
    cv.put(FOOD_RATING, BR.getFOOD_RATING());
    cv.put(FOOD_QUESTION_2, BR.getFOOD_Q2());
    cv.put(FOOD_QUESTION_3a, BR.getFOOD_Q3a());
    cv.put(FOOD_QUESTION_3b1, BR.getFOOD_Q3b1());
    cv.put(FOOD_QUESTION_3b2, BR.getFOOD_Q3b2());
    cv.put(FOOD_QUESTION_3b3, BR.getFOOD_Q3b3());
    cv.put(EMPTY, 1);
    if (BR.create_datetime == null || BR.create_datetime == "") {
      cv.put("create_datetime", currentTimestamp);
      cv.put("edit_datetime", currentTimestamp);
    } else {
      cv.put("create_datetime", BR.create_datetime);
      cv.put("edit_datetime", BR.edit_datetime);
    }

    long id = db.update(TABLE_NAME, cv, where, new String[] {create_datetime + "%"});
    return BR;
  }

  public foodAssessment.foodAssess_class getRecord(Cursor cursor) {
    // 準備回傳結果用的物件

    foodAssessment.foodAssess_class result = new foodAssessment.foodAssess_class(
            cursor.getInt(0),
            cursor.getInt(1),
            cursor.getString(2),
            cursor.getInt(3),
            cursor.getString(4),
            cursor.getString(5),
            cursor.getString(6),
            cursor.getString(7),
            cursor.getString(8));

    result.create_datetime = cursor.getString(9);
    result.edit_datetime = cursor.getString(10);

    // 回傳結果
    return result;
  }
  public foodAssessment.foodAssess_class getByCreateDatetime(String createDatetime) {
    foodAssessment.foodAssess_class BR = null;
    String where = create_datetime + " LIKE ?";
    Cursor result = db.query(TABLE_NAME, null, where, new String[] {createDatetime + "%"}, null, null, null, null);

    if (result.moveToFirst()) {
      BR = getRecord(result);
    } else if (result.getCount() == 0 || !result.moveToFirst()){
      BR = new foodAssess_class();
    }
    result.close();
    return BR;
  }
  public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList) {
    List<HashMap<String, String>> result = new ArrayList<>();
    for (int x = 0; x < requestedList.size(); x++) {
      foodAssessment.foodAssess_class objectBR = getByCreateDatetime(requestedList.get(x));
      result.add(objectBR.toHashMap());
    }

    return result;
  }

  public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime() {

    List<HashMap<String, String>> result = new ArrayList<>();
    Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

    while (cursor.moveToNext()) {
      HashMap<String, String> newL = new HashMap<>();
      foodAssessment.foodAssess_class br = getRecord(cursor);
      newL.put("create_datetime", br.create_datetime);
      newL.put("edit_datetime", br.edit_datetime);
      result.add(newL);
      br = null;
      newL = null;
    }

    cursor.close();
    return result;
  }

  public boolean delete() {
    return db.delete(TABLE_NAME, null, null) > 0;
  }

  public void close() {
    db.close();
  }

  public class foodAssess_class {
    private Integer FOOD_ASSESS_ID;
    private Integer USER_ID;
    private String FOOD_ASSESS_DATE;
    private float FOOD_RATING;
    private String FOOD_Q2;
    private String FOOD_Q3a;
    private String FOOD_Q3b1;
    private String FOOD_Q3b2;
    private String FOOD_Q3b3;
    public String create_datetime;
    public String edit_datetime;
    private Integer isEmpty;

    public Integer getFOOD_ASSESS_ID() {
      return FOOD_ASSESS_ID;
    }

    public Integer getUSER_ID() {
      return USER_ID;
    }

    public String getFOOD_ASSESS_DATE() {
      return FOOD_ASSESS_DATE;
    }

    public float getFOOD_RATING() {
      return FOOD_RATING;
    }

    public String getFOOD_Q2() {
      return FOOD_Q2;
    }

    public String getFOOD_Q3a() {
      return FOOD_Q3a;
    }
    public String getFOOD_Q3b1() {
      return FOOD_Q3b1;
    }
    public String getFOOD_Q3b2() {
      return FOOD_Q3b2;
    }
    public String getFOOD_Q3b3() {
      return FOOD_Q3b3;
    }
    public foodAssess_class(Integer FOOD_ASSESS_ID, Integer USER_ID, String FOOD_ASSESS_DATE, float FOOD_RATING, String FOOD_Q2, String FOOD_Q3a,String FOOD_Q3b1, String FOOD_Q3b2 , String FOOD_Q3b3) {
      this.FOOD_RATING = FOOD_RATING;
      this.FOOD_Q2 = FOOD_Q2;
      this.FOOD_ASSESS_DATE = FOOD_ASSESS_DATE;
      this.FOOD_Q3a = FOOD_Q3a;
      this.FOOD_Q3b1 = FOOD_Q3b1;
      this.FOOD_Q3b2 = FOOD_Q3b2;
      this.FOOD_Q3b3 = FOOD_Q3b3;
      this.FOOD_ASSESS_ID = FOOD_ASSESS_ID;
      this.USER_ID = USER_ID;
      this.isEmpty = 1;
    }
    public foodAssess_class() {
      this.isEmpty = 0;
      SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
      this.create_datetime = s.format(new Date());
    }
    public HashMap<String, String> toHashMap() {

      HashMap<String, String> out = new HashMap<>();

      out.put(foodAssessment.create_datetime, String.valueOf(this.create_datetime));
      out.put(foodAssessment.EMPTY, String.valueOf(this.isEmpty));
      if (isEmpty == 1) {
        out.put(foodAssessment.FOOD_ASSESS_ID, String.valueOf(this.FOOD_ASSESS_ID));
        out.put(foodAssessment.USER_ID, String.valueOf(this.USER_ID));
        out.put(foodAssessment.FOOD_ASSESS_DATE, String.valueOf(this.FOOD_ASSESS_DATE));
        out.put(foodAssessment.FOOD_RATING, String.valueOf(this.FOOD_RATING));
        out.put(foodAssessment.FOOD_QUESTION_2, String.valueOf(this.FOOD_Q2));
        out.put(foodAssessment.FOOD_QUESTION_3a, String.valueOf(this.FOOD_Q3a));
        out.put(foodAssessment.FOOD_QUESTION_3b1, String.valueOf(this.FOOD_Q3b1));
        out.put(foodAssessment.FOOD_QUESTION_3b2, String.valueOf(this.FOOD_Q3b2));
        out.put(foodAssessment.FOOD_QUESTION_3b3, String.valueOf(this.FOOD_Q3b3));
        out.put(foodAssessment.edit_datetime, String.valueOf(this.edit_datetime));
      }
      return out;

    }

  }
}