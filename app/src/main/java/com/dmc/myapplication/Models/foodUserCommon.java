package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.tool.foodUserCommonDB;
import com.dmc.myapplication.tool.xiaomiDeviceSettingDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by januslin on 24/12/2016.
 */
public class foodUserCommon {
    public static final String TABLE_NAME = "FOOD_USER_COMMON";

    public static final String USER_ID	 = "USER_ID";
    public static final String FOOD_ID = "FOOD_ID";
    public static final String FOOD_COMMON_COUNT = "FOOD_COMMON_COUNT";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            FOOD_ID + " INTEGER PRIMARY KEY, " +
            USER_ID + " INTEGER, " +
            FOOD_COMMON_COUNT + " INTEGER);";

    private static SQLiteDatabase db;

    public foodUserCommon(Context context) {
        db = foodUserCommonDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public foodUserCommon_class insert(foodUserCommon_class BR){
        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(FOOD_ID, BR.getFOOD_ID());
        cv.put(FOOD_COMMON_COUNT, BR.getFOOD_COMMON_COUNT());

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public boolean update(foodUserCommon_class BR) {
        ContentValues cv = new ContentValues();
        cv.put(FOOD_COMMON_COUNT, BR.getFOOD_COMMON_COUNT());

        String where = FOOD_ID + "='" + BR.getFOOD_ID() + "'";
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public foodUserCommon_class getByFoodId(Integer id) {
        foodUserCommon_class BR = null;
        String where = FOOD_ID + "='" + id + "'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        //result.close();
        return BR;
    }

    public List<foodUserCommon_class> getAll() {
        foodUserCommon_class BR = null;
        //Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);
        Cursor result = db.rawQuery("SELECT * FROM "+TABLE_NAME+" ORDER BY "+FOOD_COMMON_COUNT+" DESC LIMIT 5",null);
        List<foodUserCommon_class> out = new ArrayList<>();

        while (result.moveToNext()){
            out.add(getRecord(result));
        }

        //result.close();
        return out;
    }


    public foodUserCommon_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        foodUserCommon_class result = new foodUserCommon_class(
                cursor.getInt(1),
                cursor.getInt(0),
                cursor.getInt(2)
                );
        System.out.println("getRecord 0="+cursor.getString(0) + " 1="+cursor.getString(1));
        // 回傳結果
        return result;
    }

    public class foodUserCommon_class{
        private Integer USER_ID;
        private Integer FOOD_ID;
        private Integer FOOD_COMMON_COUNT;

        public foodUserCommon_class(Integer USER_ID, Integer FOOD_ID, Integer FOOD_COMMON_COUNT) {
            this.USER_ID = USER_ID;
            this.FOOD_ID = FOOD_ID;
            this.FOOD_COMMON_COUNT = FOOD_COMMON_COUNT;
        }

        public Integer getUSER_ID() {
            return USER_ID;
        }

        public void setUSER_ID(Integer USER_ID) {
            this.USER_ID = USER_ID;
        }

        public Integer getFOOD_ID() {
            return FOOD_ID;
        }

        public void setFOOD_ID(Integer FOOD_ID) {
            this.FOOD_ID = FOOD_ID;
        }

        public Integer getFOOD_COMMON_COUNT() {
            return FOOD_COMMON_COUNT;
        }

        public void setFOOD_COMMON_COUNT(Integer FOOD_COMMON_COUNT) {
            this.FOOD_COMMON_COUNT = FOOD_COMMON_COUNT;
        }

        public food.food_class getFoodClass(Context context){
            if (this.FOOD_ID!=null){
                food foodDB = new food(context);
                food.food_class out = foodDB.get(FOOD_ID);
                foodDB.close();
                return out;

            }
            return null;
        }
    }
}
