package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.tool.foodAssessmentDB;
import com.dmc.myapplication.tool.rewardsDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lamivan on 25/12/2017.
 */

public class rewards {
  public static final String TABLE_NAME = "FOOD_ASSESSMENT_RECORD";

  public static final String FOOD_REWARD_ID = "FOOD_REWARD_ID";
  public static final String USER_ID = "USER_ID";
  public static final String FOOD_REWARDS = "FOOD_REWARDS";
  public static final String create_datetime = "create_datetime";
  public static final String edit_datetime = "edit_datetime";

  public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
          FOOD_REWARD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          USER_ID + " INTEGER NOT NULL, " +
          FOOD_REWARDS + " INTEGER NOT NULL, " +
          create_datetime + " TEXT NOT NULL, " +
          edit_datetime + " TEXT NOT NULL)";

  private static SQLiteDatabase db;

  public rewards(Context context) {
    db = rewardsDB.getDatabase(context);
  }

  public rewards.foodRewards_class insert(rewards.foodRewards_class BR) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());

    ContentValues cv = new ContentValues();
    cv.put(USER_ID, BR.getUSER_ID());
    cv.put(FOOD_REWARDS, BR.getFOOD_REWARDS());
    if (BR.create_datetime == null || BR.create_datetime == "") {
      cv.put("create_datetime", currentTimestamp);
      cv.put("edit_datetime", currentTimestamp);
    } else {
      cv.put("create_datetime", BR.create_datetime);
      cv.put("edit_datetime", BR.edit_datetime);
    }

    long id = db.insert(TABLE_NAME, null, cv);
    return BR;
  }

  public boolean update(rewards.foodRewards_class BR) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());

    ContentValues cv = new ContentValues();
    cv.put(FOOD_REWARDS, BR.getFOOD_REWARDS());
    cv.put("edit_datetime", currentTimestamp);
    String where = USER_ID + "=" + BR.getUSER_ID();
    return db.update(TABLE_NAME, cv, where, null) > 0;
  }

  public rewards.foodRewards_class getRecord(Cursor cursor) {
    // 準備回傳結果用的物件

    rewards.foodRewards_class result = new rewards.foodRewards_class(
            cursor.getInt(0),
            cursor.getInt(1),
            cursor.getInt(2));

    result.create_datetime = cursor.getString(3);
    result.edit_datetime = cursor.getString(4);

    // 回傳結果
    return result;
  }

  public boolean delete() {
    return db.delete(TABLE_NAME, null, null) > 0;
  }

  public rewards.foodRewards_class getByCreateDatetime(String createDatetime) {
    rewards.foodRewards_class BR = null;
    String where = create_datetime + " LIKE ?";
    Cursor result = db.query(TABLE_NAME, null, where, new String[] {createDatetime + "%"}, null, null, null, null);

    if (result.moveToFirst()) {
      BR = getRecord(result);
    } else if (result.getCount() == 0 || !result.moveToFirst()){
      BR = new rewards.foodRewards_class();
    }
    result.close();
    return BR;
  }
  public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList) {
    List<HashMap<String, String>> result = new ArrayList<>();
    for (int x = 0; x < requestedList.size(); x++) {
      rewards.foodRewards_class objectBR = getByCreateDatetime(requestedList.get(x));
      result.add(objectBR.toHashMap());
    }

    return result;
  }

  public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime() {

    List<HashMap<String, String>> result = new ArrayList<>();
    Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

    while (cursor.moveToNext()) {
      HashMap<String, String> newL = new HashMap<>();
      rewards.foodRewards_class br = getRecord(cursor);
      newL.put(FOOD_REWARDS, String.valueOf(br.getFOOD_REWARDS()));
      newL.put("create_datetime", br.create_datetime);
      newL.put("edit_datetime", br.edit_datetime);
      result.add(newL);
      br = null;
      newL = null;
    }

    cursor.close();
    return result;
  }
  public void close() {
    db.close();
  }

  public class foodRewards_class {
    private Integer FOOD_REWARDS_ID;
    private Integer USER_ID;
    public String create_datetime;
    public String edit_datetime;
    private Integer FOOD_REWARDS;

    public String getCreate_datetime() {
      return create_datetime;
    }

    public void setCreate_datetime(String create_datetime) {
      this.create_datetime = create_datetime;
    }

    public String getEdit_datetime() {
      return edit_datetime;
    }

    public void setEdit_datetime(String edit_datetime) {
      this.edit_datetime = edit_datetime;
    }

    public foodRewards_class() {
    }

    public foodRewards_class(Integer FOOD_REWARDS_ID, Integer USER_ID, Integer FOOD_REWARDS) {
      this.FOOD_REWARDS_ID = FOOD_REWARDS_ID;
      this.USER_ID = USER_ID;
      this.FOOD_REWARDS = FOOD_REWARDS;
    }

    public Integer getFOOD_REWARDS_ID() {
      return FOOD_REWARDS_ID;
    }

    public void setFOOD_REWARDS_ID(Integer FOOD_REWARDS_ID) {
      this.FOOD_REWARDS_ID = FOOD_REWARDS_ID;
    }

    public Integer getUSER_ID() {
      return USER_ID;
    }

    public void setUSER_ID(Integer USER_ID) {
      this.USER_ID = USER_ID;
    }

    public Integer getFOOD_REWARDS() {
      return FOOD_REWARDS;
    }

    public void setFOOD_REWARDS(Integer FOOD_REWARDS) {
      this.FOOD_REWARDS = FOOD_REWARDS;
    }

    public HashMap<String, String> toHashMap() {

      HashMap<String, String> out = new HashMap<>();

        out.put(rewards.FOOD_REWARD_ID, String.valueOf(this.FOOD_REWARDS_ID));
        out.put(rewards.USER_ID, String.valueOf(this.USER_ID));
        out.put(rewards.FOOD_REWARDS, String.valueOf(this.FOOD_REWARDS));
        out.put(rewards.create_datetime, String.valueOf(this.create_datetime));
        out.put(rewards.edit_datetime, String.valueOf(this.edit_datetime));

      return out;

    }

  }
}
