package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.tool.stepRecordDB;
import com.dmc.myapplication.tool.xiaomiDeviceSettingDB;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 24/12/2016.
 */
public class stepRecord {
    public static final String TABLE_NAME = "STEP_RECORD";

    public static final String ID	 = "ID";
    public static final String USER_ID = "USER_ID";
    public static final String DATE = "DATE";
    public static final String STEP_COUNT = "STEP_COUNT";
    public static final String create_datetime = "create_datetime";
    public static final String edit_datetime = "edit_datetime";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_ID + " INTEGER NOT NULL, " +
            DATE + " TEXT NOT NULL, " +
            STEP_COUNT + " INTEGER NOT NULL, " +
            create_datetime + " TEXT NOT NULL, " +
            edit_datetime + " TEXT NOT NULL);";

    private static SQLiteDatabase db;

    public stepRecord(Context context) {
        db = stepRecordDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public stepRecord_class insert(stepRecord_class BR){

        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(DATE, BR.getDATE());
        cv.put(STEP_COUNT, BR.getSTEP_COUNT());

        if (BR.getCreate_datetime() != null){
            cv.put(create_datetime, BR.getCreate_datetime());
            cv.put(edit_datetime, BR.getEdit_datetime());
        }else{
            cv.put(create_datetime, currentTimestamp);
            cv.put(edit_datetime, currentTimestamp);
        }

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public int getTodayLatestStep(){
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String currentTimestamp = s.format(new Date());
        stepRecord_class obj = getByDate(currentTimestamp);
        if (obj == null) {
            return 0;
        }else{
            return obj.getSTEP_COUNT();
        }
    }

    public void insertOrUpdate(Integer step, Context context){

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String currentTimestamp = s.format(new Date());
        stepRecord_class obj = getByDate(currentTimestamp);
        if (obj == null){
            UserLocalStore user = new UserLocalStore(context);
            insert(new stepRecord_class(0, user.getLoggedInUser().userid, currentTimestamp, step));
        }else{
            update(new stepRecord_class(obj.getID(), obj.getUSER_ID(), obj.getDATE(), step));
        }
    }

    public boolean update(stepRecord_class BR) {

        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(DATE, BR.getDATE());
        cv.put(STEP_COUNT, BR.getSTEP_COUNT());
        cv.put(edit_datetime, currentTimestamp);

        String where = ID + "='" + BR.getID() + "'";
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList){
        List<HashMap<String, String>> result = new ArrayList<>();
        for (int x = 0; x < requestedList.size(); x++){
            stepRecord_class objectBR = getByCreateDatetime(requestedList.get(x));
            result.add(objectBR.toHashMap());
        }

        return result;
    }

    public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime(){

        List<HashMap<String, String>> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            stepRecord_class br = getRecord(cursor);
            newL.put("create_datetime", br.create_datetime);
            newL.put("edit_datetime", br.edit_datetime);
            result.add(newL);
            br = null;
            newL = null;
        }

        cursor.close();
        return result;
    }
    public stepRecord_class getByCreateDatetime(String createDatetime) {
        stepRecord_class BR = null;
        String where = create_datetime + "=" + createDatetime;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public stepRecord_class get(Integer id) {
        stepRecord_class BR = null;
        String where = ID + "='" + id + "'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        //result.close();
        return BR;
    }

    public List<stepRecord_class> getAllDataDesc() {
        List<stepRecord_class> output = new ArrayList<>();

        Cursor result = db.rawQuery("SELECT * FROM "+TABLE_NAME+" ORDER BY date("+DATE+") DESC",null);

        while (result.moveToNext()){
            output.add(getRecord(result));
        }

        return output;
    }

    public List<stepRecord_class> getLatest7daysrecord() {
        List<stepRecord_class> output = new ArrayList<>();

        Date date = new Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        for (int a = 6; a >=0; a--){
            stepRecord_class newday = getByDate(s.format(new DateTime(date).minusDays(a).toDate()));

            if (newday != null){
                output.add(newday);
            }else {
                output.add(new stepRecord_class(0, 0, s.format(new DateTime(date).minusDays(a).toDate()), 0));
            }
        }

        return output;
    }

    public stepRecord_class getByDate(String date) {
        stepRecord_class BR = null;
        String where = DATE + "='" + date + "'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        //result.close();
        return BR;
    }

    public stepRecord_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        stepRecord_class result = new stepRecord_class(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getInt(3)
        );

        result.setCreate_datetime(cursor.getString(4));
        result.setEdit_datetime(cursor.getString(4));
        System.out.println("getRecord 0="+cursor.getString(0) + " 1="+cursor.getString(1));
        // 回傳結果
        return result;
    }

    public class stepRecord_class{
        public Integer ID;
        public Integer USER_ID;
        public String DATE;
        public Integer STEP_COUNT;
        public String create_datetime;
        public String edit_datetime;

        public stepRecord_class(Integer ID, Integer USER_ID, String DATE, Integer STEP_COUNT) {
            this.ID = ID;
            this.USER_ID = USER_ID;
            this.DATE = DATE;
            this.STEP_COUNT = STEP_COUNT;
        }

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public Integer getUSER_ID() {
            return USER_ID;
        }

        public void setUSER_ID(Integer USER_ID) {
            this.USER_ID = USER_ID;
        }

        public String getDATE() {
            return DATE;
        }

        public void setDATE(String DATE) {
            this.DATE = DATE;
        }

        public Integer getSTEP_COUNT() {
            return STEP_COUNT;
        }

        public void setSTEP_COUNT(Integer STEP_COUNT) {
            this.STEP_COUNT = STEP_COUNT;
        }

        public String getCreate_datetime() {
            return create_datetime;
        }

        public void setCreate_datetime(String create_datetime) {
            this.create_datetime = create_datetime;
        }

        public String getEdit_datetime() {
            return edit_datetime;
        }

        public void setEdit_datetime(String edit_datetime) {
            this.edit_datetime = edit_datetime;
        }

        public HashMap<String, String> toHashMap(){
            HashMap<String, String> output = new HashMap<>();

            output.put("ID", String.valueOf(ID));
            output.put("USER_ID", String.valueOf(USER_ID));
            output.put("DATE", String.valueOf(DATE));
            output.put("STEP_COUNT", String.valueOf(STEP_COUNT));
            output.put("create_datetime", String.valueOf(create_datetime));
            output.put("edit_datetime", String.valueOf(edit_datetime));

            return output;
        }
    }
}
