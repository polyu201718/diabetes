package com.dmc.myapplication;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDexApplication;

import com.dmc.myapplication.phyActivityRecord.db.dao.DaoMaster;
import com.dmc.myapplication.phyActivityRecord.db.dao.DaoSession;

import java.util.TimeZone;

/**
 * Created by terry on 2/1/2018.
 */

public class MyApplication extends MultiDexApplication {

    private static MyApplication application;
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase phyDB;
    private DaoMaster mPhyDaoMaster;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        // Force GMT+8 Hong Kong Time
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Hong_Kong"));

        openPhyDB();

        // start upload
        // now handle by upload service
        //PhyUploadIntentService.startUploadAll();
    }

    public static MyApplication getApplication() {
        return application;
    }

    private void openPhyDB() {
        mHelper = new DaoMaster.DevOpenHelper(this, "phy.db", null);
        phyDB = mHelper.getWritableDatabase();
        mPhyDaoMaster = new DaoMaster(phyDB);
    }

    public DaoSession getDaoSession() {
        return mPhyDaoMaster.newSession();
    }


    public SQLiteDatabase getPhyDb() {
        return phyDB;
    }

}