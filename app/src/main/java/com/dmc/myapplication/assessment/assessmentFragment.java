package com.dmc.myapplication.assessment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.WindowManager;
import android.content.res.Configuration;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.dmc.myapplication.R;
import com.dmc.myapplication.goal.GoalInsertActivity;
import com.dmc.myapplication.login.UserLocalStore;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * Created by KwokSinMan on 18/3/2016.
 */
public class assessmentFragment extends Fragment {
    String userId;
    LoginButton share;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // ignore the font scale here
      //  adjustFontScale(getResources().getConfiguration());

        View v = inflater.inflate(R.layout.assessment_main, container, false);

        String year = this.getArguments().getString(assessmentConstant.GET_ASSESSMENT_YEAR);
        String weekNo = this.getArguments().getString(assessmentConstant.GET_ASSESSMENT_WEEK);

        //System.out.println("Candy getArguments year="+year);
        //System.out.println("Candy getArguments weekNo=" + weekNo);

        UserLocalStore userLocalStore = new UserLocalStore(getActivity());
        userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

        List<weekObject> weekList = new ArrayList<weekObject>();
        assessmentBiz biz = new assessmentBiz();
        weekObject thisWeek = biz.getWeekByNumber(0, "本星期");
        weekObject lastWeek = biz.getWeekByNumber(-1, "過去一星期");
        weekObject last2Week = biz.getWeekByNumber(-2, "過去兩個星期");
        weekObject last3Week = biz.getWeekByNumber(-3, "過去三個星期");

        weekList.add(thisWeek);
        weekList.add(lastWeek);
        weekList.add(last2Week);
        weekList.add(last3Week);

        ArrayAdapter<weekObject> weekListAdapter = new ArrayAdapter<weekObject>(getActivity(), R.layout.navfood_spinner_center_item, weekList);
        weekListAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner weekSpinner = (Spinner) v.findViewById(R.id.weekSpinner);
        weekSpinner.setAdapter(weekListAdapter);
        weekSpinner.setOnItemSelectedListener(new weekDDLOnItemSelectedListener() );
        weekSpinner.setEnabled(false);
        assessmentAsyncTask connect = new assessmentAsyncTask(getActivity(),assessmentConstant.GET_ASSESSMENT_RECORD);
        if (year.equals(lastWeek.getYear()) &&  weekNo.equals(lastWeek.getWeek())){
            weekSpinner.setSelection(1); // set last Week
            connect.execute(userId,lastWeek.getWeek(),lastWeek.getYear(),lastWeek.getStartWeekDate(),lastWeek.getEndWeekDate());
        }else if (year.equals(last2Week.getYear()) &&  weekNo.equals(last2Week.getWeek())){
            weekSpinner.setSelection(2); // set last 2 Week
            connect.execute(userId, last2Week.getWeek(), last2Week.getYear(), last2Week.getStartWeekDate(), last2Week.getEndWeekDate());
        }else  if (year.equals(last3Week.getYear()) &&  weekNo.equals(last3Week.getWeek())){
            weekSpinner.setSelection(3); // set last 3 Week
            connect.execute(userId, last3Week.getWeek(), last3Week.getYear(), last3Week.getStartWeekDate(), last3Week.getEndWeekDate());
        }else{
            weekSpinner.setSelection(0); // set this week
            connect.execute(userId, thisWeek.getWeek(), thisWeek.getYear(), thisWeek.getStartWeekDate(), thisWeek.getEndWeekDate());
        }
       /* Button test = (Button) v.findViewById(R.id.test);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), assessmentAddActivity.class);

                String year = ((weekObject) (((Spinner) getActivity().findViewById(R.id.weekSpinner)).getSelectedItem())).getYear();
                String weekNo = ((weekObject) (((Spinner) getActivity().findViewById(R.id.weekSpinner)).getSelectedItem())).getWeek();
                //System.out.println("Candy week=" + weekNo);
                //System.out.println("Candy year=" + year);

                intent.putExtra(assessmentConstant.GET_ASSESSMENT_WEEK, weekNo);
                intent.putExtra(assessmentConstant.GET_ASSESSMENT_YEAR, year);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });*/
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        shareDialog = new ShareDialog(this);
        final boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
        share =  (LoginButton) v.findViewById(R.id.login_button);
        share.setReadPermissions("email"); //, "user_managed_groups");
        share.setTextLocale(Locale.TAIWAN);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!loggedIn) {
                    try {
                        PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                                "com.dmc.myapplication",
                                PackageManager.GET_SIGNATURES);
                        for (android.content.pm.Signature signature : info.signatures) {
                            MessageDigest md = MessageDigest.getInstance("SHA");
                            md.update(signature.toByteArray());
                            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                        }
                    } catch (PackageManager.NameNotFoundException e) {

                    } catch (NoSuchAlgorithmException e) {

                    }
                    // LoginManager.getInstance().logInWithReadPermissions(GoalInsertActivity.this, Arrays.asList("user_managed_groups"));
                    // LoginManager.getInstance().logInWithPublishPermissions(GoalInsertActivity.this, Arrays.asList("publish_actions"));
                }


                    if (!share.getText().toString().equals("登出")) {
                       // ScrollView view2 = (ScrollView) getActivity().findViewById(R.id.scrollView);
                       // view2.scrollTo(0, 0);
                        View view1 = getActivity().getWindow().getDecorView().getRootView();
                        Bitmap image = screenShot(view1);
                        if (ShareDialog.canShow(ShareLinkContent.class)) {
                            SharePhoto photo = new SharePhoto.Builder()
                                    .setBitmap(image)
                                    .build();
                            SharePhotoContent content = new SharePhotoContent.Builder()
                                    .addPhoto(photo)
                                    .build();
                            shareDialog.show(content);
                        }
                    }}});

        return v;
    }
    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.assessment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.assessment_menuItem_add) {
            CharSequence colors[] = new CharSequence[]{getString(R.string.title_activity_assessment_food_add)};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("增加記錄");
            builder.setItems(colors, new doFoodAssessmentOnClickListener(getActivity()));
            builder.show();
         }
        return true;
    }

    private class weekDDLOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapter,View view,int position,long id) {
            weekObject selectedWeekObject = ((weekObject) adapter.getItemAtPosition(position));
            assessmentAsyncTask connect = new assessmentAsyncTask(getActivity(),assessmentConstant.GET_ASSESSMENT_RECORD);
            connect.execute(userId, selectedWeekObject.getWeek(), selectedWeekObject.getYear(), selectedWeekObject.getStartWeekDate(), selectedWeekObject.getEndWeekDate());
        }

        @Override // 什么也没选
        public void onNothingSelected(AdapterView<?> arg0) {}
    }

    private class doFoodAssessmentOnClickListener implements   DialogInterface.OnClickListener {
        Activity activity;
        public doFoodAssessmentOnClickListener(Activity activity){
            this.activity = activity;
        }
        @Override
        public void onClick(DialogInterface dialog, int id) {
            if (id == 0) { //服藥記錄
                Intent intent = new Intent();
                intent.setClass(activity, assessmentAddActivity.class);

                String year = ((weekObject) (((Spinner) activity.findViewById(R.id.weekSpinner)).getSelectedItem())).getYear();
                String weekNo = ((weekObject) (((Spinner) activity.findViewById(R.id.weekSpinner)).getSelectedItem())).getWeek();
                //System.out.println("Candy week=" + weekNo);
                //System.out.println("Candy year=" + year);

                intent.putExtra(assessmentConstant.GET_ASSESSMENT_WEEK, weekNo);
                intent.putExtra(assessmentConstant.GET_ASSESSMENT_YEAR, year);
                activity.startActivity(intent);
                activity.finish();
            }
        }
    }


}
