package com.dmc.myapplication.assessment;

/**
 * Created by KwokSinMan on 27/3/2016.
 */
public class weekObject {
    private String year;
    private String week;
    private String label;
    private String startWeekDate;
    private String endWeekDate;

    public weekObject(String year, String week, String label, String startWeekDate, String endWeekDate){
        this.year = year;
        this.week = week;
        this.label = label;
        this.startWeekDate = startWeekDate;
        this.endWeekDate = endWeekDate;
    }

    public String getYear() {
        return year;
    }


    public void setYear(String year) {
        this.year = year;
    }


    public String getWeek() {
        return week;
    }


    public void setWeek(String week) {
        this.week = week;
    }


    public String getLabel() {
        return label;
    }


    public void setLabel(String label) {
        this.label = label;
    }


    public String getStartWeekDate() {
        return startWeekDate;
    }


    public void setStartWeekDate(String startWeekDate) {
        this.startWeekDate = startWeekDate;
    }


    public String getEndWeekDate() {
        return endWeekDate;
    }


    public void setEndWeekDate(String endWeekDate) {
        this.endWeekDate = endWeekDate;
    }

    public String toAllString(){
        return "label="+label+" year="+year +" week="+week+" startWeekDate="+ startWeekDate+" endWeekDate="+endWeekDate;
    }

    public String toString(){
        return label;
    }
}
