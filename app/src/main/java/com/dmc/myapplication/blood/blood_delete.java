package com.dmc.myapplication.blood;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;


import java.util.List;

/**
 * Created by Po on 3/3/2016.
 */
public class blood_delete  extends AppCompatActivity {

    private Context ctx;
    private TextView editTextBloodValue ;
    private Spinner spinnerPeriod, spinnerType;
    private String recordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blood_del);
        ctx = getApplicationContext();



        Intent intent =getIntent();
        recordId = intent.getStringExtra("recordId");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");
        final int getPeriodGlu = Integer.parseInt(intent.getStringExtra("typePeriod"));
        final int getTypeGlu = Integer.parseInt(intent.getStringExtra("typeGlu"));
        final String getGluValue = intent.getStringExtra("gluValue");

        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date
        final TextView editTextDate = (TextView) findViewById(R.id.blood_del_date);
        editTextDate.setEnabled(false);
        editTextDate.setText(getDate);

        //handle the time
        final TextView editTextTime = (TextView) findViewById(R.id.blood_del_time);
        editTextTime.setEnabled(false);
        editTextTime.setText(getTime);


        //handle the period spinner drop down menu
        spinnerPeriod = (Spinner) findViewById(R.id.spinnerPeriod);

        // Spinner Drop down elements
        List<BodyRecord.periodType> period = record.getPeriodTypeList() ;
        ArrayAdapter<BodyRecord.periodType> adp= new ArrayAdapter<BodyRecord.periodType>(this,
                android.R.layout.simple_list_item_1,period);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPeriod.setAdapter(adp);
        spinnerPeriod.setSelection(getPeriodGlu - 1);
        spinnerPeriod.setEnabled(false);


        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        // Spinner Drop down elements
        List<BodyRecord.gluType> gluType = record.getGluTypeList() ;
        ArrayAdapter<BodyRecord.gluType> adp1= new ArrayAdapter<BodyRecord.gluType>(this,
                android.R.layout.simple_list_item_1,gluType);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adp1);
        spinnerType.setSelection(getTypeGlu - 1);
        spinnerType.setEnabled(false);


        //handle the blood value field
        editTextBloodValue = (TextView) findViewById(R.id.blood_del_value);
        editTextBloodValue.setText(getGluValue);
        editTextBloodValue.setEnabled(false);

        Button buttonBloodDel = (Button) findViewById(R.id.blood_del_button);
        buttonBloodDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new deleteDialogFragment(recordId);
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        Button buttonBloodEdit = (Button) findViewById(R.id.blood_edit_button);
        buttonBloodEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getBloodEditRecord( recordId,  getDate,  getTime,  Integer.toString(getPeriodGlu),Integer.toString(getTypeGlu),  getGluValue);
            }
        });

    }


    public void getBloodEditRecord( String recordId, String date, String time, String typePeriod, String typeGlu, String gluValue){

        Intent intent = new Intent();
        intent.setClass(this, blood_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("typePeriod", typePeriod);
        intent.putExtra("typeGlu", typeGlu);
        intent.putExtra("gluValue", gluValue);
        this.startActivity(intent);
        this.finish();
    }


    private class deleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private String recordId;
        public deleteDialogFragment (String recordId){
            this.recordId = recordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //bodyAsyncTask delRecord = new bodyAsyncTask(getActivity(), bodyRecordConstant.DELETE_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                    //String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                    //delRecord.execute(recordId, record_type);

                                    bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                                    BodyRecord a = new BodyRecord();
                                    bodyRecord.delete(Integer.parseInt(recordId));
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), MainActivity.class);
                                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
