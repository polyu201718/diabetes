package com.dmc.myapplication.bodyRecord;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by Po on 21/3/2016.
 */
public class injectionAsyncTask extends AsyncTask<String,Void,String> {

    private ProgressDialog progressBar;
    private String model;
    private Activity activity;

    public injectionAsyncTask(Activity activity, String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        //System.out.println("[injectionAsyncTask -Run at doInBackground] ");
        String result= null;
        String data="";
        try {

                String userId = (String)arg0[0];

                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");



            //get result  from server
            //dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
            //result = dmasyncTask.getDataFromServer(data);
            return "{}";

       // } catch (SocketTimeoutException e){
        //    e.printStackTrace();
        //    return dmAppAsyncTask.SERVER_SLEEP;
        } catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
        //System.out.println("[injectionAsyncTask - result] "+result);
        //return result;
    }

    @Override
    protected void onPostExecute(String result){
        if (result!=null) {
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else {
                BodyRecord.getInjectionRecordNew(activity.getBaseContext());
                progressBar.dismiss();
            }
        }
    }

}
