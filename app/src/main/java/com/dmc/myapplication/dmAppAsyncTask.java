package com.dmc.myapplication;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by KwokSinMan on 1/2/2016.
 */
public class dmAppAsyncTask {


    public final static String NO_INTERNET_CONNECTION = "bp_add_buttonbp_add_button";
    public final static String SERVER_SLEEP = "SERVER_SLEEP";

    private String model;
    public dmAppAsyncTask(String model){
        this.model = model;
    }

    public String getDataFromServer(String data) throws Exception{

        String link = systemConstant.SERVER_ADDRESS + model;
        //System.out.println("link="+link);
        URL url = new URL(link);

        BufferedReader reader;
        if(!systemConstant.SERVER_ADDRESS.contains("https")) { // run http
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(systemConstant.CONNECTION_TIMEOUT);
            conn.setReadTimeout(systemConstant.CONNECTION_TIMEOUT);
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        }else{
            //https
            trustAllHosts();
            HttpsURLConnection conn =  (HttpsURLConnection)url.openConnection();
            HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            conn.setHostnameVerifier(DO_NOT_VERIFY);
            conn.setConnectTimeout(systemConstant.CONNECTION_TIMEOUT);
            conn.setReadTimeout(systemConstant.CONNECTION_TIMEOUT);
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        }

        StringBuilder sb = new StringBuilder();
        String line = null;

        // Read Server Response
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    private void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
