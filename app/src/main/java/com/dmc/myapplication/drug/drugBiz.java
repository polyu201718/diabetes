package com.dmc.myapplication.drug;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.support.v4.app.FragmentTransaction;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.INJECTION_RECORD;
import com.dmc.myapplication.Models.MEDICATION_RECORD;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.tool.timeObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by KwokSinMan on 7/2/2016.
 */
public class drugBiz {

    public static boolean hasInjection(String InjectionCode){
        if(InjectionCode!=null && InjectionCode.equals("Y")){
            return true;
        }
        else{
            return false;
        }
    }

    public static void startDrug(Activity activity,String drugDateUI, FragmentTransaction ft){
        activity.setTitle(R.string.nav_medicine);
        Bundle bundle = new Bundle();
        bundle.putString(drugConstant.GET_DRUG_DATE, drugDateUI);
        drugFragment drugFrag = new drugFragment();
        drugFrag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, drugFrag).addToBackStack(null).commit();
    }

    public void afterSaveDrugRecord(Activity activity) {
        String getDrugDate = activity.getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_DRUG);
        intent.putExtra(drugConstant.GET_DRUG_DATE, getDrugDate);
        activity.startActivity(intent);
        activity.finish();
    }


    public void afterSaveInjectionRecord(Activity activity, Boolean isDeleteAction){
        String getDrugDate = activity.getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        if (!isDeleteAction){
            //System.out.println("Candy gymDate has run");
            getDrugDate = dateTool.getDBDateFormal((String) ((TextView) activity.findViewById(R.id.injectionDate)).getText());
        }
        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_DRUG);
        intent.putExtra(drugConstant.GET_DRUG_DATE, getDrugDate);
        activity.startActivity(intent);
        activity.finish();
    }

    public void setDrugRecord(Activity activity, Fragment fragment, String result) {
        //System.out.println("Candy setDrugRecord result=" + result);
        if (!result.isEmpty()) {
            try{
                JSONArray JA = new JSONArray(result);
                String ans = JA.getJSONObject(0).getString("HAS_TAKE_MEDICATION");
                LinearLayout noDrugRecord = (LinearLayout) activity.findViewById(R.id.noDrugRecord);
                noDrugRecord.setContentDescription(ans);
                if(!ans.equals(drugConstant.NO_RECORD)) {
                    noDrugRecord.setVisibility(View.GONE);
                    LinearLayout drugRecord = (LinearLayout) activity.findViewById(R.id.drugDetail);
                    drugRecord.setVisibility(View.VISIBLE);
                    RadioGroup questionRadioGroup = (RadioGroup) activity.findViewById(R.id.question);
                    if (ans.equals(drugConstant.ANS_YES)) {
                        questionRadioGroup.check(R.id.ansY);
                        RadioButton radioButtonY = (RadioButton) activity.findViewById(R.id.ansY);
                        radioButtonY.setVisibility(View.VISIBLE);
                    }
                    if (ans.equals(drugConstant.ANS_NO)) {
                        questionRadioGroup.check(R.id.ansN);
                        RadioButton radioButtonN = (RadioButton) activity.findViewById(R.id.ansN);
                        radioButtonN.setVisibility(View.VISIBLE);
                        String isInjectionSharePre = new UserLocalStore(activity).getLoggedInUser().injection;
                        if(drugBiz.hasInjection(isInjectionSharePre)){
                            fragment.setHasOptionsMenu(true);
                        }else{
                            fragment.setHasOptionsMenu(false);
                        }
                    }
                } else{
                    noDrugRecord.setVisibility(View.VISIBLE);
                    fragment.setHasOptionsMenu(true);
                }
                if(JA.length()>1){
                    LinearLayout noInjectionRecord = (LinearLayout) activity.findViewById(R.id.noInjectionRecord);
                    noInjectionRecord.setVisibility(View.GONE);
                    getInjectionList(activity,new JSONArray(result), 1);
                }else{
                    LinearLayout noInjectionRecord = (LinearLayout) activity.findViewById(R.id.noInjectionRecord);
                    noInjectionRecord.setVisibility(View.VISIBLE);
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void getInjectionList(Activity activity, JSONArray JA, int count) throws JSONException {
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
            for (int i = count; i < JA.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                timeObject timeObject = new timeObject(JA.getJSONObject(i).getString("INJECTION_TIME"));
                map.put("id", JA.getJSONObject(i).getString("INJECTION_RECORD_ID"));
                map.put("Time", timeObject.getUITimeFormal());
                map.put("Name", JA.getJSONObject(i).getString("INJECTION_NAME"));
                map.put("Value", JA.getJSONObject(i).getString("INJECTION_VALUE"));
                mylist.add(map);
            }
        ListView injectionList = (ListView) activity.findViewById(R.id.injectionList);
        SimpleAdapter gymAdapter = new SimpleAdapter(activity, mylist, R.layout.drug_injection_record_list, new String[]{"Time","Name","Value"}, new int[]{R.id.injectionTime,R.id.injectionName, R.id.injectionValue});
        injectionList.setAdapter(gymAdapter);
    }

    private void getInjectionListNew(Activity activity, List<HashMap<String, String>> JA, int count) throws JSONException {
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        for (int i = count; i < JA.size(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            timeObject timeObject = new timeObject(JA.get(i).get("INJECTION_TIME"));
            map.put("id", JA.get(i).get("INJECTION_RECORD_ID"));
            map.put("Time", timeObject.getUITimeFormal());
            map.put("Name", JA.get(i).get("INJECTION_NAME"));
            map.put("Value", JA.get(i).get("INJECTION_VALUE"));
            mylist.add(map);
        }
        ListView injectionList = (ListView) activity.findViewById(R.id.injectionList);
        SimpleAdapter gymAdapter = new SimpleAdapter(activity, mylist, R.layout.drug_injection_record_list, new String[]{"Time","Name","Value"}, new int[]{R.id.injectionTime,R.id.injectionName, R.id.injectionValue});
        injectionList.setAdapter(gymAdapter);
    }

    public void setDrugRecordNew(Activity activity, Fragment fragment, String result, String drugdate) {
        //System.out.println("Candy setDrugRecord result=" + result);
        List<HashMap<String, String>> resultList = new ArrayList<>();

        MEDICATION_RECORD MEDICATION_RECORD = new MEDICATION_RECORD(activity.getBaseContext());
        List<MEDICATION_RECORD.MEDICATION_RECORD_class> MEDICATION_RECORDList = MEDICATION_RECORD.getAllrecordbyrecorddate(drugdate);

        if (MEDICATION_RECORDList.size() <= 0){
            HashMap<String, String> temp = new HashMap<>();
            temp.put("HAS_TAKE_MEDICATION", "NA");
            resultList.add(temp);
        }else{
            for (int x = 0; x < MEDICATION_RECORDList.size(); x++){
                resultList.add(MEDICATION_RECORDList.get(x).toHashMap());
            }
        }

        MEDICATION_RECORD = null;
        MEDICATION_RECORDList = null;

        INJECTION_RECORD INJECTION_RECORD = new INJECTION_RECORD(activity.getBaseContext());
        List<com.dmc.myapplication.Models.INJECTION_RECORD.INJECTION_RECORD_class> INJECTION_RECORDList = INJECTION_RECORD.getAllrecordbyrecorddate(drugdate);

        for (int x = 0; x < INJECTION_RECORDList.size(); x++){
            resultList.add(INJECTION_RECORDList.get(x).toHashMap());
        }

        INJECTION_RECORD = null;
        INJECTION_RECORDList = null;

        if (resultList.size()>0) {
            try{
                //JSONArray JA = new JSONArray(result);
                String ans = resultList.get(0).get("HAS_TAKE_MEDICATION");
                LinearLayout noDrugRecord = (LinearLayout) activity.findViewById(R.id.noDrugRecord);
                noDrugRecord.setContentDescription(ans);
                if(!ans.equals(drugConstant.NO_RECORD)) {
                    noDrugRecord.setVisibility(View.GONE);
                    LinearLayout drugRecord = (LinearLayout) activity.findViewById(R.id.drugDetail);
                    drugRecord.setVisibility(View.VISIBLE);
                    RadioGroup questionRadioGroup = (RadioGroup) activity.findViewById(R.id.question);
                    if (ans.equals(drugConstant.ANS_YES)) {
                        questionRadioGroup.check(R.id.ansY);
                        RadioButton radioButtonY = (RadioButton) activity.findViewById(R.id.ansY);
                        radioButtonY.setVisibility(View.VISIBLE);
                    }
                    if (ans.equals(drugConstant.ANS_NO)) {
                        questionRadioGroup.check(R.id.ansN);
                        RadioButton radioButtonN = (RadioButton) activity.findViewById(R.id.ansN);
                        radioButtonN.setVisibility(View.VISIBLE);
                        String isInjectionSharePre = new UserLocalStore(activity).getLoggedInUser().injection;
                        if(drugBiz.hasInjection(isInjectionSharePre)){
                            fragment.setHasOptionsMenu(true);
                        }else{
                            fragment.setHasOptionsMenu(false);
                        }
                    }
                } else{
                    noDrugRecord.setVisibility(View.VISIBLE);
                    fragment.setHasOptionsMenu(true);
                }
                if(resultList.size()>1){
                    LinearLayout noInjectionRecord = (LinearLayout) activity.findViewById(R.id.noInjectionRecord);
                    noInjectionRecord.setVisibility(View.GONE);
                    getInjectionListNew(activity,resultList, 1);
                }else{
                    LinearLayout noInjectionRecord = (LinearLayout) activity.findViewById(R.id.noInjectionRecord);
                    noInjectionRecord.setVisibility(View.VISIBLE);
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    // set questionRadioGroupOnCheckedChangeListener
    //questionRadioGroup.setOnCheckedChangeListener(new radioOnCheckedChangeListener(activity));

    private class radioOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {
        Activity activity;
        public radioOnCheckedChangeListener(Activity activity){ this.activity=activity; }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            UserLocalStore userLocalStore = new UserLocalStore(activity);
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
            String drugDate = drugDateTool.getDBDateFormal((String) ((TextView) activity.findViewById(R.id.drugViewDate)).getText());
            // find which radio button is selected
            if (checkedId == R.id.ansY) {
                drugAsyncTask connect = new drugAsyncTask(activity, null, drugConstant.CHANGE_DRUG_RECORD);
                connect.execute(userId, drugDate, drugConstant.ANS_YES);
            } else if (checkedId == R.id.ansN) {
                drugAsyncTask connect = new drugAsyncTask(activity, null, drugConstant.CHANGE_DRUG_RECORD);
                connect.execute(userId, drugDate, drugConstant.ANS_NO);
            }
        }
    }

    public void drugAndInjection(Activity activity){
        CharSequence colors[] = new CharSequence[]{"服藥記錄", "注射胰島素記錄"};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("增加記錄");
        builder.setItems(colors, new drugAndInjectionOnClickListener(activity));
        builder.show();
    }

    public void onlyInjection(Activity activity){
        CharSequence colors[] = new CharSequence[]{"注射胰島素記錄"};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("增加記錄");
        builder.setItems(colors, new onlyInjectionOnClickListener(activity));
        builder.show();
    }


    private class drugAndInjectionOnClickListener implements   DialogInterface.OnClickListener {
        Activity activity;
        public drugAndInjectionOnClickListener(Activity activity){
            this.activity = activity;
        }
        @Override
        public void onClick(DialogInterface dialog, int id) {
            if (id == 0) { //服藥記錄
                Intent intent = new Intent();
                intent.setClass(activity, drugAddActivity.class);
                intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) activity.findViewById(R.id.drugViewDate)).getText()));
                activity.startActivity(intent);
                activity.finish();
            } else if (id == 1) {
                Intent intent = new Intent();
                intent.setClass(activity, injectionAddActivty.class);
                intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) activity.findViewById(R.id.drugViewDate)).getText()));
                activity.startActivity(intent);
                activity.finish();
            }
        }
    }

    private class onlyInjectionOnClickListener implements   DialogInterface.OnClickListener {
        Activity activity;
        public onlyInjectionOnClickListener(Activity activity){
            this.activity = activity;
        }
        @Override
        public void onClick(DialogInterface dialog, int id) {
            if (id == 0) {
                Intent intent = new Intent();
                intent.setClass(activity, injectionAddActivty.class);
                intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) activity.findViewById(R.id.drugViewDate)).getText()));
                activity.startActivity(intent);
                activity.finish();
            }
        }
    }

}
