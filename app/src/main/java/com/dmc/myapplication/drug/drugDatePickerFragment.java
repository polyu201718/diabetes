package com.dmc.myapplication.drug;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.*;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.Calendar;

/**
 * Created by januslin on 26/2/2017.
 */
public class drugDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    public Fragment myFragment = this;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayDate = (String)((TextView) getActivity().findViewById(R.id.drugViewDate)).getText();
        String [] displayDateArray = displayDate.split("/");
        int yy = Integer.parseInt(displayDateArray[2]);
        int mm = Integer.parseInt(displayDateArray[1]) - 1; // because Jan =0
        int dd = Integer.parseInt(displayDateArray[0]);
        DatePickerDialog dialog =  new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, yy,mm,dd);
        dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        return dialog;
    }

    // On the date picker , to confirm the following action
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        // set display date
        mm = mm + 1; // because Jan =0
        String displayAsDate = dd + "/" + mm + "/" + yy;
        ((TextView) getActivity().findViewById(R.id.drugViewDate)).setText(displayAsDate);

        // clear all setting
        //RadioGroup questionRadioGroup = (RadioGroup) getActivity().findViewById(R.id.question);
        //questionRadioGroup.setOnCheckedChangeListener(null);
        //questionRadioGroup.clearCheck();
        LinearLayout drugRecord = (LinearLayout) getActivity().findViewById(R.id.drugDetail);
        drugRecord.setVisibility(View.GONE);
        RadioButton radioButtonY = (RadioButton) getActivity().findViewById(R.id.ansY);
        radioButtonY.setVisibility(View.GONE);
        RadioButton radioButtonN = (RadioButton) getActivity().findViewById(R.id.ansN);
        radioButtonN.setVisibility(View.GONE);
        //LinearLayout drugAddArea = (LinearLayout) getActivity().findViewById(R.id.drugAddArea);
        //drugAddArea.setVisibility(View.GONE);
        //myFragment.setHasOptionsMenu(false);

        String isInjectionSharePre = new UserLocalStore(getActivity()).getLoggedInUser().injection;
        if(drugBiz.hasInjection(isInjectionSharePre)){
            myFragment.setHasOptionsMenu(true);
        }else{
            myFragment.setHasOptionsMenu(false);
        }

        LinearLayout noDrugRecord = (LinearLayout) getActivity().findViewById(R.id.noDrugRecord);
        noDrugRecord.setVisibility(View.GONE);
        LinearLayout noInjectionRecord = (LinearLayout) getActivity().findViewById(R.id.noInjectionRecord);
        noInjectionRecord.setVisibility(View.GONE);
        ListView injectionList = (ListView) getActivity().findViewById(R.id.injectionList);
        injectionList.setAdapter(null);

        UserLocalStore userLocalStore = new UserLocalStore(getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        String drugDate = yy + "-" + mm + "-" +dd;
        drugAsyncTask connect = new drugAsyncTask(getActivity(), myFragment, drugConstant.GET_DRUG_RECORD);
        connect.execute(userId, drugDate);

    }
}
