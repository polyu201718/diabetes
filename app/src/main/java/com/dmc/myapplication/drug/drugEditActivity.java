package com.dmc.myapplication.drug;

import android.app.Activity;
import android.app.Dialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

/**
 * Created by KwokSinMan on 8/3/2016.
 */
public class drugEditActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drug_add);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set Date
        String drugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        TextView drugDateTextView = (TextView)findViewById(R.id.drugViewDate);
        drugDateTextView.setText(drugDateTool.getUIDateFormal(drugDate));

        // hidden other session
        //LinearLayout drugAddArea = (LinearLayout) findViewById(R.id.drugAddArea);
        //drugAddArea.setVisibility(View.GONE);
        LinearLayout noDrugRecord = (LinearLayout) findViewById(R.id.noDrugRecord);
        noDrugRecord.setVisibility(View.GONE);
        LinearLayout editRecordButtonSession = (LinearLayout) findViewById(R.id.finishAddRecordButtonSession);
        editRecordButtonSession.setVisibility(View.GONE);
        ImageView setDrugDate = (ImageView) findViewById(R.id.setDrugViewDate);
        setDrugDate.setVisibility(View.GONE);

        // set broder of drug record
        LinearLayout drugRecord = (LinearLayout) findViewById(R.id.drugDetail);
        drugRecord.setBackgroundResource(R.drawable.border_drug_normal);

        // get record of this date
        UserLocalStore userLocalStore = new UserLocalStore(this);
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        drugAsyncTask connect = new drugAsyncTask(this, null, drugConstant.GET_DRUG_RECORD);
        connect.execute(userId, drugDate);

        // set button drugAddUpdateRecordListener
        Button drugEditButton = (Button) findViewById(R.id.drugEdit);
        drugEditButton.setOnClickListener(new drugAddUpdateRecordListener());

        //set button drug delete Record Listener
        Button drugDeleteButton = (Button) findViewById(R.id.drugDelete);
        drugDeleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new drugRecordDeleteDialogFragment();
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });
        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getDrugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_DRUG);
        intent.putExtra(drugConstant.GET_DRUG_DATE, getDrugDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    private class drugRecordDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Activity activity = (Activity) getActivity();
                                    UserLocalStore userLocalStore = new UserLocalStore(activity);
                                    String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                                    String drugDate =  activity.getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
                                    drugAsyncTask connect = new drugAsyncTask(activity, null, drugConstant.DELETE_DRUG_RECORD);
                                    connect.execute(userId, drugDate);
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }


}
