package com.dmc.myapplication.drug;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.dateTool;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class injectionEditActivity extends AppCompatActivity {
    String injectionRecordId="";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.injection_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        injectionRecordId = intent.getExtras().getString("injection_id");
        String injectionDate =getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);

        TextView injectionDateTextView = (TextView)findViewById(R.id.injectionDate);
        injectionDateTextView.setText(drugDateTool.getUIDateFormal(injectionDate));

        TextView injectionTimeTextView = (TextView)findViewById(R.id.injectionTime);
        injectionTimeTextView.setText(intent.getExtras().getString("injection_time"));

        EditText injectionNameTextView = (EditText)findViewById(R.id.injectionName);
        injectionNameTextView.setText(intent.getExtras().getString("injection_name"));

        TextView injectionValueTextView = (TextView)findViewById(R.id.injectionValue);
        injectionValueTextView.setText(intent.getExtras().getString("injection_value"));

        // hidden other session
        LinearLayout addRecordButtonSession = (LinearLayout) findViewById(R.id.addRecordButtonSession);
        addRecordButtonSession.setVisibility(View.GONE);

        // set data picker
        ImageView setInjectionDate = (ImageView) findViewById(R.id.setInjectionDate);
        setInjectionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new injectionDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set data picker
        ImageView setInjectionTime = (ImageView) findViewById(R.id.setInjectionTime);
        setInjectionTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new injectionTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set number  picker
        ImageView setInjectionValue = (ImageView) findViewById(R.id.setInjectionValue);
        setInjectionValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new injectionNumberPickerFragment();
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        EditText injectionNameEditText = (EditText) findViewById(R.id.injectionName);
        injectionNameEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String drugName = ((EditText) findViewById(R.id.injectionName)).getText().toString();
                //System.out.println("Candy drugNameEditText=" + drugName);
                Button addButton = (Button) findViewById(R.id.injectionEdit);
                if (drugName.isEmpty()) {
                    addButton.setEnabled(false);
                } else {
                    addButton.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        Button editButton = (Button) findViewById(R.id.injectionEdit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserLocalStore userLocalStore = new UserLocalStore(getActivityContentView(view));
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                String injectionDate = dateTool.getDBDateFormal((String) ((TextView) findViewById(R.id.injectionDate)).getText());
                String injectionTime = (String) ((TextView) findViewById(R.id.injectionTime)).getText();
                String injectionName =  ((EditText) findViewById(R.id.injectionName)).getText().toString();
                String injectionValue = (String) ((TextView) findViewById(R.id.injectionValue)).getText();

                //System.out.println("injectionDate="+injectionDate);
                //System.out.println("injectionTime="+injectionTime);
                //System.out.println("injectionName="+injectionName);
                //System.out.println("injectionValue="+injectionValue);

                drugAsyncTask connect = new drugAsyncTask(getActivityContentView(view),null, drugConstant.CHANGE_INJECTION_RECORD);
                connect.execute(userId,injectionRecordId,injectionDate,injectionTime,injectionName,injectionValue);

            }
        });

        Button deleteButton = (Button) findViewById(R.id.injectionDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new injectionDeleteDialogFragment();
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        String getDrugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_DRUG);
        intent.putExtra(drugConstant.GET_DRUG_DATE, getDrugDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    private class injectionDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Activity activity = (Activity) getActivity();
                                    UserLocalStore userLocalStore = new UserLocalStore(activity);
                                    String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                                    drugAsyncTask connect = new drugAsyncTask(activity,null, drugConstant.DELETE_INJECTION_RECORD);
                                    connect.execute(userId,injectionRecordId);
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }
}
