package com.dmc.myapplication.export;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Po on 30/3/2016.
 */
public class exportGeneralAsyncTask extends AsyncTask<Void,Void,Void> {

    private ProgressDialog progressBar;
    private String model;
    private Activity activity;

    public exportGeneralAsyncTask(Activity activity, String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected Void doInBackground(Void... params){

        try {
            HttpClient client;
            if(!systemConstant.SERVER_ADDRESS.contains("https")) { // run http
                HttpParams httpRequestParams = getHttpRequestParams();
                HttpConnectionParams.setConnectionTimeout(httpRequestParams, systemConstant.CONNECTION_TIMEOUT);
                HttpConnectionParams.setSoTimeout(httpRequestParams, systemConstant.CONNECTION_TIMEOUT);
                client = new DefaultHttpClient(httpRequestParams);
            }else{ // run https
                client = trustAllHosts();
            }
            HttpPost post = new HttpPost( systemConstant.SERVER_ADDRESS + model);

            JSONObject json = new JSONObject();
            UserLocalStore userLocalStore = new UserLocalStore(activity);
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
            json.put("userId", userId);

            StringEntity se = new StringEntity(json.toString(), "UTF-8" );
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
            post.setEntity(se);
            HttpResponse response = client.execute(post);

            String result = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            //System.out.println("Candy result="+result);
            exportTool.writeCsvFile(result, userId, "general");


        }   catch (SocketTimeoutException e){
            e.printStackTrace();
            progressBar.dismiss();
            noInternetConnectionDialog dialog = new noInternetConnectionDialog();
            dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.dismiss();
            serverNoResponseDialog dialog = new serverNoResponseDialog();
            dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        progressBar.dismiss();
    }

    private HttpParams getHttpRequestParams(){
        HttpParams httpRequestParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpRequestParams, systemConstant.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpRequestParams,systemConstant.CONNECTION_TIMEOUT);
        return httpRequestParams;
    }
    private HttpClient trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        try {

            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpConnectionParams.setConnectionTimeout(params, systemConstant.CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, systemConstant.CONNECTION_TIMEOUT);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            return new DefaultHttpClient(ccm, params);

        }catch (Exception e){
            e.printStackTrace();
            return new DefaultHttpClient();
        }
    }
    public class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }

}
