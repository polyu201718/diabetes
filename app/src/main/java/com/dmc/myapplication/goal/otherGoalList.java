package com.dmc.myapplication.goal;

/**
 * Created by lamivan on 6/4/2018.
 */

public class otherGoalList {
  private String goal_five;
  private String goal_six;
  private String goal_seven;
  private String goal_eight;
  private String goal_nine;
  private String goal_ten;
  private String goal_eleven;
  private String goal_twelve;

  public otherGoalList(String goal_five, String goal_six, String goal_seven, String goal_eight, String goal_nine, String goal_ten, String goal_eleven, String goal_twelve) {
    this.goal_five = goal_five;
    this.goal_six = goal_six;
    this.goal_seven = goal_seven;
    this.goal_eight = goal_eight;
    this.goal_nine = goal_nine;
    this.goal_ten = goal_ten;
    this.goal_eleven = goal_eleven;
    this.goal_twelve = goal_twelve;
  }

  public String getGoal_five() {
    return goal_five;
  }

  public String getGoal_six() {
    return goal_six;
  }

  public String getGoal_seven() {
    return goal_seven;
  }

  public String getGoal_eight() {
    return goal_eight;
  }

  public String getGoal_nine() {
    return goal_nine;
  }

  public String getGoal_ten() {
    return goal_ten;
  }

  public String getGoal_eleven() {
    return goal_eleven;
  }

  public String getGoal_twelve() {
    return goal_twelve;
  }
}
