package com.dmc.myapplication.gym;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Spinner;

import com.dmc.myapplication.R;

import java.util.HashMap;

/**
 * Created by KwokSinMan on 2/2/2016.
 */
public class gymAddAllFrag extends Fragment {
    public gymAddAllFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.gym_add_all, container, false);

        // set gym Cate  DDL  setOnItemSelectedListener
        Spinner gymCateSpinner = (Spinner) v.findViewById(R.id.gymCategories);
        gymCateSpinner.setOnItemSelectedListener(new gymCateDDLOnItemSelectedListener(this.getActivity()));

        //set All gym list setOnItemSelectedListener
        GridView gridView = (GridView) v.findViewById(R.id.allGymGridView);
        gridView.setOnItemClickListener(new gymAllListOnItemClickListener(this.getActivity()));

        // go DB to get gym Cate DDL
        gymAsyncTask connect = new gymAsyncTask(this.getActivity(), gymConstant.GET_GYM_CATE_DDL);
        connect.execute();

        return v;
    }

    private class gymCateDDLOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        private Activity activity;

        public gymCateDDLOnItemSelectedListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
            if ( ((gymBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()!=0 ){
                //获取选择的项的值
                gymAsyncTask connect = new gymAsyncTask(this.activity, gymConstant.GET_GYM_All_LIST);
                connect.execute(String.valueOf(((gymBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()));
            }
            else{
                //clear food list
                GridView gridView = (GridView) this.activity.findViewById(R.id.allGymGridView);
                gridView.setAdapter(null);
            }
        }
        @Override // 什么也没选
        public void onNothingSelected(AdapterView<?> arg0) {}

    }

    private  class  gymAllListOnItemClickListener implements  GridView.OnItemClickListener {
        Activity activity;
        public gymAllListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int gymId=((gymBiz.DbRecordStringId) ((HashMap) adapter.getItemAtPosition(position)).get("gymName")).getId();
            String gymName= ((gymBiz.DbRecordStringId) ((HashMap) adapter.getItemAtPosition(position)).get("gymName")).toString();
            Intent intent = new Intent();
            intent.setClass(activity, gymAddDetailActivity.class);
            intent.putExtra("gym_id", gymId);
            intent.putExtra("gym_name", gymName);
            intent.putExtra("is_free_text",gymConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(gymConstant.GET_GYM_DATE, activity.getIntent().getExtras().getString(gymConstant.GET_GYM_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }



}
