package com.dmc.myapplication.gym;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.exercise_type;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by KwokSinMan on 1/2/2016.
 */
public class gymBiz {

    public static void startGym(Activity activity,String gymDateUI, FragmentTransaction ft){
        activity.setTitle(R.string.nav_gym);
        Bundle bundle = new Bundle();
        bundle.putString(gymConstant.GET_GYM_DATE, gymDateUI);
        gymFragment navFoodFrag = new gymFragment();
        navFoodFrag.setArguments(bundle);
        ft.replace(R.id.content_frame, navFoodFrag).commit();
    }

    public void afterSaveGymRecord (Activity activity, Boolean isDeleteAction){
        String gymDate = activity.getIntent().getExtras().getString(gymConstant.GET_GYM_DATE);
        if (!isDeleteAction){
            gymDate = new gymDateTimeTool().getDBDateFormal((String) ((TextView) activity.findViewById(R.id.gymViewDate)).getText());
        }

        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
        intent.putExtra(gymConstant.GET_GYM_DATE, gymDate);
        activity.startActivity(intent);
        activity.finish();
    }

    public void setGymRecord (Activity activity, String result) {
        TextView gymTotalTextView = (TextView) activity.findViewById(R.id.gymTotal);
        ListView gymList = (ListView) activity.findViewById(R.id.gymList);

        if(!result.isEmpty()){
            try {
                int gymTotal = 0;
                ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    int value = JA.getJSONObject(i).getInt("EXERCISE_PERIOD");
                    gymTotal = gymTotal + value;

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("gymRecordId", JA.getJSONObject(i).getString("EXERCISE_RECORD_ID"));
                    map.put("gymName", JA.getJSONObject(i).getString("EXERCISE_NAME"));
                    map.put("gymValue", Integer.toString(value));
                    map.put("isFreeText", JA.getJSONObject(i).getString("IS_FREE_TEXT"));
                    mylist.add(map);
                }

                //set Total
                gymTotalTextView.setText("總運動分鐘：" + gymTotal + "分鐘");

                //set gym record
                SimpleAdapter gymAdapter = new SimpleAdapter(activity, mylist, R.layout.gym_record_list, new String[]{"gymName", "gymValue"}, new int[]{R.id.gymName, R.id.gymValue});
                gymList.setAdapter(gymAdapter);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //clear total session
            gymTotalTextView.setText("沒有任何記錄");

            //clear expandable listview
            gymList.setAdapter(null);
        }
    }

    public void setGymCateDDL(Activity activity, String result){
        try {
            //peogress data from server side
            //JSONArray JA = new JSONArray(result);
            exercise_type etdb = new exercise_type(activity);
            List<exercise_type.exercise_type_class> JA = etdb.getAll();

            List<DbRecordStringId> gymCateDDL = new ArrayList<DbRecordStringId>();
            gymCateDDL.add(new DbRecordStringId("請選擇", 0));
            //for (int i = 0; i < JA.length(); i++) {
            //    gymCateDDL.add(new DbRecordStringId(JA.getJSONObject(i).getString("EXERCISE_CATE_NAME"), JA.getJSONObject(i).getInt("EXERCISE_CATE_ID")));
            //}

            for (int i = 0; i < JA.size(); i++){
                gymCateDDL.add(new DbRecordStringId(JA.get(i).getEXERCISE_TYPE_NAME(), JA.get(i).getEXERCISE_TYPE_ID()));
            }
            // set food Cate DDL
            Spinner gymCateSpinner = (Spinner) activity.findViewById(R.id.gymCategories);
            ArrayAdapter<DbRecordStringId> gymCateAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.gym_spinner_center_item, gymCateDDL);
            gymCateAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            gymCateSpinner.setAdapter(gymCateAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void  setGymAllList(Activity activity, String result){
        try {
            JSONArray JA = new JSONArray(result);
            List<Map<String, DbRecordStringId>> items = new ArrayList<Map<String,DbRecordStringId>>();
            for (int i = 0; i < JA.length(); i++) {
                Map<String, DbRecordStringId> item = new HashMap<String, DbRecordStringId>();
                item.put("gymName", new DbRecordStringId(JA.getJSONObject(i).getString("EXERCISE_NAME"), JA.getJSONObject(i).getInt("EXERCISE_ID")));
                items.add(item);
            }

            SimpleAdapter adapter = new SimpleAdapter(activity, items, R.layout.gym_add_all_list, new String[]{"gymName"}, new int[]{R.id.itemName});
            GridView gridView = (GridView) activity.findViewById(R.id.allGymGridView);
            gridView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setGymSearchResult(Activity activity, String result){
        if (result.isEmpty()){

            TextView gymNotFoundTextView = (TextView) activity.findViewById(R.id.gymNotFound);
            gymNotFoundTextView.setVisibility(View.VISIBLE);

            Button navFoodFreeAdd = (Button)activity.findViewById(R.id.gymFreeAdd);
            navFoodFreeAdd.setVisibility(View.VISIBLE);

        }
        else{
            try{
                JSONArray JA = new JSONArray(result);
                List<DbRecordStringId> gymSearchList = new ArrayList<DbRecordStringId>();
                for (int i = 0; i < JA.length(); i++) {
                    gymSearchList.add(new DbRecordStringId(JA.getJSONObject(i).getString("EXERCISE_NAME"), JA.getJSONObject(i).getInt("EXERCISE_ID")));
                }
                ArrayAdapter gymSearchAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.gym_add_search_list, R.id.itemName, gymSearchList);
                ListView searchResultListView = (ListView) activity.findViewById(R.id.searchedGymList);
                searchResultListView.setAdapter(gymSearchAdapter);

                TextView gymNotFoundTextView = (TextView) activity.findViewById(R.id.gymNotFound);
                gymNotFoundTextView.setVisibility(View.GONE);

                Button gymFreeAdd = (Button)activity.findViewById(R.id.gymFreeAdd);
                gymFreeAdd.setVisibility(View.GONE);

            }catch (Exception e ){
                e.printStackTrace();
            }
        }
    }

    public void setGymCommonList(Activity activity, String result){
        try{
            if (result.isEmpty()) {
                String[] mobileArray = {"沒有任何常用的運動"};
                ArrayAdapter adapter = new ArrayAdapter<String>(activity, R.layout.gym_add_common_list, R.id.itemName, mobileArray);
                ListView listView = (ListView) activity.findViewById(R.id.commonListView);
                listView.setAdapter(adapter);
                listView.setSelector(android.R.color.transparent);
                listView.setOnItemClickListener(null);

            }else {
                JSONArray JA = new JSONArray(result);
                List<DbRecordStringId> gymCommonList = new ArrayList<DbRecordStringId>();
                for (int i = 0; i < JA.length(); i++) {
                    gymCommonList.add(new DbRecordStringId(JA.getJSONObject(i).getString("EXERCISE_NAME"), JA.getJSONObject(i).getInt("EXERCISE_ID")));
                }
                ArrayAdapter gymCommonAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.gym_add_common_list, R.id.itemName, gymCommonList);
                ListView commonListView = (ListView) activity.findViewById(R.id.commonListView);
                commonListView.setAdapter(gymCommonAdapter);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getGymDetail (Activity activity, String result){
        try{
            JSONArray JA = new JSONArray(result);
            if (0 < JA.length()) {
                gymDateTimeTool tool = new gymDateTimeTool();
                TextView gymDateTextView = (TextView)activity.findViewById(R.id.gymViewDate);
                gymDateTextView.setText(tool.getUIDateFormal(JA.getJSONObject(0).getString("EXERCISE_DATE")));

                TextView gymTimeTextView = (TextView)activity.findViewById(R.id.gymRecordTime);
                gymDateTimeTool timeTool = new gymDateTimeTool(JA.getJSONObject(0).getString("EXERCISE_TIME"));
                gymTimeTextView.setText(timeTool.getUITimeFormal());

                TextView gymNameTextView = (TextView)activity.findViewById(R.id.gymName);
                gymNameTextView.setText(JA.getJSONObject(0).getString("EXERCISE_NAME"));

                TextView gymValueTextView = (TextView) activity.findViewById(R.id.gymValue);
                gymValueTextView.setText(JA.getJSONObject(0).getString("EXERCISE_PERIOD"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public class DbRecordStringId {
        String value;
        int id;

        public DbRecordStringId(String value, int id) {
            this.value = value;
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public String toString() {
            return this.value;
        }
    }

}
