package com.dmc.myapplication.gym;

/**
 * Created by KwokSinMan on 1/2/2016.
 */
public class gymConstant {
    public final static String GET_GYM_RECORD = "gym/getGymRecord.php";
    public final static String GET_GYM_CATE_DDL = "gym/getGymCate.php";
    public final static String GET_GYM_All_LIST = "gym/getGymAllList.php";
    public final static String GET_GYM_SEARCH_RESULT = "gym/getGymSearchResult.php";
    public final static String GET_GYM_COMMON_LIST = "gym/getGymCommonList.php";

    public final static String SAVE_FREE_TEXT_GYM_RECORD = "gym/saveFreeTextGymRecord.php";
    public final static String SAVE_GYM_RECORD = "gym/saveGymRecord.php";

    public final static String GET_GYM_DETAIL = "gym/getGymDetail.php";
    public final static String DELETE_GYM_RECORD = "gym/deleteGymRecord.php";
    public final static String SAVE_EDIT_GYM_RECORD = "gym/saveEditGymRecord.php";


    public final static int GET_SPEECH_RESULT_CODE = 2;
    public final static int GET_SPEECH_RESULT_OK = -1;

    public final static int GET_NOT_FOUND_CODE= 0;
    public final static String GET_IS_FREE_TEXT_CODE = "Y";
    public final static String GET_IS_NOT_FREE_TEXT_CODE = "N";

    // intent name
    public final static String GET_GYM_DATE = "redirect_gym_date"; //DB formal
    public final static Boolean IS_DELETE_ACTION_Y = true;
    public final static Boolean IS_DELETE_ACTION_N = false;

}
