package com.dmc.myapplication.gym;

import java.util.Calendar;

/**
 * Created by KwokSinMan on 3/2/2016.
 */
public class gymDateTimeTool {
    private int hour;
    private int min;
    public gymDateTimeTool(){

    }
    public gymDateTimeTool(String time){
        String [] temp = time.split(":");
        this.hour = Integer.parseInt(temp[0]);
        this.min = Integer.parseInt(temp[1]);
    }
    public int getHour() {
        return hour;
    }
    public int getMin() {
        return min;
    }

    public String getTimeString(int inputedMin){
        if (0<=inputedMin && inputedMin <=9)
            return "0" + String.valueOf(inputedMin);
        else return String.valueOf(inputedMin);
    }

    public String getUITimeFormal(){
        return getTimeString(this.hour) +":"+getTimeString(this.min);
    }


    public String getDBDateFormal(String uiDateFormal){
        String [] temp = uiDateFormal.split("/");
        uiDateFormal = temp[2]+"-"+temp[1]+"-"+temp[0];  // YYYY-MM-DD
        return uiDateFormal;
    }

    public String getUIDateFormal(String dbDateFormal){
        String [] temp = dbDateFormal.split("-");
        dbDateFormal = temp[2]+"/"+temp[1]+"/"+temp[0];
        return  dbDateFormal;
    }

    public static String getVurrentDateInDBFormal(){
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return yy + "-" + mm + "-" +dd;   // UI = DD/MM/YYYY
    }
}
