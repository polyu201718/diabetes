package com.dmc.myapplication.gymNew;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.exercise_type;
import com.dmc.myapplication.Models.exercise_type_record;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.tool.dbRecordIdString;
import com.dmc.myapplication.tool.timeObject;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewBiz {
    public static void startNewGym(Activity activity,String gymDateUI, FragmentTransaction ft){
        activity.setTitle(R.string.nav_gym);
        Bundle bundle = new Bundle();
        bundle.putString(gymNewConstant.GET_GYM_DATE, gymDateUI);
        gymNewFragment gymNewFragment = new gymNewFragment();
        gymNewFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, gymNewFragment).addToBackStack(null).commit();
    }

    public void afterSaveGymRecord (Activity activity, Boolean isDeleteAction){
        String gymDate = activity.getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);
        if (!isDeleteAction){
            //System.out.println("Candy gymDate has run");
            gymDate = dateTool.getDBDateFormal((String) ((TextView) activity.findViewById(R.id.gymNewViewDate)).getText());
        }
       // System.out.println("Candy gymDate="+gymDate);
        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
        intent.putExtra(gymNewConstant.GET_GYM_DATE, gymDate);
        activity.startActivity(intent);
        activity.finish();
    }

    //Janus Backup
    /*public void setGymRecord(Activity activity, String result){
        //System.out.println("Candy result=" + result);
        TextView gymTotalTextView = (TextView) activity.findViewById(R.id.gymNewTotal);
        ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.gymNewRecord);
        if(!result.isEmpty()){
            try {
                int totalGymPeriod = 0;
                HashMap<Integer, dbRecordIdString> headerValueMap = new HashMap<Integer, dbRecordIdString>();
                HashMap<Integer, List<gymNewRecord>> childValueMap = new HashMap<Integer, List<gymNewRecord>>();
                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    int gymTypeId = JA.getJSONObject(i).getInt("EXERCISE_TYPE_ID");
                    String gymTypeName = JA.getJSONObject(i).getString("EXERCISE_TYPE_NAME"); ;
                    int period = JA.getJSONObject(i).getInt("EXERCISE_PERIOD");
                    totalGymPeriod = totalGymPeriod + period;
                    if(headerValueMap.containsKey(gymTypeId)){
                        dbRecordIdString temp = headerValueMap.get(gymTypeId);
                        //System.out.println("Candy has Object =" + temp);
                        headerValueMap.put(gymTypeId, new dbRecordIdString(gymTypeName, temp.getId() +period));
                    }else{
                        //System.out.println("Candy does not have object");
                        headerValueMap.put(gymTypeId, new dbRecordIdString(gymTypeName, period));
                    }

                    gymNewRecord record = new gymNewRecord(JA.getJSONObject(i).getInt("EXERCISE_RECORD_ID"), new timeObject(JA.getJSONObject(i).getString("EXERCISE_TIME")).getUITimeFormal(), Integer.toString(period) );
                    if(childValueMap.containsKey(gymTypeId)){
                        List<gymNewRecord> temp = childValueMap.get(gymTypeId);
                        temp.add(record);
                    }else{
                        List<gymNewRecord> gymNewRecordsList = new ArrayList<gymNewRecord>();
                        gymNewRecordsList.add(record);
                        childValueMap.put(gymTypeId, gymNewRecordsList);
                    }
                }

                //testing
                /*
                for (Object key : headerValueMap.keySet()) {
                    System.out.println("Candy headerValueMap Key : " + key.toString() + " Value : " + headerValueMap.get(key).getId() +"," + headerValueMap.get(key));
                }
                for (Object key : childValueMap.keySet()) {
                    List<gymNewRecord> temp = childValueMap.get(key);
                    for (int i=0; i<temp.size();i++) {
                        System.out.println("Candy record list=" + temp.get(i).getGymNewRecordId());
                        System.out.println("Candy record list=" + temp.get(i).getGymNewTime());
                        System.out.println("Candy record list=" + temp.get(i).getGymNewValue());
                    }
                }

                List<String> header =  new ArrayList<String>();
                HashMap<String, List<gymNewRecord>> child = new HashMap<String, List<gymNewRecord>>();

                int count =0;
                for (int i=1; i<=gymNewConstant.GET_NUM_GYM_TYPE; i++){
                    if(headerValueMap.containsKey(i)){
                        header.add(headerValueMap.get(i) + "," + headerValueMap.get(i).getId());
                        if(childValueMap.containsKey(i)){
                            child.put(header.get(count), childValueMap.get(i));
                        }
                        count++;
                    }
                }
                gymNewExpandableListAdapter listAdapter = new gymNewExpandableListAdapter(activity,header, child);
                expListView.setAdapter(listAdapter);
                gymTotalTextView.setText("總運動分鐘：" + totalGymPeriod + "分鐘");

            }catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            gymTotalTextView.setText("沒有任何記錄");
            //clear expandable listview
            expListView.setAdapter((gymNewExpandableListAdapter)null);
        }*/

        //set expandable list
        /*
        ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.gymNewRecord);
        List<String> header =  new ArrayList<String>();
        header.add("低等強度,"+"111");
        header.add("中等强度,"+"222");
        header.add("劇烈活動,"+"333");
        List<gymNewRecord> gymNewRecordsLow = new ArrayList<gymNewRecord>();
        gymNewRecordsLow.add(new gymNewRecord(0,"07:29","10"));

        List<gymNewRecord> gymNewRecordsMedium = new ArrayList<gymNewRecord>();
        gymNewRecordsMedium.add(new gymNewRecord(1,"13:29","20"));

        List<gymNewRecord> gymNewRecordsHigh = new ArrayList<gymNewRecord>();
        gymNewRecordsHigh.add(new gymNewRecord(2,"16:29","30"));

        HashMap<String, List<gymNewRecord>> child = new HashMap<String, List<gymNewRecord>>();
        child.put(header.get(0), gymNewRecordsLow);
        child.put(header.get(1), gymNewRecordsMedium);
        child.put(header.get(2), gymNewRecordsHigh);

         gymNewExpandableListAdapter listAdapter = new gymNewExpandableListAdapter(activity,header, child);
        expListView.setAdapter(listAdapter);
*/
   /* }*/

    public void setGymRecord(Activity activity, String result){
        //System.out.println("Candy result=" + result);
        TextView gymTotalTextView = (TextView) activity.findViewById(R.id.gymNewTotal);
        ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.gymNewRecord);
        exercise_type_record etrdb = new exercise_type_record(activity);
        System.out.println("setGymRecord - GET_GYM_DATE = "+gymNewConstant.getGymDate);
        List<exercise_type_record.exercise_type_record_class> JA = etrdb.getByDate(gymNewConstant.getGymDate);
        System.out.println("setGymRecord - JA.size = "+JA.size());

        if(JA.size() > 0){
            try {
                int totalGymPeriod = 0;
                HashMap<Integer, dbRecordIdString> headerValueMap = new HashMap<Integer, dbRecordIdString>();
                HashMap<Integer, List<gymNewRecord>> childValueMap = new HashMap<Integer, List<gymNewRecord>>();
                //JSONArray JA = new JSONArray(result);


                for (int i = 0; i < JA.size(); i++) {
                    int gymTypeId = JA.get(i).getEXERCISE_TYPE_ID();
                    String gymTypeName = JA.get(i).getEXERCISE_TYPE_NAME();
                    int period = JA.get(i).getEXERCISE_PERIOD();
                    totalGymPeriod = totalGymPeriod + period;
                    if(headerValueMap.containsKey(gymTypeId)){
                        dbRecordIdString temp = headerValueMap.get(gymTypeId);
                        //System.out.println("Candy has Object =" + temp);
                        headerValueMap.put(gymTypeId, new dbRecordIdString(gymTypeName, temp.getId() +period));
                    }else{
                        //System.out.println("Candy does not have object");
                        headerValueMap.put(gymTypeId, new dbRecordIdString(gymTypeName, period));
                    }

                    gymNewRecord record = new gymNewRecord(JA.get(i).getEXERCISE_RECORD_ID(), new timeObject(JA.get(i).getEXERCISE_TIME()).getUITimeFormal(), Integer.toString(period) );
                    if(childValueMap.containsKey(gymTypeId)){
                        List<gymNewRecord> temp = childValueMap.get(gymTypeId);
                        temp.add(record);
                    }else{
                        List<gymNewRecord> gymNewRecordsList = new ArrayList<gymNewRecord>();
                        gymNewRecordsList.add(record);
                        childValueMap.put(gymTypeId, gymNewRecordsList);
                    }
                }

                List<String> header =  new ArrayList<String>();
                HashMap<String, List<gymNewRecord>> child = new HashMap<String, List<gymNewRecord>>();

                int count =0;
                for (int i=1; i<=gymNewConstant.GET_NUM_GYM_TYPE; i++){
                    if(headerValueMap.containsKey(i)){
                        header.add(headerValueMap.get(i) + "," + headerValueMap.get(i).getId());
                        if(childValueMap.containsKey(i)){
                            child.put(header.get(count), childValueMap.get(i));
                        }
                        count++;
                    }
                }
                gymNewExpandableListAdapter listAdapter = new gymNewExpandableListAdapter(activity,header, child);
                expListView.setAdapter(listAdapter);
                gymTotalTextView.setText("總運動分鐘：" + totalGymPeriod + "分鐘");

            }catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            gymTotalTextView.setText("沒有任何記錄");
            //clear expandable listview
            expListView.setAdapter((gymNewExpandableListAdapter)null);
        }

    }

    public void setGymTypeDDL(Activity activity, String result){
        try {
            //JSONArray JA = new JSONArray(result);
            JSONArray JA = null;
            Spinner gymTypeSpinner = (Spinner) activity.findViewById(R.id.gymNewType);
            setGymTypeDDLDetail(activity,gymTypeSpinner, JA, 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setGymTypeDDLDetail(Activity activity,Spinner gymTypeSpinner, JSONArray JA2, int startCount){
        try {
            List<dbRecordIdString> gymTypeList = new ArrayList<dbRecordIdString>();

           /* for (int i = startCount; i < JA.length(); i++) {
                gymTypeList.add(new dbRecordIdString(JA.getJSONObject(i).getString("EXERCISE_TYPE_NAME"), JA.getJSONObject(i).getInt("EXERCISE_TYPE_ID")));
            }*/

             exercise_type etdb = new exercise_type(activity);
            List<exercise_type.exercise_type_class> JA = etdb.getAll();
            for (int i = startCount; i < JA.size(); i++) {
                gymTypeList.add(new dbRecordIdString(JA.get(i).getEXERCISE_TYPE_NAME(), JA.get(i).getEXERCISE_TYPE_ID()));
            }
            ArrayAdapter<dbRecordIdString> gymTypeAdapter = new ArrayAdapter<dbRecordIdString>(activity, R.layout.navfood_spinner_center_item, gymTypeList);
            gymTypeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            gymTypeSpinner.setAdapter(gymTypeAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setGymRecordDetailForOffline(Activity activity, Integer id){
        //System.out.println("Candy result="+result);
        exercise_type_record etrdb = new exercise_type_record(activity);
        exercise_type_record.exercise_type_record_class result = etrdb.get(id);
        if(result != null){
            try {
                //JSONArray JA = new JSONArray(result);
                //set date
                TextView gymDate = (TextView)activity.findViewById(R.id.gymNewViewDate);
                gymDate.setText(dateTool.getUIDateFormal(result.getEXERCISE_DATE()));

                //set time
                TextView gymTimeTextView = (TextView)activity.findViewById(R.id.gymNewRecordTime);
                timeObject timeTool = new timeObject(result.getEXERCISE_TIME());
                gymTimeTextView.setText(timeTool.getUITimeFormal());

                //set value
                TextView gymValue = (TextView) activity.findViewById(R.id.gymNewValue);
                gymValue.setText(String.valueOf(result.getEXERCISE_PERIOD()));

                //set DDL
                Spinner gymTypeSpinner = (Spinner) activity.findViewById(R.id.gymNewType);
                setGymTypeDDLDetail(activity,gymTypeSpinner, null, 0);
                gymTypeSpinner.setSelection(result.getEXERCISE_TYPE_ID()-1);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void setGymRecordDetail(Activity activity, String result){
        //System.out.println("Candy result="+result);
        if(!result.isEmpty()){
            try {
                JSONArray JA = new JSONArray(result);
                //set date
                TextView gymDate = (TextView)activity.findViewById(R.id.gymNewViewDate);
                gymDate.setText(dateTool.getUIDateFormal(JA.getJSONObject(0).getString("EXERCISE_DATE")));

                //set time
                TextView gymTimeTextView = (TextView)activity.findViewById(R.id.gymNewRecordTime);
                timeObject timeTool = new timeObject(JA.getJSONObject(0).getString("EXERCISE_TIME"));
                gymTimeTextView.setText(timeTool.getUITimeFormal());

                //set value
                TextView gymValue = (TextView) activity.findViewById(R.id.gymNewValue);
                gymValue.setText(JA.getJSONObject(0).getString("EXERCISE_PERIOD"));

                //set DDL
                Spinner gymTypeSpinner = (Spinner) activity.findViewById(R.id.gymNewType);
                setGymTypeDDLDetail(activity,gymTypeSpinner, JA, 1);
                gymTypeSpinner.setSelection(JA.getJSONObject(0).getInt("EXERCISE_TYPE_ID")-1);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    public void popUpWindow(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.gym_new_type_help);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Do something
            }
        });
        builder.show();
    }
}
