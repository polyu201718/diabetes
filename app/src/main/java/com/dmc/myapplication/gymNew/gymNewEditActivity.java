package com.dmc.myapplication.gymNew;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.exercise_type_record;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.tool.dbRecordIdString;

/**
 * Created by KwokSinMan on 14/3/2016.
 */
public class gymNewEditActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gym_new_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        Intent intent = getIntent();
        final String gymRecordId = intent.getExtras().getString("gym_new_record_id");
        //System.out.println("gymRecordId="+gymRecordId);

        //hidden add record session
        LinearLayout addRecordButtonSession = (LinearLayout) findViewById(R.id.addRecordButtonSession);
        addRecordButtonSession.setVisibility(View.GONE);

        // set data picker
        ImageView setGymRecordDate = (ImageView) findViewById(R.id.setGymNewViewDate);
        setGymRecordDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymNewDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setGymRecordTime = (ImageView) findViewById(R.id.setGymNewRecordTime);
        setGymRecordTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymNewTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set value picker
        ImageView setGymRecordValue = (ImageView) findViewById(R.id.changeGymNewValue);
        setGymRecordValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new gymNewNumberPickerFragment();
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        // set Help Pop up
        ImageView gymTypeHelp = (ImageView) findViewById(R.id.gymTypeHelp);
        gymTypeHelp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                gymNewBiz biz = new gymNewBiz();
                biz.popUpWindow(gymNewEditActivity.this);
            }
        });

        Button editButton = (Button) findViewById(R.id.gymNewEdit);
        editButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                UserLocalStore userLocalStore = new UserLocalStore(getActivityContentView(view));
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                String gymDate = dateTool.getDBDateFormal((String) ((TextView) findViewById(R.id.gymNewViewDate)).getText());
                String gymTime = (String) ((TextView) findViewById(R.id.gymNewRecordTime)).getText();
                String gymValue = (String) ((TextView) findViewById(R.id.gymNewValue)).getText();
                String gymType = Integer.toString(((dbRecordIdString) (((Spinner) findViewById(R.id.gymNewType)).getSelectedItem())).getId());

                /*
                System.out.println("Candy userId="+userId);
                System.out.println("Candy gymRecordId="+gymRecordId);
                System.out.println("Candy gymDate="+gymDate);
                System.out.println("Candy gymTime="+gymTime);
                System.out.println("Candy gymValue="+gymValue);
                System.out.println("Candy gymType="+gymType);
*/
                //gymNewAsyncTask connect = new gymNewAsyncTask(getActivityContentView(view), gymNewConstant.EDIT_GYP_RECORD);
                //connect.execute(userId,gymRecordId, gymDate, gymTime, gymType, gymValue);
                exercise_type_record etrdb = new exercise_type_record(getApplicationContext());
                etrdb.update(etrdb.new exercise_type_record_class(Integer.parseInt(gymRecordId), Integer.parseInt(userId), gymDate, gymTime, Integer.parseInt(gymType), Integer.parseInt(gymValue)));

                String gymDate2 = dateTool.getDBDateFormal((String) ((TextView) findViewById(R.id.gymNewViewDate)).getText());

                // System.out.println("Candy gymDate="+gymDate);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), MainActivity.class);
                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
                intent.putExtra(gymNewConstant.GET_GYM_DATE, gymDate2);
                startActivity(intent);
                finish();
            }
        });

        Button finishButton = (Button) findViewById(R.id.gymNewDelete);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new gymNewRecordDeleteDialogFragment(gymRecordId);
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        //get Data from DB
        //UserLocalStore userLocalStore = new UserLocalStore(this);
        //String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        //gymNewAsyncTask connect = new gymNewAsyncTask(this, gymNewConstant.GET_GYM_RECORD_DETAIL);
        //connect.execute(userId, gymRecordId);
        gymNewBiz biz = new gymNewBiz();
        biz.setGymRecordDetailForOffline(this, Integer.parseInt(gymRecordId));
        setSupportActionBar(toolbar);
        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            handleBackAction();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        String getGymDate = getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
        intent.putExtra(gymNewConstant.GET_GYM_DATE, getGymDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    private class gymNewRecordDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private String gymRecordId;
        public gymNewRecordDeleteDialogFragment (String gymRecordId){
            this.gymRecordId = gymRecordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    UserLocalStore userLocalStore = new UserLocalStore(getActivity());
                                    String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                                    //gymNewAsyncTask connect = new gymNewAsyncTask(getActivity(), gymNewConstant.DELETE_GYM_RECORD);
                                    //connect.execute(userId, gymRecordId);
                                    exercise_type_record etrdb = new exercise_type_record(getApplicationContext());
                                    etrdb.delete(Integer.parseInt(gymRecordId));
                                    String gymDate2 = getActivity().getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);

                                    // System.out.println("Candy gymDate="+gymDate);
                                    Intent intent = new Intent();
                                    intent.setClass(getApplicationContext(), MainActivity.class);
                                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
                                    intent.putExtra(gymNewConstant.GET_GYM_DATE, gymDate2);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }

}
