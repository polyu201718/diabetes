package com.dmc.myapplication.login;


import android.app.FragmentTransaction;
import android.content.Context;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.dmc.myapplication.R;



/**
 * Created by Po on 23/1/2016.
 */
public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button registerBtn = (Button) findViewById(R.id.buttonRegister);
        registerBtn.setVisibility(View.GONE);

        // Create a new Fragment to be placed in the activity layout
         FragmentTransaction ft = getFragmentManager().beginTransaction();

        // Add the fragment to the 'fragment_container' FrameLayout
        ft.replace(R.id.frameLayout, new regChildFragment()).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.register_submit:
                //System.out.println("Submit btn clicked");
                Button registerBtn = (Button) findViewById(R.id.buttonRegister);
                registerBtn.performClick();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register_menu, menu);
        return true;
    }


}
