package com.dmc.myapplication.login;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.RegisterActivity;
import com.dmc.myapplication.timeCheck;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Po on 7/2/2016.
 */
public class regChildFragment extends Fragment implements View.OnClickListener
 {

     private Context ctx;

     Button buttonRegister;
     private EditText editTextID, editTextName;
     private EditText editTextMorningStart, editTextMorningEnd,editTextBreakfastStart, editTextBreakfastEnd;
     private EditText editTextLunchStart, editTextLunchEnd, editTextDinnerStart, editTextDinnerEnd, editTextBedStart, editTextBedEnd;
     private EditText editTextBday;
     private ToggleButton switchSex, switchSmoker, switchHeight, switchWeight, switchWaist, switchInject;
     private int aSwitchSex=0;
     private String aSwitchInject="N";
     private int aSwitchSmoker=0;
     private int aSwitchHeight=0;
     private int aSwitchWeight=0;
     private int aSwitchWaist=0;



     @Override
     public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState)
     {
         View view  = inflater.inflate(R.layout.register_user, null, false);
         ctx = view.getContext();

         editTextID = (EditText) view.findViewById(R.id.reg_userid);
         editTextName = (EditText) view.findViewById(R.id.reg_name);

         editTextBday = (EditText) view.findViewById(R.id.reg_birthday);
         ImageView setBdayDate = (ImageView) view.findViewById(R.id.setBdayViewDate);
         setBdayDate.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment = new regDatePickerFragment();
                 newFragment.show(getFragmentManager(), "DatePicker");
             }
         });


         switchSex = (ToggleButton) view.findViewById(R.id.switch_sex);
         switchSex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     Log.i("info", "Sex is male!");
                     aSwitchSex = 1;
                 } else {
                     Log.i("info", "Sex is female");
                     aSwitchSex = 0;
                 }
             }
         });


         switchSmoker = (ToggleButton) view.findViewById(R.id.switch_smoker);
         switchSmoker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     Log.i("info", "Smoker is yes!");
                     aSwitchSmoker = 1;
                 } else {
                     Log.i("info", "Smoker is no");
                     aSwitchSmoker = 0;
                 }
             }
         });

         switchInject = (ToggleButton) view.findViewById(R.id.switch_injection);
         switchInject.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     Log.i("info", "Injection is yes!");
                     aSwitchInject = "Y";
                 } else {
                     Log.i("info", "Injection is no");
                     aSwitchInject = "N";
                 }
             }
         });
         editTextMorningStart = (EditText) view.findViewById(R.id.reg_morning_start);
         editTextMorningStart.setOnClickListener(this);
         ImageView setMornStart = (ImageView) view.findViewById(R.id.setMorStartViewTime);
         setMornStart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });

         editTextMorningEnd = (EditText) view.findViewById(R.id.reg_morning_end);
         editTextMorningEnd.setOnClickListener(this);
         ImageView setMornEnd = (ImageView) view.findViewById(R.id.setMorEndViewTime);
         setMornEnd.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_ME();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextBreakfastStart = (EditText) view.findViewById(R.id.reg_breakfast_start);
         editTextBreakfastStart.setOnClickListener(this);
         ImageView setBFStart = (ImageView) view.findViewById(R.id.setBFStartViewTime);
         setBFStart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_BS();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextBreakfastEnd = (EditText) view.findViewById(R.id.reg_breakfast_end);
         editTextBreakfastEnd.setOnClickListener(this);
         ImageView setBFEnd = (ImageView) view.findViewById(R.id.setBFEndViewTime);
         setBFEnd.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_BE();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextLunchStart = (EditText) view.findViewById(R.id.reg_lunch_start);
         editTextLunchStart.setOnClickListener(this);
         ImageView setLunchStart = (ImageView) view.findViewById(R.id.setLunchStartViewTime);
         setLunchStart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_LS();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextLunchEnd = (EditText) view.findViewById(R.id.reg_lunch_end);
         editTextLunchEnd.setOnClickListener(this);
         ImageView setLunchEnd = (ImageView) view.findViewById(R.id.setLunchEndViewTime);
         setLunchEnd.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_LE();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextDinnerStart = (EditText) view.findViewById(R.id.reg_dinner_start);
         editTextDinnerStart.setOnClickListener(this);
         ImageView setDinnerStart = (ImageView) view.findViewById(R.id.setDinnerStartViewTime);
         setDinnerStart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_DS();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextDinnerEnd = (EditText) view.findViewById(R.id.reg_dinner_end);
         editTextDinnerEnd.setOnClickListener(this);
         ImageView setDinnerEnd = (ImageView) view.findViewById(R.id.setDinnerEndViewTime);
         setDinnerEnd.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_DE();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextBedStart = (EditText) view.findViewById(R.id.reg_bed_start);
         editTextBedStart.setOnClickListener(this);
         ImageView setBedStart = (ImageView) view.findViewById(R.id.setBedStartViewTime);
         setBedStart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_NS();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         editTextBedEnd = (EditText) view.findViewById(R.id.reg_bed_end);
         editTextBedEnd.setOnClickListener(this);
         ImageView setBedEnd = (ImageView) view.findViewById(R.id.setBedEndViewTime);
         setBedEnd.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View arg0) {
                 DialogFragment newFragment3  = new regTimePickFragment_NE();
                 newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

             }
         });
         switchHeight = (ToggleButton) view.findViewById(R.id.switch_height);
         switchHeight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     Log.i("info", "Height is inch!");
                     aSwitchHeight = 1;
                 } else {
                     Log.i("info", "Height is cm");
                     aSwitchHeight = 0;
                 }
             }
         });
         switchWeight = (ToggleButton) view.findViewById(R.id.switch_weight);
         switchWeight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     Log.i("info", "Weight is lb!");
                     aSwitchWeight = 1;
                 } else {
                     Log.i("info", "Weight is kg");
                     aSwitchWeight = 0;
                 }
             }
         });
         switchWaist = (ToggleButton) view.findViewById(R.id.switch_waist);
         switchWaist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if (isChecked) {
                     Log.i("info", "Waist is inch!");
                     aSwitchWaist = 1;
                 } else {
                     Log.i("info", "Waist is cm");
                     aSwitchWaist = 0;
                 }
             }
         });
         buttonRegister = (Button) view.findViewById(R.id.buttonRegister);
         buttonRegister.setOnClickListener(this);

         return view;
     }

     @Override
     public void onClick(View view)
     {
         switch (view.getId()){
             case R.id.buttonRegister:


                 Fragment prev = getFragmentManager().findFragmentByTag("DatePicker");
                 if (prev != null) {
                     DialogFragment df = (DialogFragment) prev;
                     df.dismiss();
                 }
                 Fragment prev2 = getFragmentManager().findFragmentByTag("Time Dialog");
                 if (prev2 != null) {
                     DialogFragment df = (DialogFragment) prev2;
                     df.dismiss();
                 }

                 attemptReg();

                 break;
         }
     }

     private void registerUser(User user){

         ServerRequests serverRequests = new ServerRequests(ctx);
         serverRequests.storeUserDataInBackground(user, new GetUserCallback() {
             @Override
             public void done(User returnedUser) {
                 getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                 getActivity().finish();
             }
         });
     }

     private void attemptReg() {

         // Reset errors.
         editTextID.setError(null);
         editTextMorningStart.setError(null);
         editTextBreakfastStart.setError(null);
         editTextLunchStart.setError(null);
         editTextDinnerStart.setError(null);
         editTextBedStart.setError(null);
         editTextMorningEnd.setError(null);
         editTextBreakfastEnd.setError(null);
         editTextLunchEnd.setError(null);
         editTextDinnerEnd.setError(null);
         editTextBedEnd.setError(null);


         // Store values at the time of the register attempt.
         String user_id = editTextID.getText().toString();
         String morning_start = editTextMorningStart.getText().toString();
         String morning_end = editTextMorningEnd.getText().toString();
         String breakfast_start = editTextBreakfastStart.getText().toString();
         String breakfast_end = editTextBreakfastEnd.getText().toString();
         String lunch_start = editTextLunchStart.getText().toString();
         String lunch_end = editTextLunchEnd.getText().toString();
         String dinner_start = editTextDinnerStart.getText().toString();
         String dinner_end = editTextDinnerEnd.getText().toString();
         String bed_start = editTextBedStart.getText().toString();
         String bed_end = editTextBedEnd.getText().toString();

         boolean cancel = false;
         View focusView = null;

         // Check for a valid username, if the user entered one.
         if (TextUtils.isEmpty(user_id) || !isValid(user_id)) {
             editTextID.setError(getString(R.string.error_invalid_password));
             focusView =  editTextID;
             cancel = true;
         }

         //check the start time is earlier than end time, only 1 over day allow
         ArrayList<Boolean> timeCheckArray = new ArrayList<Boolean>();
         timeCheckArray.add( timeCheck.compareStartEndTime(morning_start,morning_end));
         timeCheckArray.add(timeCheck.compareStartEndTime(breakfast_start, breakfast_end));
         timeCheckArray.add( timeCheck.compareStartEndTime(lunch_start,lunch_end));
         timeCheckArray.add( timeCheck.compareStartEndTime(dinner_start,dinner_end));
         timeCheckArray.add( timeCheck.compareStartEndTime(bed_start,bed_end));

         int only1FalseAllow = 0;
         for (Boolean value : timeCheckArray) {
             if (!value) {
                 only1FalseAllow ++;
             }
         }

         if (only1FalseAllow > 1){
             editTextMorningEnd.setError(getString(R.string.error_time_end_early));
             editTextBreakfastEnd.setError(getString(R.string.error_time_end_early));
             editTextLunchEnd.setError(getString(R.string.error_time_end_early));
             editTextDinnerEnd.setError(getString(R.string.error_time_end_early));
             editTextBedEnd.setError(getString(R.string.error_time_end_early));
             focusView =  editTextMorningEnd;
             cancel = true;
         }

         if(timeCheck.compareTime(morning_start, morning_end, breakfast_start)){
             editTextBreakfastStart.setError(getString(R.string.error_time_overlap));
             focusView =  editTextBreakfastStart;
             cancel = true;
         }
         if(timeCheck.compareTime(breakfast_start, breakfast_end, lunch_start)){
             editTextLunchStart.setError(getString(R.string.error_time_overlap));
             focusView =  editTextLunchStart;
             cancel = true;
         }
         if(timeCheck.compareTime(lunch_start, lunch_end, dinner_start)){
             editTextDinnerStart.setError(getString(R.string.error_time_overlap));
             focusView =  editTextDinnerStart;
             cancel = true;
         }
         if(timeCheck.compareTime(dinner_start, dinner_end, bed_start)){
             editTextBedStart.setError(getString(R.string.error_time_overlap));
             focusView =  editTextBedStart;
             cancel = true;
         }
         if(timeCheck.compareTime(bed_start, bed_end, morning_start)){
             editTextMorningStart.setError(getString(R.string.error_time_overlap));
             focusView =  editTextMorningStart;
             cancel = true;
         }


         if (cancel) {
             focusView.requestFocus();
         } else {

             int userID = Integer.parseInt(editTextID.getText().toString());
             String name = editTextName.getText().toString();
             String birthday = editTextBday.getText().toString();
             int sex = aSwitchSex;
             int smoker =  aSwitchSmoker;
             String inject =  aSwitchInject;

             int height_unit =  aSwitchHeight;
             int weight_unit =  aSwitchWeight;
             int waist_unit =  aSwitchWaist;

             User user  =  new User(userID, name, birthday, sex,
                     smoker, inject, morning_start, morning_end, breakfast_start,
                     breakfast_end, lunch_start, lunch_end, dinner_start,
                     dinner_end, bed_start, bed_end, height_unit, weight_unit, waist_unit, 4, 6, 8, 4, 8, 10);

             checkValid(user);
         }
     }


     private boolean isValid(String password) {
         return password.matches("^([0-9]{6,6})$") ;
     }


     private void checkValid(final User user){
         ServerRequests serverRequests = new ServerRequests(ctx);
         serverRequests.fetchUserDataInBackground(user, new GetUserCallback() {
             @Override
             public void done(User returnedUser) {



                 if (returnedUser == null) {
                     registerUser(user);

                 } else {
                     editTextID.setError(getString(R.string.error_repeat_account));
                     editTextID.requestFocus();
                 }
             }
         });


     }


 }
