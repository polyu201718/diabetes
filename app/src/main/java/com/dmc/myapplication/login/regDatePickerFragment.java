package com.dmc.myapplication.login;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.dmc.myapplication.R;

/**
 * Created by Po on 5/2/2016.
 */
public class regDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayDate ="1975-01-01";
        String [] displayDateArray = displayDate.split("-");
        int yy = Integer.parseInt(displayDateArray[0]);
        int mm = Integer.parseInt(displayDateArray[1]) -1 ;
        int dd = Integer.parseInt(displayDateArray[2]);
        return new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, yy,mm,dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        mm = mm + 1;
        String displayAsDate = yy + "-" + mm + "-" + dd;
        ((TextView) getActivity().findViewById(R.id.reg_birthday)).setText(displayAsDate);

    }
}
