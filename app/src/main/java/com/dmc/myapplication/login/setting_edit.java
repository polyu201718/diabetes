package com.dmc.myapplication.login;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.*;
import com.dmc.myapplication.preference.*;

/**
 * Created by Po on 20/3/2016.
 */
public class setting_edit extends Fragment implements View.OnClickListener
{

    private Context ctx;

    Button buttonRegister;
    private EditText editTextName;
    private EditText editTextMorningStart, editTextMorningEnd,editTextBreakfastStart, editTextBreakfastEnd;
    private EditText editTextLunchStart, editTextLunchEnd, editTextDinnerStart, editTextDinnerEnd, editTextBedStart, editTextBedEnd;
    private EditText editTextBday, editTextPreMealLow, editTextPreMealMed, editTextPreMealHigh, editTextPostMealLow,editTextPostMealMed, editTextPostMealHigh;
    private ToggleButton switchSex, switchSmoker, switchHeight, switchWeight, switchWaist, switchInject;
    private int aSwitchSex=0;
    private String aSwitchInject="N";
    private int aSwitchSmoker=0;
    private int aSwitchHeight=0;
    private int aSwitchWeight=0;
    private int aSwitchWaist=0;
    private int userid;



    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState)
    {
        View view  = inflater.inflate(R.layout.setting_edit, null, false);
        ctx = view.getContext();
        UserLocalStore userLocalStore = new UserLocalStore(ctx);
        User user = userLocalStore.getLoggedInUser();
        userid = user.userid;

        editTextName = (EditText) view.findViewById(R.id.reg_name);
        editTextName.setText(user.name);

        editTextBday = (EditText) view.findViewById(R.id.reg_birthday);
        editTextBday.setText(user.birthday);

        ImageView setBdayDate = (ImageView) view.findViewById(R.id.setBdayViewDate);
        setBdayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new regDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });


        switchSex = (ToggleButton) view.findViewById(R.id.switch_sex);
        aSwitchSex = user.sex;
        if (aSwitchSex == 0)
            switchSex.setChecked(false);
        else
            switchSex.setChecked(true);
        switchSex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i("info", "Sex is male!");
                    aSwitchSex = 1;
                } else {
                    Log.i("info", "Sex is female");
                    aSwitchSex = 0;
                }
            }
        });


        switchSmoker = (ToggleButton) view.findViewById(R.id.switch_smoker);
        aSwitchSmoker = user.smoker;
        if (aSwitchSmoker == 0)
            switchSmoker.setChecked(false);
        else
            switchSmoker.setChecked(true);
        switchSmoker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i("info", "Smoker is yes!");
                    aSwitchSmoker = 1;
                } else {
                    Log.i("info", "Smoker is no");
                    aSwitchSmoker = 0;
                }
            }
        });

        switchInject = (ToggleButton) view.findViewById(R.id.switch_injection);
        aSwitchInject = user.injection;
        if (aSwitchInject.equals("N"))
            switchInject.setChecked(false);
        else
            switchInject.setChecked(true);

        switchInject.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i("info", "Injection is yes!");
                    aSwitchInject = "Y";
                } else {
                    Log.i("info", "Injection is no");
                    aSwitchInject = "N";
                }
            }
        });
        editTextMorningStart = (EditText) view.findViewById(R.id.reg_morning_start);
        editTextMorningStart.setText(user.morning_start);
        editTextMorningStart.setOnClickListener(this);
        ImageView setMornStart = (ImageView) view.findViewById(R.id.setMorStartViewTime);
        setMornStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });

        editTextMorningEnd = (EditText) view.findViewById(R.id.reg_morning_end);
        editTextMorningEnd.setText(user.morning_end);
        editTextMorningEnd.setOnClickListener(this);
        ImageView setMornEnd = (ImageView) view.findViewById(R.id.setMorEndViewTime);
        setMornEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_ME();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextBreakfastStart = (EditText) view.findViewById(R.id.reg_breakfast_start);
        editTextBreakfastStart.setText(user.breakfast_start);
        editTextBreakfastStart.setOnClickListener(this);
        ImageView setBFStart = (ImageView) view.findViewById(R.id.setBFStartViewTime);
        setBFStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_BS();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextBreakfastEnd = (EditText) view.findViewById(R.id.reg_breakfast_end);
        editTextBreakfastEnd.setText(user.breakfast_end);
        editTextBreakfastEnd.setOnClickListener(this);
        ImageView setBFEnd = (ImageView) view.findViewById(R.id.setBFEndViewTime);
        setBFEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_BE();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextLunchStart = (EditText) view.findViewById(R.id.reg_lunch_start);
        editTextLunchStart.setText(user.lunch_start);
        editTextLunchStart.setOnClickListener(this);
        ImageView setLunchStart = (ImageView) view.findViewById(R.id.setLunchStartViewTime);
        setLunchStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_LS();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextLunchEnd = (EditText) view.findViewById(R.id.reg_lunch_end);
        editTextLunchEnd.setText(user.lunch_end);
        editTextLunchEnd.setOnClickListener(this);
        ImageView setLunchEnd = (ImageView) view.findViewById(R.id.setLunchEndViewTime);
        setLunchEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_LE();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextDinnerStart = (EditText) view.findViewById(R.id.reg_dinner_start);
        editTextDinnerStart.setText(user.dinner_start);
        editTextDinnerStart.setOnClickListener(this);
        ImageView setDinnerStart = (ImageView) view.findViewById(R.id.setDinnerStartViewTime);
        setDinnerStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_DS();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextDinnerEnd = (EditText) view.findViewById(R.id.reg_dinner_end);
        editTextDinnerEnd.setText(user.dinner_end);
        editTextDinnerEnd.setOnClickListener(this);
        ImageView setDinnerEnd = (ImageView) view.findViewById(R.id.setDinnerEndViewTime);
        setDinnerEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_DE();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextBedStart = (EditText) view.findViewById(R.id.reg_bed_start);
        editTextBedStart.setText(user.bed_start);
        editTextBedStart.setOnClickListener(this);
        ImageView setBedStart = (ImageView) view.findViewById(R.id.setBedStartViewTime);
        setBedStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_NS();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });
        editTextBedEnd = (EditText) view.findViewById(R.id.reg_bed_end);
        editTextBedEnd.setText(user.bed_end);
        editTextBedEnd.setOnClickListener(this);
        ImageView setBedEnd = (ImageView) view.findViewById(R.id.setBedEndViewTime);
        setBedEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new regTimePickFragment_NE();
                newFragment3.show(getActivity().getFragmentManager(), "Time Dialog");

            }
        });

        editTextPreMealLow = (EditText) view.findViewById(R.id.glucosepremeallow);
        editTextPreMealLow.setText(String.valueOf(user.pre_meal_low));

        editTextPreMealMed = (EditText) view.findViewById(R.id.glucosepremealmedium);
        editTextPreMealMed.setText(String.valueOf(user.pre_meal_med));

        editTextPreMealHigh = (EditText) view.findViewById(R.id.glucosepremealhigh);
        editTextPreMealHigh.setText(String.valueOf(user.pre_meal_high));

        editTextPostMealLow = (EditText) view.findViewById(R.id.glucosepostmeallow);
        editTextPostMealLow.setText(String.valueOf(user.post_meal_low));

        editTextPostMealMed = (EditText) view.findViewById(R.id.glucosepostmealmedium);
        editTextPostMealMed.setText(String.valueOf(user.post_meal_med));

        editTextPostMealHigh = (EditText) view.findViewById(R.id.glucosepostmealhigh);
        editTextPostMealHigh.setText(String.valueOf(user.post_meal_high));

        switchHeight = (ToggleButton) view.findViewById(R.id.switch_height);
        aSwitchHeight = user.height_unit;
        if (aSwitchHeight == 0)
            switchHeight.setChecked(false);
        else
            switchHeight.setChecked(true);
        switchHeight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i("info", "Height is inch!");
                    aSwitchHeight = 1;
                } else {
                    Log.i("info", "Height is cm");
                    aSwitchHeight = 0;
                }
            }
        });
        switchWeight = (ToggleButton) view.findViewById(R.id.switch_weight);
        aSwitchWeight = user.weight_unit;
        if (aSwitchWeight == 0)
            switchWeight.setChecked(false);
        else
            switchWeight.setChecked(true);
        switchWeight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i("info", "Weight is lb!");
                    aSwitchWeight = 1;
                } else {
                    Log.i("info", "Weight is kg");
                    aSwitchWeight = 0;
                }
            }
        });
        switchWaist = (ToggleButton) view.findViewById(R.id.switch_waist);
        aSwitchWaist = user.waist_unit;
        if (aSwitchWaist == 0)
            switchWaist.setChecked(false);
        else
            switchWaist.setChecked(true);
        switchWaist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i("info", "Waist is inch!");
                    aSwitchWaist = 1;
                } else {
                    Log.i("info", "Waist is cm");
                    aSwitchWaist = 0;
                }
            }
        });

        buttonRegister = (Button) view.findViewById(R.id.buttonPrefEdit);
        buttonRegister.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId()){
            case R.id.buttonPrefEdit:

                Fragment prev = getFragmentManager().findFragmentByTag("DatePicker");
                if (prev != null) {
                    DialogFragment df = (DialogFragment) prev;
                    df.dismiss();
                }
                Fragment prev2 = getFragmentManager().findFragmentByTag("Time Dialog");
                if (prev2 != null) {
                    DialogFragment df = (DialogFragment) prev2;
                    df.dismiss();
                }

                editPref();

                break;
        }
    }


    private void editPref() {
            int userID = userid;
            String name = editTextName.getText().toString();
            String birthday = editTextBday.getText().toString();
            int sex = aSwitchSex;
            int smoker =  aSwitchSmoker;
            String inject =  aSwitchInject;
            String morning_start = editTextMorningStart.getText().toString();
            String morning_end = editTextMorningEnd.getText().toString();
            String breakfast_start = editTextBreakfastStart.getText().toString();
            String breakfast_end = editTextBreakfastEnd.getText().toString();
            String lunch_start = editTextLunchStart.getText().toString();
            String lunch_end = editTextLunchEnd.getText().toString();
            String dinner_start = editTextDinnerStart.getText().toString();
            String dinner_end = editTextDinnerEnd.getText().toString();
            String bed_start = editTextBedStart.getText().toString();
            String bed_end = editTextBedEnd.getText().toString();
            int height_unit =  aSwitchHeight;
            int weight_unit =  aSwitchWeight;
            int waist_unit =  aSwitchWaist;

            //Janus
            int preMealLow = Integer.valueOf(editTextPreMealLow.getText().toString());
            int preMealMed = Integer.valueOf(editTextPreMealMed.getText().toString());
            int preMealHigh = Integer.valueOf(editTextPreMealHigh.getText().toString());
        int postMealLow = Integer.valueOf(editTextPostMealLow.getText().toString());
        int postMealMed = Integer.valueOf(editTextPostMealMed.getText().toString());
        int postMealHigh = Integer.valueOf(editTextPostMealHigh.getText().toString());

        final User user  =  new User(userID, name, birthday, sex,
                    smoker, inject, morning_start, morning_end, breakfast_start,
                    breakfast_end, lunch_start, lunch_end, dinner_start,
                    dinner_end, bed_start, bed_end, height_unit, weight_unit, waist_unit, preMealLow,
                preMealMed, preMealHigh, postMealLow, postMealMed, postMealHigh);

        ServerRequests serverRequests = new ServerRequests(ctx);
        serverRequests.editUserDataInBackground(user, new GetUserCallback() {
            @Override
            public void done(User returnedUser) {
                UserLocalStore userLocalStore = new UserLocalStore(ctx);
                userLocalStore.clearUserData();
                userLocalStore.storeUserData(user);
                userLocalStore.setUserLoggedIn(true);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });

    }




}