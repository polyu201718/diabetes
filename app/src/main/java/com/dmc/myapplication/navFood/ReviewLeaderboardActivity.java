package com.dmc.myapplication.navFood;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;
/**
 * Created by lamivan on 28/12/2017.
 */
public class ReviewLeaderboardActivity extends android.support.v4.app.Fragment {

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getActivity().setTitle(R.string.title_activity_review_leaderboard);

    View v = inflater.inflate(R.layout.activity_review_leaderboard, container, false);
    TextView textView = (TextView) v.findViewById(R.id.textView64);

    navfoodAsyncTask asyncTask = new navfoodAsyncTask(getActivity(), navFoodConstant.RETRIEVE_LEADERBOARD);
    asyncTask.execute();

    return v;

  }
   // @Override
  //protected void onCreate(Bundle savedInstanceState) {
    //overridePendingTransition(R.anim.slide1, R.anim.slide2);
   // super.onCreate(savedInstanceState);
   // setContentView(R.layout.activity_review_leaderboard);
   // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
   // setSupportActionBar(toolbar);

//    getSupportActionBar().setDisplayHomeAsUpEnabled(true);



//  }


}
