package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Spinner;

import com.dmc.myapplication.R;

import java.util.HashMap;

/**
 * Created by KwokSinMan on 4/1/2016.
 */
public class navFoodAddAllFrag extends Fragment {
    public navFoodAddAllFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.navfood_add_all,container,false);

        // set food Cate DDL  setOnItemSelectedListener
        GridView foodCateSpinner = (GridView) v.findViewById(R.id.foodCategories);
        foodCateSpinner.setOnItemClickListener(new foodCateDDLOnItemSelectedListener(this.getActivity()));

        // set food Sub Cate DDL setOnItemSelectedListener
        GridView foodSubCateSpinner = (GridView) v.findViewById(R.id.foodDetailCategories);
        foodSubCateSpinner.setOnItemClickListener(new foodSubCateDDLOnItemSelectedListener(this.getActivity()));
        foodSubCateSpinner.setVisibility(View.INVISIBLE);

        //set All food list setOnItemSelectedListener
        GridView gridView = (GridView) v.findViewById(R.id.allFoodListView);
        gridView.setOnItemClickListener(new foodAllListOnItemClickListener(this.getActivity()));
        gridView.setVisibility(View.INVISIBLE);
        navfoodAsyncTask connect = new navfoodAsyncTask(this.getActivity(), navFoodConstant.GET_FOOD_CATE_DDL);
        connect.execute();
        return v;
    }

    public class foodCateDDLOnItemSelectedListener implements GridView.OnItemClickListener {
        private Activity activity;

        public foodCateDDLOnItemSelectedListener(Activity activity){
            this.activity = activity;
        }

        //@Override
        //public void onItemSelected(AdapterView<?> adapter,View view,int position,long id) {
            //if ( ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()!=0 ){
                //获取选择的项的值
                //navfoodAsyncTask connect = new navfoodAsyncTask(this.activity, navFoodConstant.GET_FOOD_SUB_CATE_DDL);
                //connect.execute(String.valueOf( ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()));
                //
            //}
            //else{ // select "Please select ". than clear DDL of sub Food Cate
                //Spinner foodSubCateSpinner = (Spinner) activity.findViewById(R.id.foodDetailCategories);
                //foodSubCateSpinner.setTextAlignment("?android:attr/textAppearanceLarge");
                //foodSubCateSpinner.setAdapter(null);
            //}

            // clear allFoodListView items
            //GridView gridView = (GridView) this.activity.findViewById(R.id.allFoodListView);
          //  gridView.setAdapter(null);

        //}

        //@Override // 什么也没选
       // public void onNothingSelected(AdapterView<?> arg0) {}

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            GridView gridView = (GridView) this.activity.findViewById(R.id.foodCategories);

            navFoodBiz biz = new navFoodBiz();
            biz.setFoodSubCateDDLNew(getActivity(), "{}", String.valueOf( ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()));
           // gridView.setVisibility(View.INVISIBLE);
        }
    }

    public static class foodSubCateDDLOnItemSelectedListener implements GridView.OnItemClickListener, View.OnClickListener {
        private Activity activity;

        public foodSubCateDDLOnItemSelectedListener(Activity activity){
            this.activity = activity;
        }

        //@Override
        //public void onItemSelected(AdapterView<?> adapter,View view,int position,long id) {
           // if ( ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()!=0 ){
                //获取选择的项的值
                //navfoodAsyncTask connect = new navfoodAsyncTask(this.activity, navFoodConstant.GET_FOOD_All_LIST);
                //connect.execute(String.valueOf(((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()));
               // navFoodBiz biz = new navFoodBiz();
             //   biz.setFoodAllListNew(this.activity, "{}", String.valueOf(((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()));
           // }
            //else{
                ///clear food list
              //  GridView gridView = (GridView) this.activity.findViewById(R.id.allFoodListView);
            //    gridView.setAdapter(null);
          //  }
        //}
       // @Override // 什么也没选
        //public void onNothingSelected(AdapterView<?> arg0) {}

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            navFoodBiz biz = new navFoodBiz();
            biz.setFoodAllListNew(this.activity, "{}", String.valueOf(((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId()));

        }

        @Override
        public void onClick(View v) {

        }
    }

    private  class  foodAllListOnItemClickListener implements  GridView.OnItemClickListener {
        Activity activity;
        public foodAllListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int food_id=((navFoodBiz.DbRecordStringId) ((HashMap) adapter.getItemAtPosition(position)).get("food")).getId();
            Intent intent = new Intent();
            intent.setClass(activity, navFoodAddDetailActivity.class);
            intent.putExtra("food_id", food_id);
            intent.putExtra("is_free_text",navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(navFoodConstant.GET_FOOD_DATE, activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

}

/*
    public void getFoodCateDDL(View v,ViewGroup container) {
        navfoodAsyncTask  abc = new navfoodAsyncTask(v,container.getContext(), navFoodConstant.GET_FOOD_CATE_DDL);
        abc.execute();

/*
        myList1 = new ArrayList<String>();
        myList1.add("請選擇");
        myList1.add("點心");
        myList1.add("小食");
        Spinner foodCategoriesSpinner = (Spinner)v.findViewById(R.id.foodCategories);
        ArrayAdapter<String> myAdapter1 = new ArrayAdapter<String>(container.getContext(),android.R.layout.simple_dropdown_item_1line,myList1);
        foodCategoriesSpinner.setAdapter(myAdapter1);


        foodCategoriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long position1) {
                if (position != 0) {
                    Toast.makeText(getActivity().getBaseContext(), mList1.get(position).toString(),
                            Toast.LENGTH_SHORT).show();
                    myList2 = new ArrayList<String>();
                    myList2.add("請選擇");
                    myList2.add("炸點");
                    myList2.add("包點");
                    Spinner foodSubCateSpinner = (Spinner) getActivity().findViewById(R.id.foodDetailCategories);
                    ArrayAdapter<String> myAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, myList2);
                    foodSubCateSpinner.setAdapter(myAdapter2);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }
*/

//set food display.
        /*
                    String[] mobileArray = {"叉燒包","菜芯","白飯","上素蒸粉果","咖喱蒸魷魚","大包","山竹牛肉","棉花雞","灌湯餃","春卷",
                            "叉燒腸粉","燒賣","牛奶朱古力","節瓜","苦瓜","西生菜","全脂奶粉","香梨(連皮)","香梨(不連皮)","雪糕"};
                    List<Map<String, String>> items = new ArrayList<Map<String,String>>();
                    for (int i = 0; i < mobileArray.length; i++) {
                            Map<String, String> item = new HashMap<String, String>();
                            item.put("food", mobileArray[i]);
                        items.add(item);
                    }
                SimpleAdapter adapter = new SimpleAdapter(container.getContext(), items, R.layout.navfood_add_all_list, new String[]{"food"}, new int[]{R.id.itemName});
                GridView listView = (GridView) v.findViewById(R.id.allFoodListView);
                listView.setAdapter(adapter);
                */
