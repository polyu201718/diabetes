package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

/**
 * Created by KwokSinMan on 28/12/2015.
 */
public class navFoodAddCommonFrag extends Fragment {

    public navFoodAddCommonFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.navfood_add_common,container,false);

        // set food Common list  OnItemClickListener
       // ListView listView = (ListView) v.findViewById(R.id.commonListView);
        //listView.setOnItemClickListener(new foodCommonListOnItemClickListener (this.getActivity()));

        //UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        //String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        //navfoodAsyncTask connect = new navfoodAsyncTask(this.getActivity(), navFoodConstant.GET_FOOD_COMMON_LIST);
        //connect.execute(userId);



        return v;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        System.gc();
       //bitmap.recycle()
       // Drawable.setCallback(null);
    }

    private class foodCommonListOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public foodCommonListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int food_id= ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId();
            //System.out.println("Candy food_id=" + food_id);
            Intent intent = new Intent();
            intent.setClass(activity, navFoodAddDetailActivity.class);
            intent.putExtra("food_id", food_id);
            intent.putExtra("is_free_text",navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(navFoodConstant.GET_FOOD_DATE, activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

}

// Array of strings...
//String[] mobileArray = {"叉燒包","菜芯","白飯","上素蒸粉果","咖喱蒸魷魚","大包","山竹牛肉","棉花雞","灌湯餃","春卷",
//        "叉燒腸粉","燒賣","牛奶朱古力","節瓜","苦瓜","西生菜","全脂奶粉","香梨(連皮)","香梨(不連皮)","雪糕"};
       /*
        navFoodBiz foodBiz = new navFoodBiz();
        List<navFoodBiz.DbRecordStringId> mobileArray = foodBiz.getCommentFoodList();
        String[] imagePathArray = {"food_1","food_2","food_3","food_4","food_5","food_6","food_7","food_8","food_9","food_10",
                "food_11","food_12","food_13","food_14","food_15","food_16","food_17","food_18","food_19","food_20"};

        Integer[] imagePath = findImageIdFromRes(imagePathArray,container);
        */
/*
        List<Map<String, navFoodBiz.DbRecordStringId>> items = new ArrayList<Map<String,navFoodBiz.DbRecordStringId>>();
        for (int i=0; i<mobileArray.length; i++){
                Map<String, navFoodBiz.DbRecordStringId> item = new HashMap<String, navFoodBiz.DbRecordStringId>();
                item.put("food", new navFoodBiz.DbRecordStringId(JA.getJSONObject(i).getString("FOOD_NAME"), JA.getJSONObject(i).getInt("FOOD_ID")));
                items.add(item);
        }
*/
// navFoodAddCommonListViewAdapter adapter = new navFoodAddCommonListViewAdapter(this.getActivity(), mobileArray, imagePath);
/*
        Integer[] imagePath={ R.drawable.ic_add, R.drawable.ic_date_range_black_24dp,  R.drawable.ic_clear_black_24dp,
                R.drawable.ic_menu_blood,     R.drawable.ic_menu_eat,    R.drawable.ic_menu_setting,
                R.drawable.ic_info_black_24dp,     R.drawable.ic_menu_chart,    R.drawable.ic_menu_quest,
                R.drawable.ic_menu_med,

                R.drawable.ic_add, R.drawable.ic_date_range_black_24dp,  R.drawable.ic_clear_black_24dp,
                R.drawable.ic_menu_blood,     R.drawable.ic_menu_eat,    R.drawable.ic_menu_setting,
                R.drawable.ic_info_black_24dp,     R.drawable.ic_menu_chart,    R.drawable.ic_menu_quest,
                R.drawable.ic_menu_med};
*/

/*

        int resID = getResources().getIdentifier("ic_date_range_black_24dp", "drawable", container.getContext().getPackageName());
        int resID_test = getResources().getIdentifier("food_123", "drawable", container.getContext().getPackageName());
        System.out.println("Package Name ="+container.getContext().getPackageName());
        System.out.println("resID ="+resID);
        System.out.println("resID_test ="+resID_test);
        System.out.println("R.drawable.ic_date_range_black_24dp ="+R.drawable.ic_date_range_black_24dp);


        ArrayAdapter adapter = new ArrayAdapter<String>(container.getContext(), R.layout.navfood_add_common_list, R.id.itemname, mobileArray);
*/