package com.dmc.myapplication.navFood;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.util.Base64;
import android.util.Log;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.jakewharton.disklrucache.DiskLruCache;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 20/1/2017.
 */
public class navFoodImageFactory {
    private LruCache<String, Bitmap> mMemoryCache;
    private DiskLruCache mDiskLruCache;


    public static Bitmap getImage(final Context context, final String filename){
        Log.d("navFoodImageFactory", "filename = "+filename);
        File file = new File(context.getFilesDir(), filename+".jpg");
        if (file.exists()){
            try {
                FileInputStream streamIn = new FileInputStream(file);
                Bitmap bitmap = BitmapFactory.decodeStream(streamIn);
                return bitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        return null;
    }

    public File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath + File.separator + uniqueName);
    }

    public String hashKeyForDisk(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    private String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }


    public static String sendRequest(String Method, String data){
        return webHelper.sendRequest(Method, data);
    }

    public static OutputStream sendRequest(String Method, String data, OutputStream outputStream){
        /*HttpURLConnection conn = null;
        try {
            // 创建一个URL对象
            URL mURL = new URL(systemConstant.SERVER_ADDRESS);
            // 调用URL的openConnection()方法,获取HttpURLConnection对象
            conn = (HttpURLConnection) mURL.openConnection();

            conn.setRequestMethod(Method);// 设置请求方法为post
            //conn.setReadTimeout(3000);// 设置读取超时为5秒
            conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
            conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

            // post请求的参数
            //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
            // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容

            OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
            out.write(data.getBytes());
            outputStream.write(data.getBytes());
            out.flush();
            out.close();

            int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
            if (responseCode == 200) {
                InputStream is = conn.getInputStream();
                String state = getStringFromInputStream(is);
                System.out.println(data);
                System.out.println(state);

                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }

                //return true;
                return out;
            } else {
                System.out.println(systemConstant.SERVER_ADDRESS);
                System.out.println("Unable to reach the server. errorcode:" + responseCode);
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();// 关闭连接
            }
        }
        return null;*/

        if (systemConstant.SERVER_ADDRESS.contains("https")){
            HttpsURLConnection conn = null;
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                    @Override
                    public boolean verify (String var1, SSLSession var2){
                        Log.i("RestUtilImpl", "Approving certificate for " + var1);
                        return true;
                    }
                });
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (GeneralSecurityException e) {
            }


            try {
                // 创建一个URL对象
                URL mURL = new URL(systemConstant.SERVER_ADDRESS);
                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpsURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(3000);// 设置读取超时为5秒
                conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容

                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                outputStream.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    String state = getStringFromInputStream(is);
                    System.out.println(data);
                    System.out.println(state);

                    if (conn != null) {
                        conn.disconnect();// 关闭连接
                    }

                    //return true;
                    return out;
                } else {
                    System.out.println(systemConstant.SERVER_ADDRESS);
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }else {
            HttpURLConnection conn = null;

            try {
                // 创建一个URL对象
                URL mURL = new URL(systemConstant.SERVER_ADDRESS);
                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(3000);// 设置读取超时为5秒
                conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容

                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                outputStream.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    String state = getStringFromInputStream(is);
                    System.out.println(data);
                    System.out.println(state);

                    if (conn != null) {
                        conn.disconnect();// 关闭连接
                    }

                    //return true;
                    return out;
                } else {
                    System.out.println(systemConstant.SERVER_ADDRESS);
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }

        return null;
    }


    private static String getStringFromInputStream(InputStream is)
            throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        // 模板代码 必须熟练
        byte[] buffer = new byte[1024];
        int len = -1;
        // 一定要写len=is.read(buffer)
        // 如果while((is.read(buffer))!=-1)则无法将数据写入buffer中
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        is.close();
        String state = os.toString();// 把流中的数据转换成字符串,采用的编码是utf-8(模拟器默认编码)
        os.close();
        return state;
    }


}
