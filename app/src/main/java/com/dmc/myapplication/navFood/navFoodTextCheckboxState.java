package com.dmc.myapplication.navFood;

/**
 * Created by lamivan on 13/11/2017.
 */

public class navFoodTextCheckboxState {
  private String title;
  private boolean selected;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }
}
