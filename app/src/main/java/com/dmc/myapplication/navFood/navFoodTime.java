package com.dmc.myapplication.navFood;

import java.util.Calendar;

/**
 * Created by KwokSinMan on 23/1/2016.
 */
public class navFoodTime{
    private int hour;
    private int min;
    public navFoodTime(){

    }
    public navFoodTime(String time){
        String [] temp = time.split(":");
        this.hour = Integer.parseInt(temp[0]);
        this.min = Integer.parseInt(temp[1]);
    }
    public int getHour() {
        return hour;
    }
    public int getMin() {
        return min;
    }

    public String getTimeString(int inputedMin){
        if (0<=inputedMin && inputedMin <=9)
            return "0" + String.valueOf(inputedMin);
        else return String.valueOf(inputedMin);
    }

    public String getDBDateFormal(String uiDateFormal){
        String [] temp = uiDateFormal.split("/");
        uiDateFormal = temp[2]+"-"+temp[1]+"-"+temp[0];  // DB = YYYY-MM-DD
        return uiDateFormal;
    }

    public String getUIDateFormal(String dbDateFormal){
        String [] temp = dbDateFormal.split("-");
        dbDateFormal = temp[2]+"/"+temp[1]+"/"+temp[0];  // UI = DD/MM/YYYY
        return  dbDateFormal;
    }

    public static String getVurrentDateInDBFormal(){
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return yy + "-" + mm + "-" +dd;
    }

    public String getCurrentTimeFormal(){
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        return getTimeString(hour) + ":" + getTimeString(min);
    }


}
