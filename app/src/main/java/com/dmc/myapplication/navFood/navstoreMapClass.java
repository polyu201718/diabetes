package com.dmc.myapplication.navFood;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lamivan on 23/12/2017.
 */

public   class navstoreMapClass {
  public static List<HashMap<String, String>> map = new ArrayList<HashMap<String, String>>();

  public navstoreMapClass() {
  }

  public static List<HashMap<String, String>> getMap() {
    return map;
  }

  public static void setMap(List<HashMap<String, String>> maps) {
    map = maps;
  }
}
