package com.dmc.myapplication.network;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by tsunmingtso on 19/12/2017.
 */

public interface RetrofitInterface {
    @GET("FetchUserData_NEW.php")
    Call<ResponseBody> login(@Query("user_id") int userID);

    @GET("BodyRecord/saveBodyRecord.php")
    Call<ResponseBody> insertBodyIndexRecord(@Query("user_id") int userID, @Query("recordType") int recordType,
                                             @Query("insertDate") Date insertDate, @Query("height") double height,
                                             @Query("weight") double weight, @Query("waistLine") double waistLine,
                                             @Query("BMI") double BMI, @Query("fat") double fat);

    @GET("BodyRecord/getBodyRecord.php")
    Call<ResponseBody> getBodyIndexRecords(@Query("user_id") int userID, @Query("recordType") int recordType);

    @GET("BodyRecord/getBodyRecordByDate.php")
    Call<ResponseBody> getBodyIndexRecordsByDate(@Query("user_id") int userID, @Query("recordType") int recordType, @Query("date") String date,
                                                 @Query("length") int length);

    @GET("BodyRecord/saveBodyRecord.php")
    Call<ResponseBody> insertBloodTestRecord(@Query("user_id") int userID, @Query("recordType") int recordType,
                                             @Query("insertDate") Date insertDate, @Query("FASTING_BLOOD_SUGAR") double fastingBloodSugar,
                                             @Query("HbA1c") double hba1c, @Query("TOTAL_C") double totalC,
                                             @Query("HDL_C") double ldlC, @Query("LDL_C") double hdlC,
                                             @Query("TRIGLYCERIDES") double triglycerides);

    @GET("BodyRecord/getBodyRecord.php")
    Call<ResponseBody> getBloodTestRecords(@Query("user_id") int userID, @Query("recordType") int recordType);

    @GET("navFood/saveFoodAssess.php")
    Call<ResponseBody> saveFoodAssessment(@Query("user_id") int userID,
                                          @Query("food_assess_date") Date insertDate,
                                          @Query("food_assess_rating") double rating,
                                          @Query("food_assess_q2") String question2,
                                          @Query("food_assess_q3a") String question3a,
                                          @Query("food_assess_q3b1") String question3b1,
                                          @Query("food_assess_q3b2") String question3b2,
                                          @Query("food_assess_q3b3") String question3b3,
                                          @Query("food_assess_create") String timeStamp
    );
    @GET("navFood/getFoodAssess.php")
    Call<ResponseBody> getFoodAssessment(@Query("user_id") int userID,
                                         @Query("food_assess_date") Date insertDate
    );

    @GET("goal/saveGoalSetting.php")
    Call<ResponseBody> saveGoalSetting(@Query("user_id") int userID, @Query("goal_date") Date goalLength,
                                       @Query("goal_length") int length, @Query("goal_type") int type,
                                       @Query("witness") String witness,
                                       @Query("goal_create") String createtimestamp, @Query("goal_reason") String reason,
                                       @Query("goal_detail") JSONArray BRList, @Query("other_goal") JSONArray BRList2);
    @GET("goal/editGoalSetting.php")
    Call<ResponseBody> editGoalSetting(@Query("user_id") int userID, @Query("goal_length") int length,
                                       @Query("goal_type") int type, @Query("witness") String witness,
                                       @Query("goal_edit") String edittimestamp, @Query("goal_detail") JSONArray BRList,
                                       @Query("goal_id") int goalID, @Query("other_goal") JSONArray BRList2
                                       );

    @GET("navFood/getFoodRecord.php")
    Call<ResponseBody> getFoodRecord(@Query("user_id") int userID, @Query("food_date") String date);
}
