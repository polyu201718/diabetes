package com.dmc.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * Created by KwokSinMan on 23/2/2016.
 */
public class noInternetConnectionDialog extends DialogFragment implements DialogInterface.OnClickListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        TextView messageTextView =  new TextView(getActivity());
        messageTextView.setText("請確定手機有良好的網絡訊號");
        messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        messageTextView.setPadding(0, 30, 0, 30);

        return new AlertDialog.Builder(getActivity())
                .setTitle("手機未連線")
                .setView(messageTextView)
                .setPositiveButton("確定", null)
                .create();
    }
    @Override
    public void onClick(DialogInterface dialog, int whichButton){}
}