package com.dmc.myapplication.otherBodyIndex;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.pojo.BodyIndexRecord;
import com.dmc.myapplication.util.DateUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsunmingtso on 28/12/2017.
 */

public class Page_WeightChart extends Fragment {
    LineChart lineChart_Weight, lineChart_BMI;
    BarChart barChart_Waist, barChart_Fat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_weightchart, container, false);

        lineChart_Weight = (LineChart) view.findViewById(R.id.lineChart_Weight);
        lineChart_BMI = (LineChart) view.findViewById(R.id.lineChart_BMI);
        barChart_Waist = (BarChart) view.findViewById(R.id.barChart_Waist);
        barChart_Fat = (BarChart) view.findViewById(R.id.barChart_Fat);

        RealmFunction realmFunction = new RealmFunction(getContext());
        DateUtil dateUtil = new DateUtil();

        /* LineChart - Weight */
        List<BodyIndexRecord> list_Weight = realmFunction.getFirstFiveWeight();

        List<Entry> entries_Weight = new ArrayList<>();
        int index_Weight = 0;
        for (int i = list_Weight.size() - 1; i >= 0; i--) {
            entries_Weight.add(new Entry((float) list_Weight.get(i).getWEIGHT(), index_Weight++));
        }

        LineDataSet lineDataSet_Weight = new LineDataSet(entries_Weight, "體重變化趨勢");
        lineDataSet_Weight.setValueTextSize(20f); // set value textSize

        Legend legend_Weight = lineChart_Weight.getLegend();
        legend_Weight.setTextSize(20f); // set legend textSize

        List<String> labels_Weight = new ArrayList<>();
        for (int i = list_Weight.size() - 1; i >= 0; i--) {
            labels_Weight.add(dateUtil.dateToString_English(list_Weight.get(i).getDATE()));
        }

        /* Add Middle Line */
        YAxis leftAxis = lineChart_Weight.getAxisLeft();
        LimitLine limitLine_Dream = new LimitLine(50f, "理想");
        limitLine_Dream.setLineColor(Color.RED);
        limitLine_Dream.setTextSize(30);
        LimitLine limitLine_Over = new LimitLine(80f, "過重");
        limitLine_Over.setLineColor(Color.GREEN);
        limitLine_Over.setTextSize(30);

        leftAxis.addLimitLine(limitLine_Dream);
        leftAxis.addLimitLine(limitLine_Over);

        LineData lineData_Weight = new LineData(labels_Weight, lineDataSet_Weight);
        lineChart_Weight.setData(lineData_Weight);
        lineChart_Weight.setDescription("KG");
        lineChart_Weight.setDescriptionTextSize(50f); // set description textSize

        /* LineChart - BMI */
        List<BodyIndexRecord> list_BMI = realmFunction.getFirstFiveBMI();

        List<Entry> entries_BMI = new ArrayList<>();
        int index_BMI = 0;
        for (int i = list_BMI.size() - 1; i >= 0; i--) {
            entries_BMI.add(new Entry((float) list_BMI.get(i).getBMI(), index_BMI++));
        }

        LineDataSet lineDataSet_BMI = new LineDataSet(entries_BMI, "BMI");
        lineDataSet_BMI.setValueTextSize(20f); // set value textSize

        Legend legend_BMI = lineChart_BMI.getLegend();
        legend_BMI.setTextSize(20f); // set legend textSize

        List<String> labels_BMI = new ArrayList<>();
        for (int i = list_BMI.size() - 1; i >= 0; i--) {
            labels_BMI.add(dateUtil.dateToString_English(list_BMI.get(i).getDATE()));
        }

        /* Add Middle Line */
        YAxis leftAxis_BMI = lineChart_BMI.getAxisLeft();
        LimitLine limitLine_Dream_BMI = new LimitLine(18.5f, "理想");
        limitLine_Dream_BMI.setLineColor(Color.RED);
        limitLine_Dream_BMI.setTextSize(30);
        LimitLine limitLine_Over_BMI = new LimitLine(22.9f, "過重");
        limitLine_Over_BMI.setLineColor(Color.GREEN);
        limitLine_Over_BMI.setTextSize(30);

        leftAxis_BMI.addLimitLine(limitLine_Dream_BMI);
        leftAxis_BMI.addLimitLine(limitLine_Over_BMI);

        LineData lineData_BMI = new LineData(labels_BMI, lineDataSet_BMI);
        lineChart_BMI.setData(lineData_BMI);
        lineChart_BMI.setDescription("");
        lineChart_BMI.setDescriptionTextSize(50f); // set description textSize

        /* BarChart - Waist */
        List<BodyIndexRecord> list_Waist = realmFunction.getFirstFiveWaist();

        List<BarEntry> barEntries_Waist = new ArrayList<>();
        int index_Waist = 0;
        for (int i = list_Waist.size() - 1; i >= 0; i--) {
            barEntries_Waist.add(new BarEntry((float) list_Waist.get(i).getWAIST(), index_Waist++));
        }

        BarDataSet barDataSet_Waist = new BarDataSet(barEntries_Waist, "腰圍");
        barDataSet_Waist.setValueTextSize(20f); // set value textSize

        Legend legend_Waist = barChart_Waist.getLegend();
        legend_Waist.setTextSize(20f); // set legend textSize

        List<String> labels_Waist = new ArrayList<>();
        for (int i = list_Waist.size() - 1; i >= 0; i--) {
            labels_Waist.add(dateUtil.dateToString_English(list_Waist.get(i).getDATE()));
        }

        BarData barData_Waist = new BarData(labels_Waist, barDataSet_Waist);
        barChart_Waist.setData(barData_Waist);
        barChart_Waist.setDescription("厘米");
        barChart_Waist.setDescriptionTextSize(50f); // set description textSize

        /* BarChart - Fat */
        List<BodyIndexRecord> list_Fat = realmFunction.getFirstFiveFat();

        List<BarEntry> barEntries_Fat = new ArrayList<>();
        int index_Fat = 0;
        for (int i = list_Fat.size() - 1; i >= 0; i--) {
            barEntries_Fat.add(new BarEntry((float) list_Fat.get(i).getFAT(), index_Fat++));
        }

        BarDataSet barDataSet_Fat = new BarDataSet(barEntries_Fat, "脂肪比率");
        barDataSet_Fat.setValueTextSize(20f); // set value textSize

        Legend legend_Fat = barChart_Fat.getLegend();
        legend_Fat.setTextSize(20f); // set legend textSize

        List<String> labels_Fat = new ArrayList<>();
        for (int i = list_Fat.size() - 1; i >= 0; i--) {
            labels_Fat.add(dateUtil.dateToString_English(list_Fat.get(i).getDATE()));
        }

        BarData barData_Fat = new BarData(labels_Fat, barDataSet_Fat);
        barChart_Fat.setData(barData_Fat);
        barChart_Fat.setDescription("%");
        barChart_Fat.setDescriptionTextSize(50f); // set description textSize

        return view;
    }
}