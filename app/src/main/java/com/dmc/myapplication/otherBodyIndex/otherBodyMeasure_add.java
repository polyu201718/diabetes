package com.dmc.myapplication.otherBodyIndex;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.rewards;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navfoodAsyncTask;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.pojo.BodyIndexRecord;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.util.DateUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Po on 14/3/2016.
 */
public class otherBodyMeasure_add extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private String TAG = this.getClass().getSimpleName();

    Context ctx;
    private EditText editTextDate, editTextTime;
    UserLocalStore userLocalStore;
    User user;

    private EditText et_Height;
    private Spinner sp_HeightUnit;
    private EditText et_Weight;
    private Spinner sp_WeightUnit;
    private TextView tv_DreamWeight;
    private TextView et_WaistLine;
    private Spinner sp_WaistLineUnit;
    private TextView tv_DreamWaistLine;
    private TextView tv_BMI;
    private EditText et_Fat;
    private TextView tv_DreamFat;
    private Button btn_Confirm;

    private double height, weight, waistLine, BMI, fat = 0;
    private static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherbodymeasure_add);

        ctx = getApplicationContext();

        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();

        final RealmFunction realmFunction = new RealmFunction(otherBodyMeasure_add.this);
        List<BodyIndexRecord> records = realmFunction.anyBodyIndexRecordDidNotUpload();
        if (records.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("有紀錄還未上傳到伺服器，是否上傳？")
                    .setPositiveButton("上傳", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            realmFunction.setRecordUploaded();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(otherBodyMeasure_add.this);
                builder.setMessage("你現在的紀錄將不會儲存，是否離開？")
                        .setPositiveButton("離開", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                onBackPressed();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.om_add_date);
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan = 0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText(yy + "-" + mm + "-" + dd);

        ImageView setOmDate = (ImageView) findViewById(R.id.setOm_add_date);
        setOmDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new otherBodyMeasureDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.om_add_time);
        Calendar time = Calendar.getInstance();
        int HH = time.get(Calendar.HOUR_OF_DAY);
        int MM = time.get(Calendar.MINUTE);

        editTextTime.setText(HH + ":" + MM + ":00");
        ImageView setTime = (ImageView) findViewById(R.id.setOm_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3 = new otherBodyMeasureTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });

        initUI();
        setHandlerFunction();
        setupSpinner();
        setupEditText();
        setupTextView();
        setupButton();
    }

    private void initUI() {
        et_Height = (EditText) findViewById(R.id.et_Height);
        sp_HeightUnit = (Spinner) findViewById(R.id.sp_HeightUnit);
        et_Weight = (EditText) findViewById(R.id.et_Weight);
        sp_WeightUnit = (Spinner) findViewById(R.id.sp_WeightUnit);
        tv_DreamWeight = (TextView) findViewById(R.id.tv_DreamWeight);
        et_WaistLine = (EditText) findViewById(R.id.et_WaistLine);
        sp_WaistLineUnit = (Spinner) findViewById(R.id.sp_WaistLineUnit);
        tv_DreamWaistLine = (TextView) findViewById(R.id.tv_DreamWaistLine);
        tv_BMI = (TextView) findViewById(R.id.tv_BMI);
        et_Fat = (EditText) findViewById(R.id.et_Fat);
        tv_DreamFat = (TextView) findViewById(R.id.tv_DreamFat);
        btn_Confirm = (Button) findViewById(R.id.btn_Confirm);
    }

    private void setHandlerFunction() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        builder.setMessage("資料已成功儲存")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        handleBackAction();
                                    }
                                })
                                .show();
                        break;
                    case 1:
                        builder.setMessage("資料暫存到手機位置，請在有網絡情況下再上傳到伺服器")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        handleBackAction();
                                    }
                                })
                                .show();
                        break;
                }
            }
        };
    }

    private void setupSpinner() {
        final String[] heightUnit = {"厘米", "米"};
        ArrayAdapter<String> heightUnitList = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                heightUnit);
        sp_HeightUnit.setAdapter(heightUnitList);

        sp_HeightUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextSize(14);

                if (sp_HeightUnit.getSelectedItemId() == 0) {
                    try {
                        height = Double.valueOf(et_Height.getText().toString());
                        height = height * 100;
                        et_Height.setText(String.format(Locale.TAIWAN, "%.1f", height));

                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Number Format is Wrong", e);
                        height = 0;
                    }

                } else if (sp_HeightUnit.getSelectedItemId() == 1) {
                    try {
                        height = Double.valueOf(et_Height.getText().toString());
                        height = height / 100;
                        et_Height.setText(String.format(Locale.TAIWAN, "%.2f", height));

                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Number Format is Wrong", e);
                        height = 0;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final String[] weightUnit = {"磅", "公斤"};
        ArrayAdapter<String> weightUnitList = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                weightUnit);
        sp_WeightUnit.setAdapter(weightUnitList);

        sp_WeightUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextSize(14);

                if (sp_WeightUnit.getSelectedItemId() == 0) {
                    try {
                        weight = Double.valueOf(et_Weight.getText().toString());
                        weight = weight * 2.2;
                        et_Weight.setText(String.format(Locale.TAIWAN, "%.1f", weight));

                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Number Format is Wrong", e);
                        weight = 0;
                    }

                } else if (sp_WeightUnit.getSelectedItemId() == 1) {
                    try {
                        weight = Double.valueOf(et_Weight.getText().toString());
                        weight = weight / 2.2;
                        et_Weight.setText(String.format(Locale.TAIWAN, "%.1f", weight));

                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Number Format is Wrong", e);
                        weight = 0;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final String[] waistLineUnit = {"厘米", "吋"};
        ArrayAdapter<String> waistLineUnitList = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                waistLineUnit);
        sp_WaistLineUnit.setAdapter(waistLineUnitList);

        sp_WaistLineUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextSize(14);

                if (sp_WaistLineUnit.getSelectedItemId() == 0) {
                    try {
                        waistLine = Double.valueOf(et_WaistLine.getText().toString());
                        waistLine = waistLine * 2.54;
                        et_WaistLine.setText(String.format(Locale.TAIWAN, "%.1f", waistLine));

                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Number Format is Wrong", e);
                        waistLine = 0;
                    }

                } else if (sp_WaistLineUnit.getSelectedItemId() == 1) {
                    try {
                        waistLine = Double.valueOf(et_WaistLine.getText().toString());
                        waistLine = waistLine / 2.54;
                        et_WaistLine.setText(String.format(Locale.TAIWAN, "%.1f", waistLine));

                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Number Format is Wrong", e);
                        waistLine = 0;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupEditText() {
        et_Height.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (sp_HeightUnit.getSelectedItemPosition() == 0) {
                        height = Double.valueOf(et_Height.getText().toString()) / 100;
                    } else {
                        height = Double.valueOf(et_Height.getText().toString());
                    }
                } catch (NumberFormatException e) {
                    Log.e(TAG, "Number Format is Wrong", e);
                    height = 0;
                }

                if (height != 0 && weight != 0) {
                    BMI = weight / Math.pow(height, 2);
                    tv_BMI.setText(String.format(Locale.TAIWAN, "%.1f", BMI));
                } else {
                    tv_BMI.setText("");
                }

                /* Remove later */
                String lowerDreamWeight = String.format(Locale.TAIWAN, "%.1f", 18.5 * Math.pow(height, 2));
                String upperDreamWeight = String.format(Locale.TAIWAN, "%.1f", 22.9 * Math.pow(height, 2));
                String dreamWeight = String.format(Locale.TAIWAN, "%s - %s公斤", lowerDreamWeight, upperDreamWeight);
                tv_DreamWeight.setText(dreamWeight);
            }
        });

        et_Weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (sp_WeightUnit.getSelectedItemPosition() == 0) {
                        weight = Double.valueOf(et_Weight.getText().toString()) / 2.2;
                    } else {
                        weight = Double.valueOf(et_Weight.getText().toString());
                    }
                } catch (NumberFormatException e) {
                    Log.e(TAG, "Number Format is Wrong", e);
                    weight = 0;
                }

                if (height != 0 && weight != 0) {
                    BMI = weight / Math.pow(height, 2);
                    tv_BMI.setText(String.format(Locale.TAIWAN, "%.1f", BMI));
                } else {
                    tv_BMI.setText("");
                }
            }
        });

        et_Fat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(otherBodyMeasure_add.this);
                LayoutInflater inflater = otherBodyMeasure_add.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_addbodyindex, null);
                builder.setView(dialogView);

                SeekBar sb_Fat = (SeekBar) dialogView.findViewById(R.id.sb_Fat);
                final EditText et_FatValue = (EditText) dialogView.findViewById(R.id.et_FatValue);

                RealmFunction realmFunction = new RealmFunction(otherBodyMeasure_add.this);
                int sex = realmFunction.getUserInfo().getSex();

                if (sex == 0) {
                    sb_Fat.setProgress(18);
                    et_FatValue.setText("18");
                } else if (sex == 1) {
                    sb_Fat.setProgress(10);
                    et_FatValue.setText("10");
                }

                sb_Fat.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        et_FatValue.setText(String.valueOf(seekBar.getProgress()));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        et_Fat.setText(et_FatValue.getText().toString());
                        dialogInterface.dismiss();
                    }
                });

                builder.show();
            }
        });
    }

    private void setupTextView() {
        RealmFunction realmFunction = new RealmFunction(this);
        int sex = realmFunction.getUserInfo().getSex();

        if (sex == 0) {
            tv_DreamWaistLine.setText("少於80厘米/31.5吋");
            tv_DreamFat.setText("17 - 27%");

        } else if (sex == 1) {
            tv_DreamWaistLine.setText("少於90厘米/35.4吋");
            tv_DreamFat.setText("14 - 23%");
        }
    }

    private void setupButton() {
        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertRecord();
            }
        });
    }

    private void insertRecord() {
        String weight = et_Weight.getText().toString();
        String waistLine = et_WaistLine.getText().toString();
        String fat = et_Fat.getText().toString();

        if (weight.isEmpty() && waistLine.isEmpty() && fat.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("請至少輸入一項")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
        } else {
            // save to server
            RealmFunction realmFunction = new RealmFunction(this);
            int userID = realmFunction.getUserInfo().getUser_id();

            DateUtil dateUtil = new DateUtil();
            Date insertDate = dateUtil.stringToDate(editTextDate.getText().toString());

            int weightError = 0, waistLineError = 0, fatError = 0;

            if (!et_Height.getText().toString().isEmpty()) {
                if (sp_HeightUnit.getSelectedItemId() == 0) {
                    height = Double.valueOf(et_Height.getText().toString());
                    this.height = this.height / 100;
                } else if (sp_HeightUnit.getSelectedItemId() == 1) {
                    height = Double.valueOf(et_Height.getText().toString());
                }
            }
            if (!et_Weight.getText().toString().isEmpty()) {
                if (sp_WeightUnit.getSelectedItemId() == 0) {
                    this.weight = Double.valueOf(et_Weight.getText().toString());
                    this.weight = this.weight / 2.2;
                } else if (sp_WeightUnit.getSelectedItemId() == 1) {
                    this.weight = Double.valueOf(et_Weight.getText().toString());
                }
            }
            if (!et_WaistLine.getText().toString().isEmpty()) {
                if (sp_WaistLineUnit.getSelectedItemId() == 0) {
                    this.waistLine = Double.valueOf(et_WaistLine.getText().toString());
                    this.waistLine = this.waistLine / 2.54;
                } else if (sp_WaistLineUnit.getSelectedItemId() == 1) {
                    this.waistLine = Double.valueOf(et_WaistLine.getText().toString());
                }
            }
            if (!tv_BMI.getText().toString().isEmpty()) {
                BMI = Double.valueOf(tv_BMI.getText().toString());
            }
            if (!et_Fat.getText().toString().isEmpty()) {
                this.fat = Double.valueOf(et_Fat.getText().toString());
            }

            if (this.weight < 30 || this.weight >= 150) {
                if (this.weight != 0) {
                    weightError = 1;

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("請核實輸入的體重讀數是否正確")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }
            }

            if (this.waistLine < 15 || this.waistLine >= 59) {
                if (this.waistLine != 0) {
                    waistLineError = 1;

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("請核實輸入的腰圍讀數是否正確")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }
            }

            if (this.fat < 10 || this.fat >= 60) {
                if (this.fat != 0) {
                    fatError = 1;

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("請核實輸入的脂肪比率讀數是否正確")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }
            }

            if (weightError == 0 && waistLineError == 0 && fatError == 0) {
                RetrofitFunction retrofitFunction = new RetrofitFunction(otherBodyMeasure_add.this, webService.serverURL);
                retrofitFunction.insertBodyIndexRecord(userID, 1, insertDate, height, this.weight, this.waistLine, BMI, this.fat);
                // save in local
//                realmFunction.saveBodyIndexRecord(userID, 1, insertDate, height, this.weight, this.waistLine, BMI, this.fat);
            }

            /* Ivan */
            ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
            String currentTimestamp = s.format(new Date());
            if (!et_Weight.getText().toString().isEmpty()) {
                rewards rewards = new rewards(getApplicationContext());
                List<HashMap<String, String>> result = new ArrayList<>();

                result = rewards.getAllCreatedatetimeEditdatetime();
                System.out.println("current rewards other weight: " + result);

                if (isConnected || (!result.get(0).get("edit_datetime").substring(0, 8).equals(currentTimestamp.substring(0, 8)))) {
                    navfoodAsyncTask asyncTask = new navfoodAsyncTask(otherBodyMeasure_add.this, navFoodConstant.SAVE_REWARDS_DATA);
                    asyncTask.execute(userId, String.valueOf(Integer.valueOf(result.get(0).get("FOOD_REWARDS")) + 10), currentTimestamp);
                }
                rewards.update(rewards.new foodRewards_class(0, Integer.valueOf(userId), Integer.valueOf(result.get(0).get("FOOD_REWARDS")) + 10));
            }
            if (!et_WaistLine.getText().toString().isEmpty()) {
                rewards rewards = new rewards(getApplicationContext());
                List<HashMap<String, String>> result = new ArrayList<>();

                result = rewards.getAllCreatedatetimeEditdatetime();
                System.out.println("current rewards waist line: " + result);

                if (isConnected || (!result.get(0).get("edit_datetime").substring(0, 8).equals(currentTimestamp.substring(0, 8)))) {
                    navfoodAsyncTask asyncTask = new navfoodAsyncTask(otherBodyMeasure_add.this, navFoodConstant.SAVE_REWARDS_DATA);
                    asyncTask.execute(userId, String.valueOf(Integer.valueOf(result.get(0).get("FOOD_REWARDS")) + 10), currentTimestamp);
                }
            }
            /* Ivan */
        }
    }

    public void sendSuccessMessage() {
        Message message = new Message();
        message.what = 0;
        handler.sendMessage(message);
    }

    public void sendFailMessage() {
        Message message = new Message();
        message.what = 1;
        handler.sendMessage(message);
    }

    //For spinner control - manual
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
    //For spinner control - auto


    private boolean isValidWaist(String om) {

        double omValue = Double.parseDouble(om);

        if (omValue < bodyRecordConstant.GET_WAIST_LOWER_LIMIT || omValue >= bodyRecordConstant.GET_WAIST_UPPER_LIMIT) {
            return false;
        }
        return true;
    }

    private boolean isValidBmi(double bmi) {

        if (bmi < bodyRecordConstant.GET_BMI_LOWER_LIMIT || bmi >= bodyRecordConstant.GET_BMI_UPPER_LIMIT) {
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

}
