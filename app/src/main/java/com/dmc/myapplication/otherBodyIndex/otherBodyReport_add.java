package com.dmc.myapplication.otherBodyIndex;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.util.DateUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Po on 25/3/2016.
 */
public class otherBodyReport_add extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Context ctx;
    private EditText editTextDate, editTextTime, editTextOrHbA1cValue, editTextOrTotalCValue, editTextOrHdlCValue, editTextOrLdlCValue, editTextOrTriValue, editTextOrRemarkCValue, value_fastingBloodGlucose;
    UserLocalStore userLocalStore;
    User user;

    private double fastingBloodGlucose, hbAlc, totalC, hdlC, ldlC, tri = 0;
    private static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherbodyreport_add);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.or_add_date);
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText(yy + "-" + mm + "-" + dd);

        ImageView setOmDate = (ImageView) findViewById(R.id.setOr_add_date);
        setOmDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new otherBodyReportDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.or_add_time);
        Calendar time = Calendar.getInstance();
        int HH = time.get(Calendar.HOUR_OF_DAY);
        int MM = time.get(Calendar.MINUTE);

        editTextTime.setText(HH + ":" + MM + ":00");
        ImageView setTime = (ImageView) findViewById(R.id.setOr_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3 = new otherBodyReportTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });

        //handle the value field
        value_fastingBloodGlucose = (EditText) findViewById(R.id.value_fastingBloodGlucose);
        editTextOrHbA1cValue = (EditText) findViewById(R.id.or_add_hba1c_value);
        editTextOrTotalCValue = (EditText) findViewById(R.id.or_add_totalC_value);
        editTextOrHdlCValue = (EditText) findViewById(R.id.or_add_HDL_C_value);
        editTextOrLdlCValue = (EditText) findViewById(R.id.or_add_LDL_C_value);
        editTextOrTriValue = (EditText) findViewById(R.id.or_add_tri_value);

        editTextOrRemarkCValue = (EditText) findViewById(R.id.or_add_remark_value);

        setHandlerFunction();

        Button buttonOrAdd = (Button) findViewById(R.id.or_add_button);
        buttonOrAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                validOr();
                insertRecord();
            }
        });

    }

    private void insertRecord() {
        String fastingBloodGlucose = value_fastingBloodGlucose.getText().toString();
        String hbAlc = editTextOrHbA1cValue.getText().toString();
        String totalC = editTextOrTotalCValue.getText().toString();
        String hdlC = editTextOrHdlCValue.getText().toString();
        String ldlC = editTextOrLdlCValue.getText().toString();
        String tri = editTextOrTriValue.getText().toString();

        if (fastingBloodGlucose.isEmpty() && hbAlc.isEmpty() && totalC.isEmpty() && hdlC.isEmpty() && ldlC.isEmpty() && tri.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("請至少輸入一項")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
        } else {
            // save to server
            RealmFunction realmFunction = new RealmFunction(this);
            int userID = realmFunction.getUserInfo().getUser_id();
            DateUtil dateUtil = new DateUtil();
            Date insertDate = dateUtil.stringToDate(editTextDate.getText().toString());

            if (!fastingBloodGlucose.isEmpty()) {
                this.fastingBloodGlucose = Double.valueOf(fastingBloodGlucose);
            }
            if (!hbAlc.isEmpty()) {
                this.hbAlc = Double.valueOf(hbAlc);
            }
            if (!totalC.isEmpty()) {
                this.totalC = Double.valueOf(totalC);
            }
            if (!hdlC.isEmpty()) {
                this.hdlC = Double.valueOf(hdlC);
            }
            if (!ldlC.isEmpty()) {
                this.ldlC = Double.valueOf(ldlC);
            }
            if (!tri.isEmpty()) {
                this.tri = Double.valueOf(tri);
            }
            RetrofitFunction retrofitFunction = new RetrofitFunction(otherBodyReport_add.this, webService.serverURL);
            retrofitFunction.insertBloodTestRecord(userID, 2, insertDate, this.fastingBloodGlucose, this.hbAlc, this.totalC, this.hdlC, this.ldlC, this.tri);
            // save in local
            realmFunction.saveBloodTestRecord(userID, 2, insertDate, this.fastingBloodGlucose, this.hbAlc, this.totalC, this.hdlC, this.ldlC, this.tri);
        }
    }

    private void setHandlerFunction() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        builder.setMessage("資料已成功儲存")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        handleBackAction();
                                    }
                                })
                                .show();
                        break;
                    case 1:
                        builder.setMessage("請稍後再試")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .show();
                        break;
                }
            }
        };
    }

    public void sendSuccessMessage() {
        Message message = new Message();
        message.what = 0;
        handler.sendMessage(message);
    }

    public void sendFailMessage() {
        Message message = new Message();
        message.what = 1;
        handler.sendMessage(message);
    }

    private void validOr() {

        // Reset errors.
        editTextOrHbA1cValue.setError(null);
        editTextOrTotalCValue.setError(null);
        editTextOrHdlCValue.setError(null);
        editTextOrLdlCValue.setError(null);
        editTextOrTriValue.setError(null);
        editTextOrRemarkCValue.setError(null);

        // Store values of input.
        String orHbA1c = editTextOrHbA1cValue.getText().toString();
        String orTotalC = editTextOrTotalCValue.getText().toString();
        String orHdlC = editTextOrHdlCValue.getText().toString();
        String orLdlC = editTextOrLdlCValue.getText().toString();
        String orTri = editTextOrTriValue.getText().toString();
        String orRemark = editTextOrRemarkCValue.getText().toString();


        boolean cancel = false;
        View focusView = null;
        boolean isHba1c = false;
        boolean isTotalC = false;
        boolean isRemark = false;

        // Check for a valid input, if the user entered one.
        if (!TextUtils.isEmpty(orHbA1c)) {
            if (!isValidHbA1c(orHbA1c)) {
                editTextOrHbA1cValue.setError(getString(R.string.or_HbA1c_error));
                focusView = editTextOrHbA1cValue;
                cancel = true;
                isHba1c = false;
            }
            isHba1c = true;
        }


        if (!TextUtils.isEmpty(orTotalC)) {
            if (!isValidTotalC(orTotalC)) {
                editTextOrTotalCValue.setError(getString(R.string.or_totalC_error));
                focusView = editTextOrTotalCValue;
                cancel = true;
                isTotalC = false;
            }
        }

        if (!TextUtils.isEmpty(orHdlC)) {
            if (!isValidTotalC(orHdlC)) {
                editTextOrHdlCValue.setError(getString(R.string.or_HDL_C_error));
                focusView = editTextOrHdlCValue;
                cancel = true;
                isTotalC = false;
            }
        }

        if (!TextUtils.isEmpty(orLdlC)) {
            if (!isValidTotalC(orLdlC)) {
                editTextOrLdlCValue.setError(getString(R.string.or_LDL_C_error));
                focusView = editTextOrLdlCValue;
                cancel = true;
                isTotalC = false;
            }
        }

        if (!TextUtils.isEmpty(orTri)) {
            if (!isValidTotalC(orTri)) {
                editTextOrTriValue.setError(getString(R.string.or_tri_error));
                focusView = editTextOrTriValue;
                cancel = true;
                isTotalC = false;
            }
        }

        if (TextUtils.isEmpty(orTotalC) && TextUtils.isEmpty(orHdlC) && TextUtils.isEmpty(orLdlC) && TextUtils.isEmpty(orTri)) {
            isTotalC = false;
        } else if (TextUtils.isEmpty(orTotalC) || TextUtils.isEmpty(orHdlC) || TextUtils.isEmpty(orLdlC) || TextUtils.isEmpty(orTri)) {

            if (TextUtils.isEmpty(orTotalC)) {
                editTextOrTotalCValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrTotalCValue;
            }
            if (TextUtils.isEmpty(orHdlC)) {
                editTextOrHdlCValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrHdlCValue;
            }
            if (TextUtils.isEmpty(orLdlC)) {
                editTextOrLdlCValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrLdlCValue;
            }
            if (TextUtils.isEmpty(orTri)) {
                editTextOrTriValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrTriValue;
            }
            cancel = true;
            isTotalC = false;
        } else {
            isTotalC = true;
        }

        if (!TextUtils.isEmpty(orRemark)) {
            isRemark = true;
        }

        if (cancel && (focusView != null)) {
            focusView.requestFocus();
        } else {

            String date = editTextDate.getText().toString();
            String time = editTextTime.getText().toString();

            if (isHba1c) {
                //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.SAVE_BODY_RECORD,bodyRecordConstant.GET_HBA1C_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                //String user_id = Integer.toString(user.userid);
                //String record_type = bodyRecordConstant.GET_HBA1C_TYPE_CODE;
                //addRecord.execute(user_id, record_type, date, time, orHbA1c );

                bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                BodyRecord a = new BodyRecord();
                BodyRecord.bodyRecord bodyrecord = bodyRecord.insert(a.new bodyRecord(0, Integer.parseInt(bodyRecordConstant.GET_HBA1C_TYPE_CODE), (user.userid), date, time, 0, 0, 0, 0, 0, 0, 0, Double.parseDouble(orHbA1c), 0, 0, 0, 0, 0, 0, 0, ""));
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                this.startActivity(intent);
                this.finish();
            }

            if (isTotalC) {
                //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.SAVE_BODY_RECORD,bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                //String user_id = Integer.toString(user.userid);
                //String record_type = bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE;
                //addRecord.execute(user_id, record_type, date, time, orTotalC, orHdlC, orLdlC, orTri );
                bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                BodyRecord a = new BodyRecord();
                BodyRecord.bodyRecord bodyrecord = bodyRecord.insert(a.new bodyRecord(0, Integer.parseInt(bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE), (user.userid), date, time, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Double.parseDouble(orTotalC), Double.parseDouble(orLdlC), Double.parseDouble(orHdlC), Double.parseDouble(orTri), ""));
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                this.startActivity(intent);
                this.finish();
            }

            if (isRemark) {
                //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.SAVE_BODY_RECORD,bodyRecordConstant.GET_REMARK_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                //String user_id = Integer.toString(user.userid);
                //String record_type = bodyRecordConstant.GET_REMARK_TYPE_CODE;
                //addRecord.execute(user_id, record_type, date, time, orRemark);

                BodyRecord a = new BodyRecord();
                BodyRecord.bodyRecord bodyrecord = bodyRecord.insert(a.new bodyRecord(0, Integer.parseInt(bodyRecordConstant.GET_REMARK_TYPE_CODE), (user.userid), date, time, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, orRemark));
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                this.startActivity(intent);
                this.finish();
            }


        }
    }

    //For spinner control - manual
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
    //For spinner control - auto


    private boolean isValidHbA1c(String or) {
        double orValue = Double.parseDouble(or);

        if (orValue < bodyRecordConstant.GET_HBA1C_LOWER_LIMIT || orValue >= bodyRecordConstant.GET_HBA1C_UPPER_LIMIT) {
            return false;
        }
        return true;
    }


    private boolean isValidTotalC(String or) {
        double orValue = Double.parseDouble(or);

        if (orValue < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT || orValue >= bodyRecordConstant.GET_TOTALC_UPPER_LIMIT) {
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }
}
