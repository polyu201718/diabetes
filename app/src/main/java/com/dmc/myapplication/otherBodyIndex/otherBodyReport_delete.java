package com.dmc.myapplication.otherBodyIndex;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.systemConstant;

/**
 * Created by Po on 26/3/2016.
 */
public class otherBodyReport_delete extends AppCompatActivity {

    private Context ctx;
    private String recordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherbodyreport_del);
        ctx = getApplicationContext();


        final Intent intent = getIntent();
        recordId = intent.getStringExtra("recordId");
        final String recordType = intent.getStringExtra("recordType");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date
        EditText editTextDate = (EditText) findViewById(R.id.or_del_date);
        editTextDate.setText(getDate);
        editTextDate.setEnabled(false);


        //handle the time
        EditText editTextTime = (EditText) findViewById(R.id.or_del_time);
        editTextTime.setText(getTime);
        editTextTime.setEnabled(false);


        //handle the value field
        final EditText editTextOrHba1cValue = (EditText) findViewById(R.id.or_del_hba1c_value);
        final EditText editTextOrTotalCValue = (EditText) findViewById(R.id.or_del_totalC_value);
        final EditText editTextOrHdlCValue = (EditText) findViewById(R.id.or_del_HDL_C_value);
        final EditText editTextOrLdlCValue = (EditText) findViewById(R.id.or_del_LDL_C_value);
        final EditText editTextOrTriValue = (EditText) findViewById(R.id.or_del_tri_value);
        final EditText editTextOrRemarkValue = (EditText) findViewById(R.id.or_del_remark_value);



        if (recordType.equals("3")){
            final double getHba1c = Double.parseDouble(intent.getStringExtra("HbA1cValue"));
            editTextOrHba1cValue.setText(Double.toString(getHba1c));

        }else if (recordType.equals("5")){
            final double getTotalC = Double.parseDouble(intent.getStringExtra("totalCValue"));
            final double getHdlC = Double.parseDouble(intent.getStringExtra("hdlCValue"));
            final double getLdlC = Double.parseDouble(intent.getStringExtra("ldlCValue"));
            final double getTri = Double.parseDouble(intent.getStringExtra("orTriValue"));

            editTextOrTotalCValue.setText(Double.toString(getTotalC));
            editTextOrHdlCValue.setText(Double.toString(getHdlC));
            editTextOrLdlCValue.setText(Double.toString(getLdlC));
            editTextOrTriValue.setText(Double.toString(getTri));

        }else if(recordType.equals("6")){
            final String getRemark = intent.getStringExtra("remark");
            editTextOrRemarkValue.setText(getRemark);
        }
        editTextOrHba1cValue.setEnabled(false);
        editTextOrTotalCValue.setEnabled(false);
        editTextOrHdlCValue.setEnabled(false);
        editTextOrLdlCValue.setEnabled(false);
        editTextOrTriValue.setEnabled(false);
        editTextOrRemarkValue.setEnabled(false);


        Button buttonOmDel = (Button) findViewById(R.id.or_del_button);
        buttonOmDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new deleteDialogFragment(recordId);
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        Button buttonOmEdit = (Button) findViewById(R.id.or_edit_button);
        buttonOmEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recordType.equals("3")){
                    getOrHba1cEditRecord(recordId, recordType, getDate, getTime, editTextOrHba1cValue.getText().toString());
                }else if (recordType.equals("5")){
                    getOrTotalCEditRecord(recordId, recordType, getDate, getTime,  editTextOrTotalCValue.getText().toString(),
                            editTextOrHdlCValue.getText().toString(), editTextOrLdlCValue.getText().toString(), editTextOrTriValue.getText().toString());
                }else if(recordType.equals("6")){
                    getOrRemarkEditRecord(recordId, recordType, getDate, getTime, editTextOrRemarkValue.getText().toString());
                }

            }
        });

    }

    public void getOrHba1cEditRecord(String recordId, String recordType,String date, String time, String HbA1cValue) {

        Intent intent = new Intent();
        intent.setClass(this, otherBodyReport_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("recordType", recordType);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("HbA1cValue", HbA1cValue);
        this.startActivity(intent);
        this.finish();
    }

    public void getOrTotalCEditRecord(String recordId, String recordType,String date, String time,String totalCValue, String hdlCValue, String ldlCValue, String orTriValue ) {

        Intent intent = new Intent();
        intent.setClass(this, otherBodyReport_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("recordType", recordType);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("totalCValue", totalCValue);
        intent.putExtra("hdlCValue", hdlCValue);
        intent.putExtra("ldlCValue", ldlCValue);
        intent.putExtra("orTriValue", orTriValue);
        this.startActivity(intent);
        this.finish();
    }

    public void getOrRemarkEditRecord(String recordId, String recordType,String date, String time, String remark) {

        Intent intent = new Intent();
        intent.setClass(this, otherBodyReport_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("recordType", recordType);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("remark", remark);
        this.startActivity(intent);
        this.finish();
    }

    private class deleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private String recordId;

        public deleteDialogFragment(String recordId) {
            this.recordId = recordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView = new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0, 30, 0, 30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //bodyAsyncTask delRecord = new bodyAsyncTask(getActivity(), bodyRecordConstant.DELETE_BODY_RECORD, bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                    //String record_type = bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE;
                                    //delRecord.execute(recordId, record_type);
                                    bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                                    bodyRecord.delete(Integer.parseInt(recordId));
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), MainActivity.class);
                                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }

        @Override
        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}