package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bp.bpMain;
import com.dmc.myapplication.bp.bp_add;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;

/**
 * Created by Po on 25/3/2016.
 */
public class otherBody_Main extends Fragment {
    Context ctx;
    private static int flag = 0;

    Button btnBodyMeasurement, btnBodyReport, btn_bloodPressure;

    private static Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.otherbody_container, container, false);
        ctx = getActivity().getApplicationContext();
        setHasOptionsMenu(true);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.otherBody_frame, new otherBodyMeasurementMain()).commit();

        btnBodyMeasurement = (Button) v.findViewById(R.id.title_activity_om_switch);
        btnBodyReport = (Button) v.findViewById(R.id.title_activity_or_switch);
        btn_bloodPressure = (Button) v.findViewById(R.id.btn_bloodPressure);

        btnBodyMeasurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setTitle("身體指數");

                btnBodyMeasurement.setBackgroundColor(Color.parseColor("#FF7a00")); // active
                btnBodyReport.setBackgroundColor(Color.parseColor("#26C6DA"));
                btn_bloodPressure.setBackgroundColor(Color.parseColor("#26C6DA"));

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.otherBody_frame, new otherBodyMeasurementMain()).commit();
                flag = 0;
            }
        });

        btnBodyReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setTitle("驗血紀錄");

                btnBodyMeasurement.setBackgroundColor(Color.parseColor("#26C6DA"));
                btnBodyReport.setBackgroundColor(Color.parseColor("#FF7a00")); // active
                btn_bloodPressure.setBackgroundColor(Color.parseColor("#26C6DA"));

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.otherBody_frame, new otherBodyReportMain()).commit();
                flag = 1;
            }
        });

        btn_bloodPressure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setTitle(R.string.title_activity_bp);

                btnBodyMeasurement.setBackgroundColor(Color.parseColor("#26C6DA"));
                btnBodyReport.setBackgroundColor(Color.parseColor("#26C6DA"));
                btn_bloodPressure.setBackgroundColor(Color.parseColor("#FF7a00")); // active

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.otherBody_frame, new bpMain()).commit();
                flag = 2;
            }
        });

        return v;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getActivity().getMenuInflater();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.om_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent();
                if (flag == 0) {
                    intent.setClass(this.getActivity(), otherBodyMeasure_add.class);
                } else if (flag == 1) {
                    intent.setClass(this.getActivity(), otherBodyReport_add.class);
                } else if (flag == 2) {
                    intent.setClass(this.getActivity(), bp_add.class);
                }
                getActivity().startActivity(intent);
                getActivity().finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        flag = 0;
        setHasOptionsMenu(true);
    }
}
