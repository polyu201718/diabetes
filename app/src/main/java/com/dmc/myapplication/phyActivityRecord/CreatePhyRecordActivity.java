package com.dmc.myapplication.phyActivityRecord;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivity;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecord;
import com.dmc.myapplication.phyActivityRecord.helper.PhyActivityStrengthEnum;
import com.dmc.myapplication.phyActivityRecord.helper.PhyDBHelper;
import com.dmc.myapplication.phyActivityRecord.helper.PhyStrengthInfoDialog;
import com.dmc.myapplication.phyActivityRecord.helper.UserHelper;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.DecimalMin;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.commons.lang.time.DateUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreatePhyRecordActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    Toolbar toolbar;
    @NotEmpty
    TextView tvDate;
    @NotEmpty
    TextView tvActivity;
    @NotEmpty
    TextView tvStrength;

    @DecimalMax(value = 1440, message = "不能大於一天")
    @DecimalMin(value = 1, message = "不能小於一分鐘")
    TextView tvDuration;
    Button btnSubmit;
    ImageButton ibStrengthInfo;

    PhyActivity selectedPhyItem;
    final Calendar cDate = Calendar.getInstance();

    Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // basic init
        //
        setTitle("新增體能活動");
        setContentView(R.layout.activity_create_phy_item);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get intent message
        //
        Intent intent = getIntent();
        final Date date = (Date) intent.getSerializableExtra(MainActivity.EXTRA_DATE);

        // Form
        //
        tvDate = findViewById(R.id.tv_phy_create_date);
        tvActivity = findViewById(R.id.tv_phy_create_activity);
        tvStrength = findViewById(R.id.tv_phy_create_strength);
        tvDuration = findViewById(R.id.tv_phy_create_duration);
        btnSubmit = findViewById(R.id.btn_phy_create_submit);
        ibStrengthInfo = findViewById(R.id.ib_phy_create_strength_info);

        // Init date picker
        //
        // Set date
        cDate.setTime(date);

        // Set default date on textEdit
        this.setDateText(cDate.get(Calendar.YEAR), cDate.get(Calendar.MONTH), cDate.get(Calendar.DAY_OF_MONTH));

        // Form Component listeners
        //
        // Strength Info
        ibStrengthInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhyStrengthInfoDialog.getDialog(CreatePhyRecordActivity.this);
            }
        });

        // TextEdit date on click listener
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open date picker
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        CreatePhyRecordActivity.this,
                        cDate.get(Calendar.YEAR),
                        cDate.get(Calendar.MONTH),
                        cDate.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        // TextEdit activity on click listener
        tvActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreatePhyRecordActivity.this.goToPhyItemList();
            }
        });

        // TextEdit strength on click listener
        tvStrength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreatePhyRecordActivity.this.goToPhyItemList();
            }
        });

        // Submit Data on click listener
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go to validator, validator will emit result to validator listener
                validator.validate();
            }
        });

        // Init validator
        validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                // get selected Date from date picker
                UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
                User user = userLocalStore.getLoggedInUser();

                PhyDBHelper.insertRecord(
                        new PhyActivityRecord(
                                null,
                                user.userid,
                                selectedPhyItem.getActivity(),
                                DateUtils.truncate(cDate.getTime(), Calendar.DATE),
                                selectedPhyItem.getStrength(),
                                Integer.parseInt(tvDuration.getText().toString()), selectedPhyItem.getCategory()
                        )
                );

                Log.d("InsertPhyActivity", DateUtils.truncate(cDate.getTime(), Calendar.DATE).toString());
                Toast.makeText(getBaseContext(), "新增了1個活動記錄", Toast.LENGTH_SHORT).show();
                Intent returnIntent = new Intent(CreatePhyRecordActivity.this, PhyActivityDayRecord.class);
                returnIntent.putExtra(MainActivity.EXTRA_DATE, date);
                setResult(Activity.RESULT_OK, returnIntent);
                CreatePhyRecordActivity.this.startActivity(returnIntent);
                finish();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getBaseContext());

                    // Display error messages ;)
                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });

    }

    // Go To Phy Item List to choose activity
    private void goToPhyItemList(){
        Intent intent = new Intent(this, PhyItemListActivity.class);
        startActivityForResult(intent, 1);
    }

    // Phy Item List Activity callback
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Serializable result = data.getSerializableExtra("result");
                try {
                    PhyActivity phyItem = (PhyActivity) result;
                    selectedPhyItem = phyItem;

                    tvActivity.setText(phyItem.getActivity());
                    tvActivity.setError(null);
                    tvStrength.setText(PhyActivityStrengthEnum.getName(phyItem.getStrength()));

                    tvStrength.setError(null);
                } catch (Exception e) {
                    Log.e("CreatePhyRecordActivity", e.toString());
                }
            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//            }
        }
    }

    // Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        // set picker selected date
        cDate.set(year, monthOfYear, dayOfMonth);
        // set text
        setDateText(year, monthOfYear, dayOfMonth);
    }

    // Display date on date textEdit
    public void setDateText(int year, int monthOfYear, int dayOfMonth) {
        String date = year + "年" + (monthOfYear + 1) + "月" + dayOfMonth + "日";
        tvDate.setText(date);
    }

    // https://stackoverflow.com/questions/19184154/dynamically-set-parent-activity/34775181#34775181
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
