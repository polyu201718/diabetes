package com.dmc.myapplication.phyActivityRecord;


import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.applikeysolutions.cosmocalendar.model.Day;
import com.applikeysolutions.cosmocalendar.selection.OnDaySelectedListener;
import com.applikeysolutions.cosmocalendar.settings.lists.connected_days.ConnectedDays;
import com.applikeysolutions.cosmocalendar.view.CalendarView;
import com.dmc.myapplication.R;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecord;
import com.dmc.myapplication.phyActivityRecord.helper.PhyDBHelper;

import org.apache.commons.lang.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_DATE = ".PHY_SELECTED_DATE";

    private Date selectedDate;
    private CalendarView cv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("體能活動記錄");

        // set locale
        Locale locale = Locale.TAIWAN; // language
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        // basic init
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_phy_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // outer layout
        final CoordinatorLayout clPhy = findViewById(R.id.cl_phy);

        // fab button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToCreatePhyActivity(view, selectedDate);
            }
        });

        //this.insertData();

        // init calendar view
        cv = findViewById(R.id.calendar_view);
        cv.setFirstDayOfWeek(1);
        cv.setWeekendDayTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        cv.setSelectedDayBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
        cv.setConnectedDayIconRes(R.drawable.ic_dot_orange);
        cv.setConnectedDaySelectedIconRes(R.drawable.ic_dot_orange);

        cv.setCurrentDayIconRes(R.drawable.ic_dot_black);
        cv.setCurrentDaySelectedIconRes(R.drawable.ic_dot_black);
        addRecordExistsDaysToCV(cv);

        //cv.setCurrentDayIconRes(R.drawable.ic_triangle_white);
        CalendarSingleSelectionManager cssm = new CalendarSingleSelectionManager(cv);

        cssm.setOnDayClickListener(new CalendarSingleSelectionManager.OnDayClickListener() {
            private Snackbar sb;
            @Override
            public void onClick(Date date) {
                selectedDate = date;
                Date d = PhyDBHelper.isRecordExists(date);
                if(d != null) {
                    Intent intent = new Intent(MainActivity.this, PhyActivityDayRecord.class);
                    intent.putExtra(EXTRA_DATE, d);
                    MainActivity.this.startActivity(intent);
                } else {

                    // android 4.0 bug, if multiple sandbar, it will disappear
                    if(sb != null){
                        sb.dismiss();
                    }

                    sb = Snackbar.make(clPhy, new SimpleDateFormat("yyyy年MM月dd日", Locale.TAIWAN).format(date) + " 無記錄", Snackbar.LENGTH_LONG);
                    sb.show();
                }
            }
        });

        cv.setSelectionManager(cssm);
        //cv.setCalendarOrientation(0);

    }


    @Override
    public void onResume() {
        super.onResume();
        // TO-DO add dot to cv
        addRecordExistsDaysToCV(cv);
    }

    private void addRecordExistsDaysToCV(CalendarView cv) {

        HashSet<Long> recordExistsDays = new HashSet<>();
        HashSet<Long> holidayRecordExistsDays = new HashSet<>();
        // reset
        cv.getSettingsManager().getConnectedDaysManager().setConnectedDaysList(new LinkedList<ConnectedDays>());

        Calendar d = Calendar.getInstance();
        for (PhyActivityRecord r : PhyDBHelper.getRecordExistsDays()) {

            d.setTime(r.getDate());

            if (d.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                holidayRecordExistsDays.add(r.getDate().getTime());
            }else {
                recordExistsDays.add(r.getDate().getTime());
            }


        }

        cv.addConnectedDays(new ConnectedDays(recordExistsDays, getResources().getColor(R.color.default_day_text_color), getResources().getColor(R.color.default_selected_day_text_color)));
        cv.addConnectedDays(new ConnectedDays(holidayRecordExistsDays, getResources().getColor(R.color.default_connected_day_text_color), getResources().getColor(R.color.default_selected_day_text_color)));
        cv.setVisibility(View.GONE);
        cv.setVisibility(View.VISIBLE);
    }

    public void goToCreatePhyActivity(View view, Date defaultDate) {
        // no selected date
        if(defaultDate == null){
            Date today = DateUtils.round(new Date(), Calendar.DATE);
            //Log.d("phy date", today.toString());
            defaultDate = today;
        }

        //Intent intent = new Intent(this, CreatePhyRecordActivity.class);
        Intent intent = new Intent(this, CreatePhyRecordActivity.class);
        intent.putExtra(EXTRA_DATE, defaultDate);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_phy_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_weekly_chart:
                Intent intent = new Intent(this, ChartPhyActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}

class CalendarSingleSelectionManager extends com.applikeysolutions.cosmocalendar.selection.SingleSelectionManager {
    private OnDayClickListener onDayClickListener;

    public interface OnDayClickListener {
        void onClick(Date date);
    }

    public CalendarSingleSelectionManager(OnDaySelectedListener onDaySelectedListener) {
        super(onDaySelectedListener);
    }

    @Override
    public void toggleDay(@NonNull Day day) {
        //Log.d("phy", "toggleDay");

        if (onDayClickListener != null) {
            onDayClickListener.onClick(day.getCalendar().getTime());
        }
        super.toggleDay(day);
    }

    public void setOnDayClickListener(OnDayClickListener onDayClickListener){
        this.onDayClickListener = onDayClickListener;
    }
}