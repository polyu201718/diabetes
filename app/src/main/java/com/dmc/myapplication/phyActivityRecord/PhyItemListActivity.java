package com.dmc.myapplication.phyActivityRecord;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivity;
import com.dmc.myapplication.phyActivityRecord.helper.PhyActivityStrengthEnum;
import com.dmc.myapplication.phyActivityRecord.helper.PhyDBHelper;
import com.dmc.myapplication.phyActivityRecord.helper.PhyStrengthInfoDialog;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.truizlop.sectionedrecyclerview.SimpleSectionedAdapter;

import org.apache.commons.lang.ArrayUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;

public class PhyItemListActivity extends AppCompatActivity {

    PhyActivityListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy_item_list);
        setTitle("體能活動種類");

        RecyclerView mRecyclerView = findViewById(R.id.phy_item_list);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        mAdapter = new PhyActivityListAdapter(getAllPhyActivities());

        mAdapter.setOnItemClickListener(new PhyActivityListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, PhyActivity obj) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", obj);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

            @Override
            public void onItemLongClick(View view, final PhyActivity obj) {
                if (obj.getId() == null) {
                    return;
                }

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(PhyItemListActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(PhyItemListActivity.this);
                }

                builder.setTitle("刪除活動")
                        .setMessage("是否確定刪除活動: " + obj.getActivity() + "?")
                        .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                PhyDBHelper.deleteCustomActivity(obj.getId());
                                mAdapter.setDataset(getAllPhyActivities());
                                mAdapter.notifyDataSetChanged();
                            }
                        })
                        .setPositiveButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }

        });

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVerticalScrollBarEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_phy_list, menu);

        // SearchView
        final SearchView searchView = (SearchView) menu.findItem(R.id.btn_phy_list_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.setDataset(DefaultPhyActivityList.getFilterList(getAllPhyActivities(), newText));
                mAdapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();

        switch (item_id) {
            case R.id.btn_phy_list_add_new_custom_activity:
                showCreateCustomPhyActivityDialog();
            case R.id.btn_phy_list_search:

                break;
            default:
                return false;
        }
        return true;
    }

    private LinkedHashMap<String, PhyActivity[]> getAllPhyActivities() {
        List<PhyActivity> phyCustomActivities = PhyDBHelper.getAllCustomActivities();
        //Log.d("getAllPhyActivities", phyCustomActivities.size() + "");
        PhyActivity[] customPhyActivities = phyCustomActivities.toArray(new PhyActivity[phyCustomActivities.size()]);
        return DefaultPhyActivityList.getList(customPhyActivities);
    }


    //
    // CustomActivityDialog
    @NotEmpty(message = "必填")
    EditText tvCustomActivity;
    @NotEmpty(message = "必填")
    AutoCompleteTextView tvCustomStrength;

    private void showCreateCustomPhyActivityDialog() {
        View mView = LayoutInflater.from(this).inflate(R.layout.dialog_phy_create_custom_activity, null);


        // ui component
        tvCustomActivity = mView.findViewById(R.id.tv_phy_create_custom_activity);
        tvCustomStrength = mView.findViewById(R.id.tv_phy_create_custom_strength);
        final ImageButton ibStrengthInfo = mView.findViewById(R.id.ib_phy_create_custom_strength_info);

        // info button
        ibStrengthInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhyStrengthInfoDialog.getDialog(PhyItemListActivity.this);
            }
        });

        // Str options
        final String[] options = {
                PhyActivityStrengthEnum.LOW.getName(), PhyActivityStrengthEnum.MEDIUM.getName(), PhyActivityStrengthEnum.HIGH.getName(),
        };
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line,
                options);
        tvCustomStrength.setAdapter(adapter);
        tvCustomStrength.setKeyListener(null);

        // show dropdown
        tvCustomStrength.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                tvCustomStrength.showDropDown();
                return false;
            }
        });

        // hide keyboard
        tvCustomStrength.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    tvCustomStrength.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(tvCustomStrength.getWindowToken(), 0);
                        }
                    }, 1);

                }
            }
        });

        // Build popup
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);
        alertDialogBuilderUserInput
                .setTitle("新增自訂活動")
                .setPositiveButton("完成", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {

                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();

        // when submit
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // Validate
                        Validator validator = new Validator(PhyItemListActivity.this);
                        validator.setValidationListener(new Validator.ValidationListener() {
                            @Override
                            public void onValidationSucceeded() {
                                //Dismiss once everything is OK.
                                PhyDBHelper.insertCustomActivity(new PhyActivity(null, tvCustomActivity.getText().toString(), PhyActivityStrengthEnum.getNameIndex(tvCustomStrength.getText().toString()), "自訂"));

                                mAdapter.setDataset(getAllPhyActivities());
                                mAdapter.notifyDataSetChanged();
                                alertDialog.dismiss();
                            }

                            @Override
                            public void onValidationFailed(List<ValidationError> errors) {
                                for (ValidationError error : errors) {
                                    View view = error.getView();
                                    String message = error.getCollatedErrorMessage(getBaseContext());

                                    // Display error messages ;)
                                    if (view instanceof EditText) {
                                        ((EditText) view).setError(message);
                                    }
                                }
                            }
                        });
                        validator.validate();
                    }
                });
            }
        });

        // show popup
        alertDialog.show();
    }
}

//
// Physical Item List Adapter
//
class PhyActivityListAdapter extends SimpleSectionedAdapter<PhyActivityListAdapter.ItemViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(View view, PhyActivity obj);

        void onItemLongClick(View view, PhyActivity obj);
    }


    @Override
    protected String getSectionHeaderTitle(int section) {
        return sectionTitles[section];
    }

    private PhyActivity[][] mDataset = new PhyActivity[][]{};
    private String[] sectionTitles;
    private OnItemClickListener onItemClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PhyActivityListAdapter(LinkedHashMap<String, PhyActivity[]> defaultDataset) {
        setDataset(defaultDataset);
    }

    @Override
    protected com.truizlop.sectionedrecyclerview.HeaderViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(getLayoutResource(), parent, false);
        HeaderViewHolder holder = new HeaderViewHolder(view, getTitleTextID());
        return holder;
    }

    @Override
    protected int getSectionCount() {
        return sectionTitles.length;
    }

    @Override
    protected int getItemCountForSection(int section) {
        return mDataset[section].length;
    }

    @Override
    protected ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vh_phy_item_list_item, parent, false);

        ItemViewHolder vh = new ItemViewHolder(v);
        return vh;
    }

    @Override
    protected void onBindItemViewHolder(ItemViewHolder holder, int section, int position) {
        holder.render(mDataset[section][position], onItemClickListener);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setDataset(LinkedHashMap<String, PhyActivity[]> defaultDataset) {
        this.sectionTitles = defaultDataset.keySet().toArray(new String[defaultDataset.size()]);
        mDataset = new PhyActivity[][]{};
        for (String s : this.sectionTitles) {
            mDataset = (PhyActivity[][]) ArrayUtils.add(mDataset, defaultDataset.get(s));
        }
    }

    //
    // View Holder
    //
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvActivity;
        public TextView tvStrength;
        public RelativeLayout rlStrengthColor;

        public ItemViewHolder(View v) {
            super(v);
            tvActivity = v.findViewById(R.id.tv_vh_phy_day_record_activity);
            tvStrength = v.findViewById(R.id.tv_vh_phy_day_record_strength);
            rlStrengthColor = v.findViewById(R.id.rl_vh_phy_day_record_strength_color);
        }

        public void render(final PhyActivity data, final OnItemClickListener onItemClickListener) {
            tvActivity.setText(data.getActivity());

            try {
                tvStrength.setText(PhyActivityStrengthEnum.getName(data.getStrength()));
                rlStrengthColor.setBackgroundColor(PhyActivityStrengthEnum.getColor(data.getStrength()));
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(view, data);
                        }
                    }
                });
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemLongClick(view, data);
                        }
                        return false;
                    }
                });
            } catch (Exception e) {
                Log.e("PhyActivityListActivity", e.toString());
            }
        }
    }

    public static class HeaderViewHolder extends com.truizlop.sectionedrecyclerview.HeaderViewHolder {

        public HeaderViewHolder(View itemView, int titleID) {
            super(itemView, titleID);
        }

        @Override
        public void render(String title) {
            super.render(title);
            titleText.setTextColor(Color.parseColor("#555555"));
            titleText.setTextSize(COMPLEX_UNIT_DIP, 20);
        }
    }
}

// Phy Activity List
class DefaultPhyActivityList {

    public static LinkedHashMap<String, PhyActivity[]> getFilterList(LinkedHashMap<String, PhyActivity[]> map, String filterString) {
        LinkedHashMap<String, PhyActivity[]> newMap = new LinkedHashMap<>();
        for (String key : map.keySet()) {
            ArrayList<PhyActivity> newList = new ArrayList<>();
            for(PhyActivity pa : map.get(key)){
                boolean isMatch = true;
                for (char ch: filterString.toCharArray()) {
                        isMatch = isMatch && pa.getActivity().contains(Character.toString(ch));
                }

                if(isMatch){
                    newList.add(pa);
                }
            }

            if (newList.size() > 0) {
                newMap.put(key, newList.toArray(new PhyActivity[newList.size()]));
            }
        }

        return newMap;
    }

    public static LinkedHashMap<String, PhyActivity[]> getList(PhyActivity[] customList) {

        LinkedHashMap<String, PhyActivity[]> pa = new LinkedHashMap<>();

        if (customList != null && customList.length > 0) {
            pa.put("自訂", customList);
        }

        pa.put("步行", new PhyActivity[]{
                new PhyActivity(null, "爬山", PhyActivityStrengthEnum.HIGH.ordinal(), "步行"),
                new PhyActivity(null, "行山", PhyActivityStrengthEnum.MEDIUM.ordinal(), "步行"),
                new PhyActivity(null, "行樓梯（上）", PhyActivityStrengthEnum.HIGH.ordinal(), "步行"),
                new PhyActivity(null, "行樓梯（落）", PhyActivityStrengthEnum.MEDIUM.ordinal(), "步行"),
                new PhyActivity(null, "一般步行 ", PhyActivityStrengthEnum.MEDIUM.ordinal(), "步行"),
                new PhyActivity(null, "急步行", PhyActivityStrengthEnum.HIGH.ordinal(), "步行"),
                new PhyActivity(null, "跑步 ", PhyActivityStrengthEnum.HIGH.ordinal(), "步行"),
        });

        pa.put("家務", new PhyActivity[]{
                new PhyActivity(null, "熨衣服、掃地、洗碗、煮食", PhyActivityStrengthEnum.LOW.ordinal(), "家務"),
                new PhyActivity(null, "抹窗、拖地／抹地", PhyActivityStrengthEnum.MEDIUM.ordinal(), "家務"),
                new PhyActivity(null, "清潔洗手間／廚房（馬桶、洗手盆）", PhyActivityStrengthEnum.MEDIUM.ordinal(), "家務"),
        });

        pa.put("單車", new PhyActivity[]{
                new PhyActivity(null, "休閒踏單車", PhyActivityStrengthEnum.MEDIUM.ordinal(), "單車"),
                new PhyActivity(null, "單車訓練/比賽，山地單車", PhyActivityStrengthEnum.HIGH.ordinal(), "單車")
        });

        pa.put("負重健身運動", new PhyActivity[]{
                new PhyActivity(null, "大字跳", PhyActivityStrengthEnum.HIGH.ordinal(), "負重健身運動"),
                new PhyActivity(null, "掌上壓", PhyActivityStrengthEnum.HIGH.ordinal(), "負重健身運動"),
                new PhyActivity(null, "仰臥起坐", PhyActivityStrengthEnum.HIGH.ordinal(), "負重健身運動"),
                new PhyActivity(null, "弓步蹲", PhyActivityStrengthEnum.MEDIUM.ordinal(), "負重健身運動"),
                new PhyActivity(null, "捲腹", PhyActivityStrengthEnum.LOW.ordinal(), "負重健身運動"),
                new PhyActivity(null, "深蹲", PhyActivityStrengthEnum.MEDIUM.ordinal(), "負重健身運動"),
        });

        pa.put("球類運動", new PhyActivity[]{
                new PhyActivity(null, "籃球", PhyActivityStrengthEnum.HIGH.ordinal(), "球類運動"),
                new PhyActivity(null, "排球", PhyActivityStrengthEnum.MEDIUM.ordinal(), "球類運動"),
                new PhyActivity(null, "排球（訓綀/比賽)", PhyActivityStrengthEnum.HIGH.ordinal(), "球類運動"),
                new PhyActivity(null, "羽毛球（訓練／比賽）", PhyActivityStrengthEnum.HIGH.ordinal(), "球類運動"),
                new PhyActivity(null, "羽毛球活動", PhyActivityStrengthEnum.MEDIUM.ordinal(), "球類運動"),
                new PhyActivity(null, "壁球", PhyActivityStrengthEnum.HIGH.ordinal(), "球類運動"),
                new PhyActivity(null, "足球", PhyActivityStrengthEnum.HIGH.ordinal(), "球類運動"),
                new PhyActivity(null, "保齡球", PhyActivityStrengthEnum.MEDIUM.ordinal(), "球類運動"),
                new PhyActivity(null, "高爾夫球", PhyActivityStrengthEnum.MEDIUM.ordinal(), "球類運動"),
                new PhyActivity(null, "網球（擊球，非對打）", PhyActivityStrengthEnum.MEDIUM.ordinal(), "球類運動"),
                new PhyActivity(null, "網球（單打，雙打）", PhyActivityStrengthEnum.HIGH.ordinal(), "球類運動"),
                new PhyActivity(null, "乒乓球 ", PhyActivityStrengthEnum.MEDIUM.ordinal(), "球類運動"),
        });

        pa.put("水上運動", new PhyActivity[]{
                new PhyActivity(null, "游泳（自游式、蛙式、蝶式）", PhyActivityStrengthEnum.HIGH.ordinal(), "水上運動"),
                new PhyActivity(null, "游泳（背泳 – 休閒）", PhyActivityStrengthEnum.MEDIUM.ordinal(), "水上運動"),
                new PhyActivity(null, "游泳（背泳 – 訓練／比賽）", PhyActivityStrengthEnum.HIGH.ordinal(), "水上運動"),
                new PhyActivity(null, "划艇", PhyActivityStrengthEnum.HIGH.ordinal(), "水上運動"),
                new PhyActivity(null, "獨木舟（休閒）", PhyActivityStrengthEnum.MEDIUM.ordinal(), "水上運動"),
                new PhyActivity(null, "獨木舟（訓練／比賽）", PhyActivityStrengthEnum.HIGH.ordinal(), "水上運動"),
        });

        pa.put("瑜伽", new PhyActivity[]{
                new PhyActivity(null, "哈達瑜伽、動態瑜伽、淨脈呼吸法瑜伽", PhyActivityStrengthEnum.LOW.ordinal(), "瑜伽"),
                new PhyActivity(null, "塑身經絡瑜珈", PhyActivityStrengthEnum.MEDIUM.ordinal(), "瑜伽"),
                new PhyActivity(null, "高溫瑜珈", PhyActivityStrengthEnum.MEDIUM.ordinal(), "瑜伽"),
        });

        pa.put("跳舞", new PhyActivity[]{
                new PhyActivity(null, "芭蕾舞", PhyActivityStrengthEnum.MEDIUM.ordinal(), "跳舞"),
                new PhyActivity(null, "現代舞", PhyActivityStrengthEnum.MEDIUM.ordinal(), "跳舞"),
                new PhyActivity(null, "爵士舞", PhyActivityStrengthEnum.MEDIUM.ordinal(), "跳舞"),
                new PhyActivity(null, "一般舞蹈（例如：Disco舞、民俗舞、愛爾蘭踢踏舞、波爾卡）", PhyActivityStrengthEnum.HIGH.ordinal(), "跳舞"),
                new PhyActivity(null, "舞廳舞蹈（例如：華爾茲舞、狐步舞、慢跳舞、森巴）", PhyActivityStrengthEnum.LOW.ordinal(), "跳舞"),
        });

        pa.put("其他", new PhyActivity[]{
                new PhyActivity(null, "跳繩", PhyActivityStrengthEnum.HIGH.ordinal(), "其他"),
                new PhyActivity(null, "武術（中等強度）", PhyActivityStrengthEnum.MEDIUM.ordinal(), "其他"),
                new PhyActivity(null, "武術（劇烈）", PhyActivityStrengthEnum.HIGH.ordinal(), "其他"),
                new PhyActivity(null, "武拳擊 (對打)", PhyActivityStrengthEnum.HIGH.ordinal(), "其他"),
                new PhyActivity(null, "拳擊 (吊球)", PhyActivityStrengthEnum.MEDIUM.ordinal(), "其他"),
        });

        return pa;
    }
}