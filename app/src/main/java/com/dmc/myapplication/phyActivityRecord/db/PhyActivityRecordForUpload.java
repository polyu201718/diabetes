package com.dmc.myapplication.phyActivityRecord.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by terry on 2/1/2018.
 */


@Entity(nameInDb = "phy_activity_record_for_upload")
public class PhyActivityRecordForUpload implements Serializable {
    public enum ActionEnum {
        INSERT,
        DELETE,
    }

    static final long serialVersionUID = 536871008;

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "userId")
    private Integer userId;

    @Property(nameInDb = "activity")
    private String activity;

    @Property(nameInDb = "date")
    private Date date;

    @Property(nameInDb = "strength")
    private Integer strength;

    @Property(nameInDb = "category")
    private String category;

    @Property(nameInDb = "duration")
    private Integer duration;

    @Property(nameInDb = "action")
    private Integer action;

    @Generated(hash = 1190436323)
    public PhyActivityRecordForUpload(Long id, Integer userId, String activity,
            Date date, Integer strength, String category, Integer duration,
            Integer action) {
        this.id = id;
        this.userId = userId;
        this.activity = activity;
        this.date = date;
        this.strength = strength;
        this.category = category;
        this.duration = duration;
        this.action = action;
    }

    @Generated(hash = 1681747384)
    public PhyActivityRecordForUpload() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getActivity() {
        return this.activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Integer getStrength() {
        return this.strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getAction() {
        return this.action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
