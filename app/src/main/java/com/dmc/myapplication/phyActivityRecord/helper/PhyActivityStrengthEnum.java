package com.dmc.myapplication.phyActivityRecord.helper;

import android.graphics.Color;

/**
 * Created by terry on 8/1/2018.
 */

public enum PhyActivityStrengthEnum {
    LOW("低等強度活動", 0), MEDIUM("中等強度活動", 1), HIGH("劇烈活動", 2);

    private String name;
    private int index;

    PhyActivityStrengthEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(int index) {
        for (PhyActivityStrengthEnum c : PhyActivityStrengthEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public static int getColor(int index) {
        switch (index) {
            case 0:
                return Color.parseColor("#39ff92");
            case 1:
                return Color.parseColor("#fdfb5b");
            case 2:
                return Color.parseColor("#fd6b3a");
        }
        return Color.parseColor("#000000");
    }

    public static int getNameIndex(String name) {
        for (PhyActivityStrengthEnum c : PhyActivityStrengthEnum.values()) {
            if (c.getName().equals(name)) {
                return c.ordinal();
            }
        }

        return -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }


    public void setIndex(int index) {
        this.index = index;
    }
}
