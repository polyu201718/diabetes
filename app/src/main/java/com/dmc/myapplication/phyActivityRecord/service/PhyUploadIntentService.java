package com.dmc.myapplication.phyActivityRecord.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.dmc.myapplication.MyApplication;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecordForUpload;
import com.dmc.myapplication.phyActivityRecord.helper.PhyDBHelper;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class PhyUploadIntentService extends IntentService {

    private static HttpClient client = new DefaultHttpClient();

    private static final String EXTRA_DATA = "com.dmc.myapplication.phyActivityRecord.service.extra.DATA";
//
//    private static final PhyUploadIntentService instance = new PhyUploadIntentService();
//
//    public static PhyUploadIntentService getInstance() {
//        return instance;
//    }

    public PhyUploadIntentService() {
        super("PhyUploadIntentService");
    }

    public static void startUpload(PhyActivityRecordForUpload r) {
        Intent intent = new Intent(MyApplication.getApplication(), PhyUploadIntentService.class);
        intent.putExtra(EXTRA_DATA, r);
        MyApplication.getApplication().startService(intent);
    }

    public static void startUploadAll() {
        List<PhyActivityRecordForUpload> list = PhyDBHelper.getUploadRecord();
        for(PhyActivityRecordForUpload r: list){
            Intent intent = new Intent( MyApplication.getApplication(), PhyUploadIntentService.class);
            intent.putExtra(EXTRA_DATA, r);
            MyApplication.getApplication().startService(intent);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("PhyUploadService", "res onCreate");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("PhyUploadService", "res intent");
        if(intent == null ){
            //Log.d("PhyUploadService", "res intent null");
            return;
        }

        PhyActivityRecordForUpload r = (PhyActivityRecordForUpload) intent.getSerializableExtra(EXTRA_DATA);
        if(r == null) {
            Log.d("PhyUploadService", "r empty");
            return;
        }

        //Log.d("PhyUploadService", "r " + r.getActivity());

        boolean respOk = false;
        while(!respOk) {
            if(!isOnline()){
                Log.d("PhyUploadService", "not online");
                errorSleep();
                continue;
                //Log.d("PhyUploadService", "not online release");
            }

            //Log.d("PhyUploadService", "res start");
            HttpPost httpPost = new HttpPost("https://158.132.8.53/dma/prediabetes/phyactivity/phyactivity.php");
            try {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("activity", r.getActivity()));
                params.add(new BasicNameValuePair("category", r.getCategory()));
                params.add(new BasicNameValuePair("duration", r.getDuration().toString()));
                params.add(new BasicNameValuePair("strength", r.getStrength().toString()));
                params.add(new BasicNameValuePair("action", r.getAction().toString()));
                params.add(new BasicNameValuePair("id", r.getId().toString()));
                params.add(new BasicNameValuePair("user_id", r.getUserId().toString()));


                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"); // Quoted "Z" to indicate UTC, no timezone offset

                params.add(new BasicNameValuePair("date", df.format(r.getDate())));
                UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
                httpPost.setEntity(ent);

                HttpResponse resp = client.execute(httpPost);
                if(resp.getStatusLine().getStatusCode() != 200) {
                    Log.d("PhyUploadService", "res not 200");
                    errorSleep();
                    continue;
                }

                Log.d("PhyUploadService", "res ok");
                respOk = true;
                PhyDBHelper.deleteUploadRecord(r.getId());

            } catch (IOException e) {
                //Log.d("PhyUploadService", "res exp");
                //e.printStackTrace();
                errorSleep();
            }
        }
    }

    private void errorSleep() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    public static boolean isOnline() {
        ConnectivityManager mgr = (ConnectivityManager) MyApplication.getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mgr == null) {
            return false;
        }
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        }
        return networkInfo.isConnected();
    }
}