package com.dmc.myapplication.pojo;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by tsunmingtso on 26/12/2017.
 */

public class BloodTestRecord extends RealmObject {
    private int ID;
    private int USER_ID;
    private int RECORD_TYPE;

    private Date DATE;
    private double FASTING_BLOOD_SUGAR;
    private double HbA1c;
    private double TOTAL_C;
    private double HDL_C;
    private double LDL_C;
    private double TRIGLYCERIDES;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

    public int getRECORD_TYPE() {
        return RECORD_TYPE;
    }

    public void setRECORD_TYPE(int RECORD_TYPE) {
        this.RECORD_TYPE = RECORD_TYPE;
    }

    public Date getDATE() {
        return DATE;
    }

    public void setDATE(Date DATE) {
        this.DATE = DATE;
    }

    public double getFASTING_BLOOD_SUGAR() {
        return FASTING_BLOOD_SUGAR;
    }

    public void setFASTING_BLOOD_SUGAR(double FASTING_BLOOD_SUGAR) {
        this.FASTING_BLOOD_SUGAR = FASTING_BLOOD_SUGAR;
    }

    public double getHbA1c() {
        return HbA1c;
    }

    public void setHbA1c(double hbA1c) {
        HbA1c = hbA1c;
    }

    public double getTOTAL_C() {
        return TOTAL_C;
    }

    public void setTOTAL_C(double TOTAL_C) {
        this.TOTAL_C = TOTAL_C;
    }

    public double getLDL_C() {
        return LDL_C;
    }

    public void setLDL_C(double LDL_C) {
        this.LDL_C = LDL_C;
    }

    public double getHDL_C() {
        return HDL_C;
    }

    public void setHDL_C(double HDL_C) {
        this.HDL_C = HDL_C;
    }

    public double getTRIGLYCERIDES() {
        return TRIGLYCERIDES;
    }

    public void setTRIGLYCERIDES(double TRIGLYCERIDES) {
        this.TRIGLYCERIDES = TRIGLYCERIDES;
    }
}
