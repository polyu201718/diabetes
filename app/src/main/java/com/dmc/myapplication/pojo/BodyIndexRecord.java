package com.dmc.myapplication.pojo;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by tsunmingtso on 20/12/2017.
 */

public class BodyIndexRecord extends RealmObject {
    private int ID;
    private int USER_ID;
    private int RECORD_TYPE;

    private Date DATE;
    private double HEIGHT;
    private double WEIGHT;
    private double WAIST;
    private double BMI;
    private double FAT;

    private boolean isStoredToServer;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getRECORD_TYPE() {
        return RECORD_TYPE;
    }

    public void setRECORD_TYPE(int RECORD_TYPE) {
        this.RECORD_TYPE = RECORD_TYPE;
    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

    public Date getDATE() {
        return DATE;
    }

    public void setDATE(Date DATE) {
        this.DATE = DATE;
    }

    public double getHEIGHT() {
        return HEIGHT;
    }

    public void setHEIGHT(double HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public double getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(double WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public double getWAIST() {
        return WAIST;
    }

    public void setWAIST(double WAIST) {
        this.WAIST = WAIST;
    }

    public double getBMI() {
        return BMI;
    }

    public void setBMI(double BMI) {
        this.BMI = BMI;
    }

    public double getFAT() {
        return FAT;
    }

    public void setFAT(double FAT) {
        this.FAT = FAT;
    }

    public boolean isStoredToServer() {
        return isStoredToServer;
    }

    public void setStoredToServer(boolean storedToServer) {
        isStoredToServer = storedToServer;
    }
}
