package com.dmc.myapplication.pojo;

import io.realm.RealmObject;

/**
 * Created by tsunmingtso on 15/1/2018.
 */

public class User extends RealmObject {
    private int ID;
    private int user_id;
    private String name;
    private int sex;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
