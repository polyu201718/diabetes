package com.dmc.myapplication.prediction;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by lamivan on 30/8/2017.
 */

public class Json {
  static String fileName = "iteration.json";

  public static void saveData(Context context, String mJsonResponse) {
    try {
      // overwritting existing file contents
      FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" + fileName, false);
      file.write(mJsonResponse);
      file.flush();
      file.close();
      Log.e("end", mJsonResponse);
    } catch (IOException e) {
      Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
    }
  }

  public static String getData(Context context) {
    try {
      File f = new File(context.getFilesDir().getPath() + "/" + fileName);
      //check whether file exists
      FileInputStream is = new FileInputStream(f);
      int size = is.available();
      byte[] buffer = new byte[size];
      is.read(buffer);
      is.close();
      System.out.println(new String(buffer));
      return new String(buffer);
    } catch (IOException e) {
      Log.e("TAG", "Error in Reading: " + e.getLocalizedMessage());
      return null;
    }
  }
}