package com.dmc.myapplication.reminder;

import org.json.JSONObject;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class reminder {
    private long id;
    private String drugName;
    private int manyTime;
    private String time1;
    private String time2;
    private String time3;
    private String time4;
    private String broadcastNumber1;
    private String broadcastNumber2;
    private String broadcastNumber3;
    private String broadcastNumber4;
    private String remark;
    private String photoPath;

    public reminder() {
    }

    public reminder(long id, String drugName, int manyTime, String time1, String time2, String time3, String time4, String broadcastNumber1, String broadcastNumber2, String broadcastNumber3, String broadcastNumber4, String remark, String photoPath) {
        this.id = id;
        this.drugName = drugName;
        this.manyTime = manyTime;
        this.time1 = time1;
        this.time2 = time2;
        this.time3 = time3;
        this.time4 = time4;
        this.broadcastNumber1 = broadcastNumber1;
        this.broadcastNumber2 = broadcastNumber2;
        this.broadcastNumber3 = broadcastNumber3;
        this.broadcastNumber4 = broadcastNumber4;
        this.remark = remark;
        this.photoPath = photoPath;
    }

    public reminder(String drugName, int manyTime, String time1, String time2, String time3, String time4, String broadcastNumber1, String broadcastNumber2, String broadcastNumber3, String broadcastNumber4, String remark, String photoPath) {
        this.drugName = drugName;
        this.manyTime = manyTime;
        this.time1 = time1;
        this.time2 = time2;
        this.time3 = time3;
        this.time4 = time4;
        this.broadcastNumber1 = broadcastNumber1;
        this.broadcastNumber2 = broadcastNumber2;
        this.broadcastNumber3 = broadcastNumber3;
        this.broadcastNumber4 = broadcastNumber4;
        this.remark = remark;
        this.photoPath = photoPath;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public int getManyTime() {
        return manyTime;
    }

    public void setManyTime(int manyTime) {
        this.manyTime = manyTime;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {
        this.time3 = time3;
    }

    public String getTime4() {
        return time4;
    }

    public void setTime4(String time4) {
        this.time4 = time4;
    }

    public String getBroadcastNumber1() {
        return broadcastNumber1;
    }

    public void setBroadcastNumber1(String broadcastNumber1) {
        this.broadcastNumber1 = broadcastNumber1;
    }

    public String getBroadcastNumber2() {
        return broadcastNumber2;
    }

    public void setBroadcastNumber2(String broadcastNumber2) {
        this.broadcastNumber2 = broadcastNumber2;
    }

    public String getBroadcastNumber3() {
        return broadcastNumber3;
    }

    public void setBroadcastNumber3(String broadcastNumber3) {
        this.broadcastNumber3 = broadcastNumber3;
    }

    public String getBroadcastNumber4() {
        return broadcastNumber4;
    }

    public void setBroadcastNumber4(String broadcastNumber4) {
        this.broadcastNumber4 = broadcastNumber4;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }


    public String toString() {
        return "id=" + id + "name=" + drugName + " manyTime=" + manyTime + "time1=" + time1 + " time2=" + time2 + "time3=" + time3 + " time4=" + time4 + "broadcastNumber1=" + broadcastNumber1 + " broadcastNumber2=" + broadcastNumber2 + "broadcastNumber3=" + broadcastNumber3 + " broadcastNumber4=" + broadcastNumber4 + " remark=" + remark + " photoPath=" + photoPath;
    }

    public JSONObject toJsonObject(){
        JSONObject json = new JSONObject();
        try {
            json.put("drugName", drugName);
            json.put("manyTime", manyTime);
            json.put("time1", time1);
            json.put("time2",time2);
            json.put("time3",time3);
            json.put("time4",time4);
            json.put("remark",remark);
        }catch (Exception e){
            e.printStackTrace();
        }
        return json;
    }
}
