package com.dmc.myapplication.reminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by KwokSinMan on 16/3/2016.
 */
public class reminderEditActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener {
    private long id;
    private ImageView drugImage;
    private boolean hasImage = false;
    private String mNewPhotoPath="";
    private String oldPhotoPath="";
    private String imageFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reminder_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //reminder reminder = new reminder()
        Intent intent = getIntent();
        id = Long.parseLong(intent.getExtras().getString("id"));
        String name = intent.getExtras().getString("name");
        int manyTime = Integer.parseInt(intent.getExtras().getString("manyTime"));
        String time1 = intent.getExtras().getString("time1");
        String time2 = intent.getExtras().getString("time2");
        String time3 = intent.getExtras().getString("time3");
        String time4 = intent.getExtras().getString("time4");
        String remark = intent.getExtras().getString("remark");
        oldPhotoPath = intent.getExtras().getString("photoPath");

        /*
        System.out.println("Candy id=" + id);
        System.out.println("Candy name=" + name);
        System.out.println("Candy manyTime=" + manyTime);
        System.out.println("Candy time1=" + time1);
        System.out.println("Candy time2=" + time2);
        System.out.println("Candy time3=" + time3);
        System.out.println("Candy time4=" + time4);
        System.out.println("Candy remark=" + remark);
        System.out.println("Candy photoPath=" + photoPath);
        */

        reminderBiz reminderBiz = new reminderBiz();
        List<reminderRecordStringId> getFrequencyList = reminderBiz.getFrequencyDDL();
        ArrayAdapter<reminderRecordStringId> frequencyAdapter = new ArrayAdapter<reminderRecordStringId>(this, R.layout.navfood_spinner_center_item, getFrequencyList);
        frequencyAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner frequencySpinner = (Spinner) findViewById(R.id.frequency);
        frequencySpinner.setAdapter(frequencyAdapter);
        frequencySpinner.setOnItemSelectedListener(this);
        frequencySpinner.setSelection(manyTime-1);

        ((EditText)findViewById(R.id.drugName)).setText(name);

        if(manyTime == reminderConstant.GET_FREQUENCY_ONE){
            ((TextView)findViewById(R.id.time1)).setText(time1);
        }
        if(manyTime == reminderConstant.GET_FREQUENCY_TWO){
            ((TextView)findViewById(R.id.time1)).setText(time1);
            ((TextView)findViewById(R.id.time2)).setText(time2);
        }
        if(manyTime == reminderConstant.GET_FREQUENCY_THREE){
            ((TextView)findViewById(R.id.time1)).setText(time1);
            ((TextView)findViewById(R.id.time2)).setText(time2);
            ((TextView)findViewById(R.id.time3)).setText(time3);
        }
        if(manyTime == reminderConstant.GET_FREQUENCY_FOUR){
            ((TextView)findViewById(R.id.time1)).setText(time1);
            ((TextView)findViewById(R.id.time2)).setText(time2);
            ((TextView)findViewById(R.id.time3)).setText(time3);
            ((TextView)findViewById(R.id.time4)).setText(time4);
        }
        ((EditText)findViewById(R.id.other)).setText(remark);

        LinearLayout addRecordButtonSession = (LinearLayout) findViewById(R.id.addRecordButtonSession);
        addRecordButtonSession.setVisibility(View.GONE);

        // set time  picker
        ImageView settime1 = (ImageView) findViewById(R.id.setTime1);
        settime1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new reminderTime1PickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set time  picker
        ImageView setTime2 = (ImageView) findViewById(R.id.setTime2);
        setTime2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new reminderTime2PickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set time  picker
        ImageView setTime3 = (ImageView) findViewById(R.id.setTime3);
        setTime3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new reminderTime3PickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set time  picker
        ImageView setTime4 = (ImageView) findViewById(R.id.setTime4);
        setTime4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new reminderTime4PickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        EditText drugNameEditText = (EditText) findViewById(R.id.drugName);
        drugNameEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String drugName = ((EditText) findViewById(R.id.drugName)).getText().toString();
                Button editButton = (Button) findViewById(R.id.reminderEdit);
                if (drugName.isEmpty()) {
                    editButton.setEnabled(false);
                } else {
                    editButton.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        Button editButton = (Button) findViewById(R.id.reminderEdit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // delete all old reminder
                // delete reminder
                reminderDAO reminderDAO = new reminderDAO(getApplicationContext());
                reminderBiz biz = new reminderBiz();
                Context context = getApplicationContext();
                reminder result = reminderDAO.get(id);
                biz.deleteReminder(context,result.getBroadcastNumber1());
                biz.deleteReminder(context,result.getBroadcastNumber2());
                biz.deleteReminder(context,result.getBroadcastNumber3());
                biz.deleteReminder(context,result.getBroadcastNumber4());

                String drugName = ((EditText) findViewById(R.id.drugName)).getText().toString();
                int frequencyId = ((reminderRecordStringId) (((Spinner) findViewById(R.id.frequency)).getSelectedItem())).getId();
                String time1 = (String) ((TextView) findViewById(R.id.time1)).getText();
                String time2 = (String) ((TextView) findViewById(R.id.time2)).getText();
                String time3 = (String) ((TextView) findViewById(R.id.time3)).getText();
                String time4 = (String) ((TextView) findViewById(R.id.time4)).getText();
                String remark = ((EditText) findViewById(R.id.other)).getText().toString();

                /*
                System.out.println("Candy drugName=" + drugName);
                System.out.println("Candy frequencyId=" + frequencyId);
                System.out.println("Candy time1=" + time1);
                System.out.println("Candy time2=" + time2);
                System.out.println("Candy time3=" + time3);
                System.out.println("Candy time4=" + time4);
                System.out.println("Candy remark=" + remark);
                */

                String photoPath ="";
                if(hasImage){
                    photoPath = mNewPhotoPath;
                }else{
                    photoPath=oldPhotoPath;
                }

                broadcastNumberDAO broadcastNumberDAO = new broadcastNumberDAO(getApplicationContext());
                if(frequencyId == reminderConstant.GET_FREQUENCY_ONE){
                    long numberLong1 = broadcastNumberDAO.insert(id);
                    String number1 = Long.toString(numberLong1);
                    // set reminder
                    biz.setReminder(context, time1, id, numberLong1);
                    //update record
                    reminderDAO.update(new reminder(id,drugName,frequencyId,time1,"","","",number1,"","","",remark,photoPath));
                }
                if(frequencyId == reminderConstant.GET_FREQUENCY_TWO){
                    long numberLong1 = broadcastNumberDAO.insert(id);
                    long numberLong2 = broadcastNumberDAO.insert(id);
                    String number1 = Long.toString(numberLong1);
                    String number2 = Long.toString(numberLong2);
                    // set reminder
                    biz.setReminder(context, time1, id, numberLong1);
                    biz.setReminder(context, time2, id, numberLong2);
                    //update record
                    reminderDAO.update(new reminder(id, drugName, frequencyId, time1, time2, "", "",number1,number2,"","", remark, photoPath));
                }
                if(frequencyId == reminderConstant.GET_FREQUENCY_THREE){
                    long numberLong1 = broadcastNumberDAO.insert(id);
                    long numberLong2 = broadcastNumberDAO.insert(id);
                    long numberLong3 = broadcastNumberDAO.insert(id);
                    String number1 = Long.toString(numberLong1);
                    String number2 = Long.toString(numberLong2);
                    String number3 = Long.toString(numberLong3);
                    // set reminder
                    biz.setReminder(context, time1, id, numberLong1);
                    biz.setReminder(context, time2, id, numberLong2);
                    biz.setReminder(context, time3, id, numberLong3);
                    //update record
                    reminderDAO.update(new reminder(id, drugName, frequencyId, time1, time2, time3, "",number1,number2,number3,"", remark, photoPath));
                }
                if(frequencyId == reminderConstant.GET_FREQUENCY_FOUR){
                    long numberLong1 = broadcastNumberDAO.insert(id);
                    long numberLong2 = broadcastNumberDAO.insert(id);
                    long numberLong3 = broadcastNumberDAO.insert(id);
                    long numberLong4 = broadcastNumberDAO.insert(id);
                    String number1 = Long.toString(numberLong1);
                    String number2 = Long.toString(numberLong2);
                    String number3 = Long.toString(numberLong3);
                    String number4 = Long.toString(numberLong4);
                    // set reminder
                    biz.setReminder(context, time1, id, numberLong1);
                    biz.setReminder(context, time2, id, numberLong2);
                    biz.setReminder(context, time3, id, numberLong3);
                    biz.setReminder(context, time4, id, numberLong4);
                    //update record
                    reminderDAO.update(new reminder(id, drugName, frequencyId, time1, time2, time3, time4,number1,number2,number3,number4, remark, photoPath));
                }
                reminderBiz reminderBiz = new reminderBiz();
                reminderBiz.afterSaveReminderRecord(getActivityContentView(view));
            }

        });

        Button deleteButton = (Button) findViewById(R.id.reminderDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new reminderDeleteDialogFragment();
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        drugImage =   (ImageView) findViewById(R.id.addPhoto);
        imageTool.setPic(drugImage,oldPhotoPath);
        drugImage.setOnClickListener(new imageOnClickListener());

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

    private class imageOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            CharSequence[] items = { "相機", "相簿" };
            AlertDialog.Builder builder = new AlertDialog.Builder(reminderEditActivity.this);
            builder.setItems(items, new getImageMethodDialogOnClickListener());
            builder.show();
        }
    }
    private class getImageMethodDialogOnClickListener implements DialogInterface.OnClickListener{
        public void onClick(DialogInterface dialog, int item) {
            //System.out.println("Candy testing get image=" + item);
            if (item == 0) { // 照相機
                takePhoto();
            }else if (item == 1) { // 相簿
                selectFromGallery();
            }
        }
    }
    private void selectFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"),  reminderConstant.GET_EDIT_REMINDER_IMAGE_FROM_GALLERY_CODE);
    }
    private void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, reminderConstant.GET_EDIT_REMINDER_IMAGE);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        imageFileName = imageTool.getImageName();
        new exportTool().createDirIfNotExists(systemConstant.FILE_PATH_REMINDER_IMAGE);
        File image = new File(systemConstant.FILE_PATH_REMINDER_IMAGE + "/" + imageFileName + systemConstant.IMAGE_TYPE);
        // Save a file: path for use with ACTION_VIEW intents
        mNewPhotoPath = image.getAbsolutePath();
        return image;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == reminderConstant.GET_EDIT_REMINDER_IMAGE) { // 照相機
                setPic();
                hasImage = true;
            }else if (requestCode == reminderConstant.GET_EDIT_REMINDER_IMAGE_FROM_GALLERY_CODE){ //
                setGalleryPic(data);
                hasImage = true;
            }
        }
    }
    private void setGalleryPic(Intent data){
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        mNewPhotoPath = cursor.getString(column_index);
        imageFileName =imageTool.getImageName();
        //System.out.println("Candy mNewPhotoPath="+mNewPhotoPath);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mNewPhotoPath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = imageTool.rotateImage90(BitmapFactory.decodeFile(mNewPhotoPath, options));
        drugImage.setImageBitmap(bm);
    }

    private  void setPic(){
        int targetW = drugImage.getWidth();
        int targetH = drugImage.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mNewPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap rotatedBMP = imageTool.rotateImage90(BitmapFactory.decodeFile(mNewPhotoPath, bmOptions)) ;

        drugImage.setImageBitmap(rotatedBMP);
        //foodImage.setImageBitmap(bitmap);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int selectCode = ((reminderRecordStringId)parent.getItemAtPosition(position)).getId();
        LinearLayout timeArea2 = (LinearLayout) findViewById(R.id.timeArea2);
        timeArea2.setVisibility(View.GONE);
        LinearLayout timeArea3 = (LinearLayout) findViewById(R.id.timeArea3);
        timeArea3.setVisibility(View.GONE);
        LinearLayout timeArea4 = (LinearLayout) findViewById(R.id.timeArea4);
        timeArea4.setVisibility(View.GONE);

        if (selectCode== reminderConstant.GET_FREQUENCY_TWO) {
            timeArea2.setVisibility(View.VISIBLE);
        }

        if (selectCode== reminderConstant.GET_FREQUENCY_THREE){
            timeArea2.setVisibility(View.VISIBLE);
            timeArea3.setVisibility(View.VISIBLE);
        }
        if (selectCode== reminderConstant.GET_FREQUENCY_FOUR){
            timeArea2.setVisibility(View.VISIBLE);
            timeArea3.setVisibility(View.VISIBLE);
            timeArea4.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }
    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_REMINDER);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    private class reminderDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    reminderDAO reminderDAO = new reminderDAO(getApplicationContext());
                                    // delete reminder
                                    reminderBiz biz = new reminderBiz();
                                    Context context = getApplicationContext();
                                    reminder result = reminderDAO.get(id);
                                    biz.deleteReminder(context,result.getBroadcastNumber1());
                                    biz.deleteReminder(context,result.getBroadcastNumber2());
                                    biz.deleteReminder(context,result.getBroadcastNumber3());
                                    biz.deleteReminder(context,result.getBroadcastNumber4());

                                    //delete record from DB
                                    reminderDAO.delete(id);
                                    reminderBiz reminderBiz = new reminderBiz();
                                    reminderBiz.afterSaveReminderRecord((Activity) getActivity());
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }
}
