package com.dmc.myapplication.reminder;

/**
 * Created by KwokSinMan on 9/3/2016.
 */
public class reminderRecordStringId {
    private String value;
    private int id;

    public reminderRecordStringId(String value, int id) {
        this.value = value;
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public String toString() {
        return this.value;
    }
}
