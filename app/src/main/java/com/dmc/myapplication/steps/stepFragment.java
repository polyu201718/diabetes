package com.dmc.myapplication.steps;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dmc.myapplication.Models.stepRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Po on 6/3/2016.
 */
public class stepFragment extends Fragment {

    UserLocalStore userLocalStore;
    Context ctx;


    public stepFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.step_list, container, false);
        ctx = getContext();
        if (v.getParent() != null)
            ((ViewGroup) v.getParent()).removeView(v);

        userLocalStore = new UserLocalStore(getActivity());
        User user = userLocalStore.getLoggedInUser();

        LineChart chart = (LineChart) v.findViewById(R.id.chart1);
        chart.setDescription("");
        stepRecord stepRecordDB = new stepRecord(this.getContext());
        List<stepRecord.stepRecord_class> latest7data = stepRecordDB.getLatest7daysrecord();
        List<stepRecord.stepRecord_class> allRecordDesc = stepRecordDB.getAllDataDesc();

        //List<Entry> entries = new ArrayList<Entry>();
        //for (int x = 0; x < 7; x++) {

        // turn your data into Entry objects
        //    entries.add(new Entry(x, latest7data.get(x).STEP_COUNT));
        // }

        ArrayList<String> xValues = new ArrayList<String>();
        ArrayList<Entry> entries = new ArrayList<>();       //y-axis glucose

        final List<stepRowObj> listOfRow = new ArrayList<stepRowObj>();

        for (int x = 0; x < 7; x++) {
            xValues.add(latest7data.get(x).getDATE());
                entries.add(new Entry(latest7data.get(x).getSTEP_COUNT(), x));
//            int step = (int) (Math.random() * 5000) + 1;
//            entries.add(new Entry(step, x));
        }

        System.out.println("allRecordDesc.size = " + allRecordDesc.size());
        for (int x = 0; x < allRecordDesc.size(); x++) {
            listOfRow.add(new stepRowObj(allRecordDesc.get(x).getDATE(), allRecordDesc.get(x).getSTEP_COUNT()));
        }

        LineDataSet set1 = new LineDataSet(entries, "最近7天步數");

        set1.setCircleColor(Color.rgb(51, 102, 255));
        set1.setLineWidth(3);
        set1.setColor(Color.rgb(51, 102, 255));
        List<LineDataSet> setList = new ArrayList<>();
        setList.add(set1);

        LineData data = new LineData(xValues, setList);
        data.setValueFormatter(new MyValueFormatter());


        chart.setData(data);
        chart.invalidate(); // refresh

        final ListView listview = (ListView) v.findViewById(R.id.step_list);
        stepListAdapter madapter = new stepListAdapter(getContext(), listOfRow);
        listview.setAdapter(madapter);
        stepRecordDB.close();

        return v;
    }

    public class MyValueFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0"); // use one decimal
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

            return mFormat.format(value);
        }
    }

    public class stepRowObj {
        private String date;
        private Integer stepCount;
        private Integer year;
        private Integer month;
        private Integer day;

        public stepRowObj(String date, Integer stepCount) {
            this.date = date;
            this.stepCount = stepCount;

            String[] datespilt = date.split("-");
            year = Integer.parseInt(datespilt[0].trim());
            month = Integer.parseInt(datespilt[1].trim());
            day = Integer.parseInt(datespilt[2].trim());
        }

        public String getDate() {
            return date;
        }

        public Integer getStepCount() {
            return stepCount;
        }

        public Integer getYear() {
            return year;
        }

        public Integer getMonth() {
            return month;
        }

        public String getMonthChiStr() {

            switch (month) {
                case 1:
                    return "一月";

                case 2:
                    return "二月";
                case 3:
                    return "三月";
                case 4:
                    return "四月";
                case 5:
                    return "五月";
                case 6:
                    return "六月";
                case 7:
                    return "七月";
                case 8:
                    return "八月";
                case 9:
                    return "九月";
                case 10:
                    return "十月";
                case 11:
                    return "十一月";
                case 12:
                    return "十二月";
            }

            return null;
        }

        public Integer getDay() {
            return day;
        }

        public String getDaystr() {
            String strday = String.valueOf(day);
            if (strday.length() == 1) {
                return "0" + strday;
            } else {
                return strday;
            }
        }
    }

    public class stepListAdapter extends BaseAdapter {
        private Context context;
        private List<stepRowObj> liststepRowObj;


        public stepListAdapter(Context context, List<stepRowObj> liststepRowObj) {
            this.context = context;
            this.liststepRowObj = liststepRowObj;
        }

        public int getCount() {
            return liststepRowObj.size();
        }

        public Object getItem(int position) {
            return liststepRowObj.get(position);
        }

        public View getView(int position, View convertView, ViewGroup viewGroup) {
            stepRowObj entry = liststepRowObj.get(position);
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.step_list_row_layout, null);
            }
            TextView dayText = (TextView) convertView.findViewById(R.id.day);
            dayText.setText(entry.getDaystr());

            TextView monthText = (TextView) convertView.findViewById(R.id.month);
            monthText.setText(entry.getMonthChiStr());

            TextView stepCountNum = (TextView) convertView.findViewById(R.id.stepcountnumber);
            stepCountNum.setText(String.valueOf(entry.getStepCount()));

            ImageView foot3 = (ImageView) convertView.findViewById(R.id.foot3);
            ImageView foot2 = (ImageView) convertView.findViewById(R.id.foot2);
            ImageView foot1 = (ImageView) convertView.findViewById(R.id.foot1);

            if (entry.getStepCount() < 4000) {
                foot3.setVisibility(View.INVISIBLE);
                foot1.setVisibility(View.INVISIBLE);
                foot2.setVisibility(View.INVISIBLE);
            } else if (entry.getStepCount() < 8000) {
                foot2.setVisibility(View.INVISIBLE);
                foot3.setVisibility(View.INVISIBLE);
                foot1.setVisibility(View.VISIBLE);


            } else if (entry.getStepCount() < 10000) {
                foot3.setVisibility(View.INVISIBLE);
                foot1.setVisibility(View.VISIBLE);
                foot2.setVisibility(View.VISIBLE);

            }

            FrameLayout bgcolor = (FrameLayout) convertView.findViewById(R.id.bgcolor);
            if (entry.getMonth() <= 1) {
                bgcolor.setBackground(getResources().getDrawable(R.drawable.winter_bg_step_list));
            } else if (entry.getMonth() <= 3) {
                bgcolor.setBackground(getResources().getDrawable(R.drawable.spring_bg_step_list));

            } else if (entry.getMonth() <= 0) {
                bgcolor.setBackground(getResources().getDrawable(R.drawable.summer_bg_step_list));

            } else {
                bgcolor.setBackground(getResources().getDrawable(R.drawable.winter_bg_step_list));
            }


            return convertView;
        }

        public long getItemId(int position) {
            return position;
        }

    }

}


