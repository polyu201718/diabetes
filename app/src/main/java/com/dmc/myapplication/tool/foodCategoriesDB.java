package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dmc.myapplication.Models.*;

/**
 * Created by januslin on 11/10/2016.
 */
public class foodCategoriesDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "foodCategoriesDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public foodCategoriesDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new foodCategoriesDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //
        db.execSQL(foodCategories.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + foodCategories.TABLE_NAME);
        onCreate(db);
    }
}
