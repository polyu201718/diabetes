package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dmc.myapplication.Models.rewards;

/**
 * Created by lamivan on 26/12/2017.
 */

public class rewardsDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "rewards.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public rewardsDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new rewardsDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(rewards.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + rewards.TABLE_NAME);
        onCreate(db);
    }
}
