package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dmc.myapplication.Models.foodSubCategories;
import com.dmc.myapplication.Models.sync_data_version;

/**
 * Created by januslin on 11/10/2016.
 */
public class syncDataVersionDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "syncDataVersionDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public syncDataVersionDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new syncDataVersionDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //
        db.execSQL(sync_data_version.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + sync_data_version.TABLE_NAME);
        onCreate(db);
    }
}
