package com.dmc.myapplication.tool;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.R.id.input;

/**
 * Created by Po on 2/3/2016.
 */
public class timeCheck {

    //to check the time is within the time period
    public static boolean compareTimePlus2(String startTime, String endTime, String testTime){
        splitTime s_startTime = new splitTime(startTime);
        splitTime s_endTime = new splitTime(endTime);
        splitTime s_testTime = new splitTime(testTime);
        DateTime start, end;
        //System.out.println("Start: " + startTime+" End: "+endTime);

        if (s_startTime.getHour() > s_endTime.getHour()){ //handle case over the other day
            if (s_startTime.getHour() < 22 && s_endTime.getHour() < 22) {
                start = new DateTime(2016, 1, 25, s_startTime.getHour() + 2, s_startTime.getMin());
                end = new DateTime(2016, 1, 26, s_endTime.getHour() + 2, s_endTime.getMin());
            } else {
                start = new DateTime(2016, 1, 25, s_startTime.getHour(), s_startTime.getMin());
                end = new DateTime(2016, 1, 26, s_endTime.getHour(), s_endTime.getMin());
            }

        }else {
            if (s_startTime.getHour() < 22 && s_endTime.getHour() < 22) {
                start = new DateTime(2016, 1, 25, s_startTime.getHour() + 2, s_startTime.getMin());
                end = new DateTime(2016, 1, 25, s_endTime.getHour() + 2, s_endTime.getMin());
            } else {
                start = new DateTime(2016, 1, 25, s_startTime.getHour(), s_startTime.getMin());
                end = new DateTime(2016, 1, 25, s_endTime.getHour(), s_endTime.getMin());
            }
        }
        Interval interval = new Interval(start, end);
        DateTime test = new DateTime(2016, 1, 25, s_testTime.getHour(), s_testTime.getMin());
        return interval.contains(test);
    }

    //to check the time is within the time period
    public static boolean compareTime(String startTime, String endTime, String testTime){
        splitTime s_startTime = new splitTime(startTime);
        splitTime s_endTime = new splitTime(endTime);
        splitTime s_testTime = new splitTime(testTime);
        DateTime start, end;
        //System.out.println("Start: " + startTime+" End: "+endTime);

        if (s_startTime.getHour() > s_endTime.getHour()){ //handle case over the other day
                start = new DateTime(2016, 1, 25, s_startTime.getHour(), s_startTime.getMin());
                end = new DateTime(2016, 1, 26, s_endTime.getHour(), s_endTime.getMin());
        }else {
                start = new DateTime(2016, 1, 25, s_startTime.getHour(), s_startTime.getMin());
                end = new DateTime(2016, 1, 25, s_endTime.getHour(), s_endTime.getMin());
        }
        Interval interval = new Interval(start, end);
        DateTime test = new DateTime(2016, 1, 25, s_testTime.getHour(), s_testTime.getMin());
        return interval.contains(test);
    }


    //method to get hour and min from string
    private static class splitTime {
        int hour;
        int min;

        public splitTime(String time) {
            String[] temp = time.split(":");
            this.hour = Integer.parseInt(temp[0]);
            this.min = Integer.parseInt(temp[1]);
        }
        public int getHour() {
            return hour;
        }
        public int getMin() {
            return min;
        }
    }


    //to check the time is within the time period - String date
    public static boolean compareDateS(String startDate, String endDate, String testDate){
        splitDate s_startDate = new splitDate(startDate);
        splitDate s_endDate = new splitDate(endDate);
        splitDate s_testDate = new splitDate(testDate);
        DateTime start, end;


        start = new DateTime(s_startDate.getYear(), s_startDate.getMonth(), s_startDate.getDay(), 0, 0);
        end = new DateTime(s_endDate.getYear(), s_endDate.getMonth(), s_endDate.getDay(), 0, 0);

        Interval interval = new Interval(start, end);
        DateTime test = new DateTime(s_testDate.getYear(), s_testDate.getMonth(), s_testDate.getDay(), 0, 0);
        return interval.contains(test);
    }

    // //to check the time is within the time period - Datetime date
    public static boolean compareDateD(DateTime start, DateTime end, String testDate){

        //splitDate s_testDate = new splitDate(testDate);
        //Interval interval = new Interval(start, end);
        //DateTime test = new DateTime(s_testDate.getYear(), s_testDate.getMonth(), s_testDate.getDay(), 0, 0);

        try
        {
         /*   Calendar cal = Calendar.getInstance();
            //DateTimeFormat df =  new DateTimeFormat("yyyy-MM-dd");
            LocalDateTime localDateTime = LocalDateTime.parse(testDate, DateTimeFormat.forPattern("yyyy-MM-dd"));
            //cal.setTime(df.parse(testDate));
            cal.set(localDateTime.getYear(), localDateTime.getMonthOfYear(), localDateTime.getDayOfMonth(), 0, 0);

            Calendar cal2 = Calendar.getInstance();
            cal2.set(start.year().get(), start.monthOfYear().get(), start.dayOfMonth().get(), start.hourOfDay().get(), start.minuteOfHour().get());
            //cal2.setTime(start.toDate());

            Calendar cal3 = Calendar.getInstance();
            //cal3.setTime(end.toDate());
            cal3.set(end.year().get(), end.monthOfYear().get(), end.dayOfMonth().get(), end.hourOfDay().get(), end.minuteOfHour().get());

            //System.out.println("test="+cal.toString());
            //System.out.println("start="+cal2.toString());
            //System.out.println("end="+cal3.toString());

            System.out.println("CalAfterCal2Test1="+cal.after(cal2));
            System.out.println("CalBeforeCal3Test2="+cal.before(cal3));*/

            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime testDateO = fmt.parseDateTime(testDate);

            System.out.println("CalAfterCal2Test1="+testDateO.isAfter(start));
            System.out.println("CalBeforeCal3Test2="+testDateO.isBefore(end));


            return testDateO.isAfter(start)&&testDateO.isBefore(end);

        }catch (Exception e){
            System.out.println(e);
        }


        //return interval.contains(test);
        return false;
    }


    // //to check how many days  - Datetime date
    public static int calDaysUptoNow(String start, DateTime end){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime start_day = formatter.parseDateTime(start);

        return Days.daysBetween(start_day.withTimeAtStartOfDay(), end.withTimeAtStartOfDay()).getDays() ;
    }




    //method to get year, month and day from string
    private static class splitDate {
        int yy;
        int mm;
        int dd;

        public splitDate (String date) {
            String[] temp = date.split("-");
            this.yy = Integer.parseInt(temp[0]);
            this.mm = Integer.parseInt(temp[1]);
            this.dd = Integer.parseInt(temp[2]);
        }
        public int getYear() {
            return yy;
        }
        public int getMonth() {
            return mm;
        }
        public int getDay() {
            return dd;
        }

    }



}
