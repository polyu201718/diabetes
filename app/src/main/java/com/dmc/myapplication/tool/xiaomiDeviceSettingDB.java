package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.Models.exercise_type;
import com.dmc.myapplication.Models.exercise_type_record;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;

/**
 * Created by januslin on 11/10/2016.
 */
public class xiaomiDeviceSettingDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "xiaomiDeviceSettingDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public xiaomiDeviceSettingDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new xiaomiDeviceSettingDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(xiaomiDeviceSetting.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + xiaomiDeviceSetting.TABLE_NAME);
        onCreate(db);
    }
}
