package com.dmc.myapplication.util;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by tsunmingtso on 21/12/2017.
 */

public class DateUtil {
    private String TAG = this.getClass().getSimpleName();

    public Date stringToDate(String dateString) {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN);
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            Log.e(TAG, "Format is Wrong", e);
        }
        return date;
    }

    public String dateToString_English(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("d MMM", Locale.ENGLISH);
        return dateFormat.format(date);
    }


}
