package com.dmc.myapplication;

import android.util.Log;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

/**
 * Created by januslin on 23/2/2017.
 */
public class webHelper {
    public static String sendRequest(String Method, String data){
        if (systemConstant.SERVER_ADDRESS.contains("https")){
            HttpsURLConnection conn = null;
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                    @Override
                    public boolean verify (String var1, SSLSession var2){
                        Log.i("RestUtilImpl", "Approving certificate for " + var1);
                        return true;
                    }
                });
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (GeneralSecurityException e) {
            }


            try {
                // 创建一个URL对象
                URL mURL = new URL(systemConstant.SERVER_ADDRESS);
                System.out.println(systemConstant.SERVER_ADDRESS);
                System.out.println(data);
                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpsURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(5000);// 设置读取超时为5秒
                //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    String state = getStringFromInputStream(is);
                    System.out.println(data);
                    System.out.println(state);
                    if (conn != null) {
                        is.close();
                        conn.disconnect();// 关闭连接
                    }
                    return state;
                } else {
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }else{
            HttpURLConnection conn = null;
            try {
                // 创建一个URL对象
                URL mURL = new URL(systemConstant.SERVER_ADDRESS);

                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(5000);// 设置读取超时为5秒
                //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    String state = getStringFromInputStream(is);
                    System.out.println(data);
                    System.out.println(state);
                    if (conn != null) {
                        is.close();
                        conn.disconnect();// 关闭连接
                    }
                    return state;
                } else {
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }

        return null;
    }

    public static String sendRequestForPredict(String Method, String data){
        String SERVER_ADDRESS = "http://175.159.3.147:8080/predict";
        if (SERVER_ADDRESS.contains("https")){
            HttpsURLConnection conn = null;
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                    @Override
                    public boolean verify (String var1, SSLSession var2){
                        Log.i("RestUtilImpl", "Approving certificate for " + var1);
                        return true;
                    }
                });
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (GeneralSecurityException e) {
            }


            try {
                // 创建一个URL对象
                URL mURL = new URL(SERVER_ADDRESS);
                System.out.println(SERVER_ADDRESS);
                System.out.println(data);
                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpsURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(5000);// 设置读取超时为5秒
                //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    String state = getStringFromInputStream(is);
                    System.out.println(data);
                    System.out.println(state);
                    if (conn != null) {
                        is.close();
                        conn.disconnect();// 关闭连接
                    }
                    return state;
                } else {
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }else{
            HttpURLConnection conn = null;
            try {
                // 创建一个URL对象
                URL mURL = new URL(SERVER_ADDRESS);

                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(5000);// 设置读取超时为5秒
                //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    String state = getStringFromInputStream(is);
                    System.out.println(data);
                    System.out.println(state);
                    if (conn != null) {
                        is.close();
                        conn.disconnect();// 关闭连接
                    }
                    return state;
                } else {
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }

        return null;
    }

    private static String getStringFromInputStream(InputStream is)
            throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        // 模板代码 必须熟练
        byte[] buffer = new byte[1024];
        int len = -1;
        // 一定要写len=is.read(buffer)
        // 如果while((is.read(buffer))!=-1)则无法将数据写入buffer中
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        is.close();
        String state = os.toString();// 把流中的数据转换成字符串,采用的编码是utf-8(模拟器默认编码)
        os.close();
        return state;
    }

    private static void getFileFromInputStream(InputStream is, File file)
            throws IOException {
        FileOutputStream os = new FileOutputStream(file);
        // 模板代码 必须熟练
        byte[] buffer = new byte[1024];
        int len = -1;
        // 一定要写len=is.read(buffer)
        // 如果while((is.read(buffer))!=-1)则无法将数据写入buffer中
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        is.close();
        os.close();
    }


    public static File sendRequestAndDownloadFileTo(String Method, String data, File fileLocation) {
        if (systemConstant.SERVER_ADDRESS.contains("https")){
            HttpsURLConnection conn = null;
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                    @Override
                    public boolean verify (String var1, SSLSession var2){
                        Log.i("RestUtilImpl", "Approving certificate for " + var1);
                        return true;
                    }
                });
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (GeneralSecurityException e) {
            }


            try {
                // 创建一个URL对象
                URL mURL = new URL(systemConstant.SERVER_ADDRESS);
                System.out.println(systemConstant.SERVER_ADDRESS);
                System.out.println(data);
                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpsURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(5000);// 设置读取超时为5秒
                //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    getFileFromInputStream(is, fileLocation);
                    System.out.println(data);
                    if (conn != null) {
                        is.close();
                        conn.disconnect();// 关闭连接
                    }
                    return fileLocation;
                } else {
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }else{
            HttpURLConnection conn = null;
            try {
                // 创建一个URL对象
                URL mURL = new URL(systemConstant.SERVER_ADDRESS);

                // 调用URL的openConnection()方法,获取HttpURLConnection对象
                conn = (HttpURLConnection) mURL.openConnection();

                conn.setRequestMethod(Method);// 设置请求方法为post
                //conn.setReadTimeout(5000);// 设置读取超时为5秒
                //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
                conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

                // post请求的参数
                //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
                // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
                OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
                out.write(data.getBytes());
                out.flush();
                out.close();

                int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
                if (responseCode == 200) {
                    InputStream is = conn.getInputStream();
                    getFileFromInputStream(is, fileLocation);
                    System.out.println(data);
                    if (conn != null) {
                        is.close();
                        conn.disconnect();// 关闭连接
                    }
                    return fileLocation;
                } else {
                    System.out.println("Unable to reach the server. errorcode:" + responseCode);
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();// 关闭连接
                }
            }
        }

        return null;
    }
}
