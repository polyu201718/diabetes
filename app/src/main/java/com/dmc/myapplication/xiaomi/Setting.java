package com.dmc.myapplication.xiaomi;


import android.content.Context;
import android.content.SharedPreferences;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.preference.Settings;


/**
 * Created by Po on 25/1/2016.
 */
public class Setting {

    //set the SharedPreferences database name
    //public static final String SP_NAME = "userData";

    SharedPreferences SettingLocalDatabase;


    public Setting(Context context) {
        SettingLocalDatabase = context.getSharedPreferences("XiaomiSetting",
                Context.MODE_PRIVATE);
    }

    //save user data to the local SharePreferences
    public void setSetting(setting setting) {
        SharedPreferences.Editor userLocalDatabaseEditor = SettingLocalDatabase.edit();
        userLocalDatabaseEditor.putString("mac_address", setting.getMac_address());
        userLocalDatabaseEditor.putString("device_name", setting.getDevice_name());
        userLocalDatabaseEditor.commit();
    }


    public setting getSetting() {

        //read user data from local sharePreferences
        String mac_address = SettingLocalDatabase.getString("mac_address", "");
        String device_name = SettingLocalDatabase.getString("device_name", "");

        setting storedUser = new setting(mac_address, device_name);
        return storedUser;
    }

    public class setting{
        String mac_address = "";
        String device_name = "";

        public setting(String mac_address, String device_name) {
            this.mac_address = mac_address;
            this.device_name = device_name;
        }

        public String getMac_address() {
            return mac_address;
        }

        public void setMac_address(String mac_address) {
            this.mac_address = mac_address;
        }

        public String getDevice_name() {
            return device_name;
        }

        public void setDevice_name(String device_name) {
            this.device_name = device_name;
        }
    }

}


